<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeographicBoundariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geographic_boundaries', function (Blueprint $table) {

            $table->id();
            $table->string('name');
            $table->string('description');
            $table->integer('geographic_boundary_type_id');
            $table->integer('parent_id');
            $table->boolean('is_leaf');
            $table->enum('status', ['active', 'passive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
