<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SeedInitialData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        $organizationTypeSeeder = new OrganizationTypeSeeder();
        $organizationTypeSeeder->run();

        $organizationSeeder = new OrganizationSeeder();
        $organizationSeeder->run();

        $geographicSeederType = new GeographicBoundaryTypeSeeder();
        $geographicSeederType->run();

        $geographicSeeder = new GeographicBoundarySeeder();
        $geographicSeeder->run();

        $corporationTypeSeeder = new CorporationTypeSeeder();
        $corporationTypeSeeder->run();

        $corporationSeeder = new CorporationSeeder();
        $corporationSeeder->run();

        $productTypeSeeder = new ProductTypeSeeder();
        $productTypeSeeder->run();

        $paymentSeeder = new PaymentSeeder();
        $paymentSeeder->run();

        $rolesAndPermissionsSeeder = new RolesAndPermissionsSeeder();
        $rolesAndPermissionsSeeder->run();

        $userSeeder = new UserSeeder();
        $userSeeder->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
