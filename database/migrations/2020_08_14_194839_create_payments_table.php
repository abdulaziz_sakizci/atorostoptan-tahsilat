<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('code');     //manuel //*
            $table->integer('corporation_id'); //user in corporation idsi olucak user dan gelen
            $table->string('student_name');     //*
            $table->string('guardian_name');//*
            $table->double('amount');//*
            $table->text('guardian_phone');//*
            $table->string('product_type_name'); //* açılır menu seklinde
            $table->string('description');  //*
            $table->integer('user_id');  //* login olanın id si
            $table->string('bank_name')->nullable();
            $table->integer('installment')->default(0)->nullable();
            $table->string('bank_success_message')->nullable();
            $table->string('bank_error_message')->nullable();
            $table->string('bank_response_code')->nullable();
            $table->text('bank_response')->nullable();
            $table->enum('status', ['pending', 'paid', 'cancelled', 'failed'])->default('pending'); //panding //* create gozukcek
            $table->dateTime('paid_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
