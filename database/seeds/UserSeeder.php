<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'admin';
        $user->email = 'info@aurorabilisim.com';
        $user->password = bcrypt('password');
        $user->corporation_id = 1;
        $user->save();

        $user->assignRole(1, 'Admin');

        $user2 = new User();
        $user2->name = 'Test Bölge';
        $user2->email = ' ';
        $user2->password = bcrypt('password');
        $user2->corporation_id = 1;
        $user2->save();

        $user2->assignRole(308, 'Bölge Muhasebe');

        $user3 = new User();
        $user3->name = 'Test Mıntıka';
        $user3->email = 'mintika@aurorabilisim.com';
        $user3->password = bcrypt('password');
        $user3->corporation_id = 1;
        $user3->save();

        $user3->assignRole(309, 'Mıntıka Muhasebe');

        $user4 = new User();
        $user4->name = 'Test Kurum';
        $user4->email = 'kurum@aurorabilisim.com';
        $user4->password = bcrypt('password');
        $user4->corporation_id = 1;
        $user4->save();

        $user4->assignRole(310, 'Kurum Muhasebe');

        $user5 = new User();
        $user5->name = 'Test Çamlıca Muhasebe';
        $user5->email = 'camlica@aurorabilisim.com';
        $user5->password = bcrypt('password');
        $user5->corporation_id = 1;
        $user5->save();

        $user5->assignRole(1, 'Çamlıca Muhasebe');

    }
}
