<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class OrganizationSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'organizations';
        $this->filename = base_path().'/database/seeds/csvs/organizations.csv';
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->table)->truncate();

        parent::run();
    }
}
