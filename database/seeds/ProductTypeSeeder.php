<?php

use Illuminate\Database\Seeder;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_types')->insert([
            "id" =>'1',
            "name" =>"Kırtasiye Malzemesi",
            "status" => "active"
        ]);
    }
}
