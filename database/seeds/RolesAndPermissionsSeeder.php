<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use \Illuminate\Support\Facades\DB;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'profile operations']);
        Permission::create(['name' => 'product operations']);
        Permission::create(['name' => 'corporation operations']);
        Permission::create(['name' => 'user operations']);
        Permission::create(['name' => 'role operations']);
        Permission::create(['name' => 'permission operations']);
        Permission::create(['name' => 'create update payment']);
        Permission::create(['name' => 'list payments']);

        $role = Role::create(['name' => 'Admin', 'organization_type_id' => 1]);
        $role->givePermissionTo(Permission::all());

        // adminden ödeme ekleme yetkisini aldık
        $role->revokePermissionTo('create update payment');

        $role = Role::create(['name' => 'Çamlıca Muhasebe', 'organization_type_id' => 1]);
        $role->givePermissionTo(['profile operations', 'product operations', 'corporation operations', 'user operations', 'list payments']);

        $role = Role::create(['name' => 'Bölge Muhasebe', 'organization_type_id' => 2]);
        $role->givePermissionTo(['list payments']);


        $role = Role::create(['name' => 'Mıntıka Muhasebe', 'organization_type_id' => 3]);
        $role->givePermissionTo(['profile operations', 'list payments', 'create update payment']);

        $role = Role::create(['name' => 'Kurum Muhasebe', 'organization_type_id' => 4]);
        $role->givePermissionTo(['profile operations', 'list payments', 'create update payment']);
    }
}
