<?php

use Illuminate\Database\Seeder;

class GeographicBoundaryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

           DB::table('geographic_boundary_types')->insert(
               [
                   [
                       'id' => 1,
                       'name' => 'Ülke',
                       'level' => 1,
                       'code' => 'ÜLKE',
                       'status' => 'active',
                   ],
                   [
                       'id' => 2,
                       'name' => 'İl',
                       'level' => 2,
                       'code' => 'İL',
                       'status' => 'active',
                   ],
                   [
                       'id' => 3,
                       'name' => 'İlçe',
                       'level' => 3,
                       'code' => 'İLÇE',
                       'status' => 'active',
                   ]
               ]
           );
    }
}
