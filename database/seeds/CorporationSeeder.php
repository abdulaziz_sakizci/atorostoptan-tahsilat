<?php

use Illuminate\Database\Seeder;

class CorporationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('corporations')->insert(
            [
                [
                    'id' => 1,
                    'name' => 'TEST KURUM',
                    'code' => '1234567890',
                    'description' => 'Özel',
                    'corporation_type_id' => 1,
                    'organization_id' => 310,
                    'geographic_boundary_id' => 297,
                    'status' => 'active',

                ]
            ]
        );
    }
}
