<?php

use Illuminate\Database\Seeder;

class CorporationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('corporation_types')->insert(
            [
                [
                    'id' => 1,
                    'name' => 'Anaokulu',
                    'status' => 'active',
                ],
                [
                    'id' => 2,
                    'name' => 'Özel Okul',
                    'status' => 'active',
                ],
                [
                    'id' => 3,
                    'name' => 'Bayi',
                    'status' => 'active',
                ],

            ]
        );
    }
}
