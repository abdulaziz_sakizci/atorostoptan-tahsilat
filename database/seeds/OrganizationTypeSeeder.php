<?php

use Illuminate\Database\Seeder;

class OrganizationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organization_types')->insert(
            [
                [
                    'id' => 1,
                    'name' => 'Merkez',
                    'level' => 1,
                    'code' => 'MERKEZ',
                    'status' => 'active',
                ],
                [
                    'id' => 2,
                    'name' => 'Bölge',
                    'level' => 2,
                    'code' => 'BOLGE',
                    'status' => 'active',
                ],
                [
                    'id' => 3,
                    'name' => 'Mıntıka',
                    'level' => 3,
                    'code' => 'MINTIKA',
                    'status' => 'active',
                ],
                [
                    'id' => 4,
                    'name' => 'Kurum',
                    'level' => 4,
                    'code' => 'KURUM',
                    'status' => 'active',
                ]
            ]
        );
    }
}
