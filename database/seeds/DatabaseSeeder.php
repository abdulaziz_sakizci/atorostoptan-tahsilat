<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(OrganizationTypeSeeder::class);
        $this->call(OrganizationSeeder::class);
        $this->call(GeographicBoundarySeeder::class);
        $this->call(GeographicBoundaryTypeSeeder::class);
        $this->call(CorporationSeeder::class);
        $this->call(CorporationTypeSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ProductTypeSeeder::class);
        $this->call(PaymentSeeder::class);

    }
}
