<?php
use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class GeographicBoundarySeeder extends CsvSeeder
{

    public function __construct()
    {
        $this->table = 'geographic_boundaries';
        $this->filename = base_path().'/database/seeds/csvs/geographicBoundary.csv';
        $this->insert_chunk_size = 1;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->table)->truncate();

        parent::run();
    }
}
