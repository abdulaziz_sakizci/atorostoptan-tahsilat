<?php

// Home
Breadcrumbs::for('home', function ($trail) {

    $dashboard = e(__('admin/layouts.dashboard'));

    $trail->push( $dashboard , route('default.index'));
});

// Home > User
Breadcrumbs::for('user', function ($trail) {
    $user = e(__('admin/user.index.user operations'));
    $trail->parent('home');
    $trail->push($user, route('user.index'));
});


// Home > User > User_create
Breadcrumbs::for('user_create', function ($trail) {
    $user_create = e(__('admin/user.create.user add'));
    $trail->parent('user');
    $trail->push($user_create, route('user.create'));
});

// Home > User > User_edit
    Breadcrumbs::for('user_edit', function ($trail, $users) {
    $trail->parent('user');
    $trail->push($users->name, route('user.edit', $users->id));
});


// Home > Corporation
Breadcrumbs::for('corporation', function ($trail) {
    $corporation = e(__('admin/corporation.index.corporation operations'));
    $trail->parent('home');
    $trail->push($corporation, route('corporation.index'));
});

// Home > Corporation > Corporation_create
Breadcrumbs::for('corporation_create', function ($trail) {
    $corporation_create = e(__('admin/corporation.create.corporation add'));
    $trail->parent('corporation');
    $trail->push($corporation_create, route('corporation.create'));
});

// Home > Corporation > Corporation_edit
Breadcrumbs::for('corporation_edit', function ($trail, $corporations) {
    $trail->parent('corporation');
    $trail->push($corporations->name, route('corporation.edit', $corporations->id));
});

// Home > Payment
Breadcrumbs::for('payment', function ($trail) {
    $payment = e(__('admin/payment.index.payment transactions'));
    $trail->parent('home');
    $trail->push($payment, route('payment.index'));
});

// Home > Payment > Payment_create
Breadcrumbs::for('payment_create', function ($trail) {
    $payment_create = e(__('admin/payment.create.payment add'));
    $trail->parent('payment');
    $trail->push($payment_create, route('payment.create'));
});

// Home > Payment > Payment_edit
Breadcrumbs::for('payment_edit', function ($trail, $payments) {
    $payment_edit = e(__('admin/payment.edit.payment edit'));
    $trail->parent('payment');
    $trail->push($payment_edit, route('payment.edit', $payments->id));
});

// Home > Payment > Payment_details
Breadcrumbs::for('payment_show', function ($trail, $payments) {
    $payment_show = e(__('admin/payment.edit.payment details'));
    $trail->parent('payment');
    $trail->push($payment_show, route('payment.show', $payments->id));
});


// Home > Product
Breadcrumbs::for('product', function ($trail) {
    $product = e(__('admin/productType.index.product operations'));
    $trail->parent('home');
    $trail->push($product, route('product.index'));
});

// Home > Product > Product_create
Breadcrumbs::for('product_create', function ($trail) {
    $product_create = e(__('admin/productType.create.product add'));
    $trail->parent('product');
    $trail->push($product_create, route('product.create'));
});

// Home > Product > Product_edit
Breadcrumbs::for('product_edit', function ($trail, $products) {
    $product_edit = e(__('admin/productType.edit.product edit'));
    $trail->parent('product');
    $trail->push($product_edit, route('product.edit', $products->id));
});

// Home > Role
Breadcrumbs::for('role', function ($trail) {
    $role = e(__('admin/user.index.roles'));
    $trail->parent('home');
    $trail->push($role, route('role.index'));
});

// Home > Role > Role_create
Breadcrumbs::for('role_create', function ($trail) {
    $role_create = e(__('admin/user.create.role add'));
    $trail->parent('role');
    $trail->push($role_create, route('role.create'));
});

// Home > Role > Role_edit
Breadcrumbs::for('role_edit', function ($trail, $roles) {
    $trail->parent('role');
    $trail->push($roles->name, route('role.edit', $roles->id));
});

// Home > Permission
Breadcrumbs::for('permission', function ($trail) {
    $permission = e(__('admin/user.index.permissions'));
    $trail->parent('home');
    $trail->push($permission, route('permission.index'));
});

// Home > Permission > Permission_create
Breadcrumbs::for('permission_create', function ($trail) {
    $permission_create = e(__('admin/user.create.permission add'));
    $trail->parent('permission');
    $trail->push($permission_create, route('permission.create'));
});

// Home > Permission > Permission_edit
Breadcrumbs::for('permission_edit', function ($trail, $permissions) {
    $trail->parent('permission');
    $trail->push($permissions->name, route('permission.edit', $permissions->id));
});

// Home > Report
Breadcrumbs::for('report', function ($trail) {
    $payment = e(__('admin/report.index.report transactions'));
    $trail->parent('home');
    $trail->push($payment, route('report.index'));
});
//
//// Home > User > User_create
//Breadcrumbs::for('corporation_create', function ($trail) {
//    $user_create = e(__('admin/user.create.user add'));
//    $trail->parent('user');
//    $trail->push($user_create, route('user.create'));
//});
//
//// Home > User > User_edit
//    Breadcrumbs::for('corporation_edit', function ($trail, $users) {
//    $trail->parent('user');
//    $trail->push($users->name, route('user.edit', $users->id));
//});


//// Home > Blog > [Category]
//Breadcrumbs::for('user_edit', function ($trail,$user) {
//    $trail->parent('user');
//    $trail->push('Kullanıcı Ekle', route('user.edit',$user->id));
//});


//
//// Home > Blog
//Breadcrumbs::for('blog', function ($trail) {
//    $trail->parent('home');
//    $trail->push('Blog', route('blog'));
//});
//
//// Home > Blog > [Category]
//Breadcrumbs::for('category', function ($trail, $category) {
//    $trail->parent('blog');
//    $trail->push($category->title, route('category', $category->id));
//});
//
//// Home > Blog > [Category] > [Post]
//Breadcrumbs::for('post', function ($trail, $post) {
//    $trail->parent('category', $post->category);
//    $trail->push($post->title, route('post', $post->id));
//});
