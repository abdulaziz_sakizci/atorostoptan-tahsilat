<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Dashboard

Route::get('/', function () {
    return redirect('/admin');
});

Route::namespace('Admin')->group(function () {
    Route::prefix('admin')->group(function () {

        //Ralationship işlemleri için test controller
        Route::get('/test', 'OrganizationController@index');

        Route::get('/', function () {
            return view('admin.default.index');
        })->middleware('auth')->name('default.index');

        //user
        Route::resource('user', 'UserController')->middleware('auth');

        //corporation
        Route::resource('corporation', 'CorporationController')->middleware('auth');
        Route::resource('report', 'ReportController')->middleware('auth');
        Route::get('export', 'ReportController@export');

        Route::get('/ajax/geographic-boundaries', 'AjaxController@getGeographicBoundriesByParentId')->name('ajax.geographic-boundaries');
        Route::get('/ajax/organizations', 'AjaxController@getOrganizationsByParentId')->name('ajax.organizations');
        Route::get('/ajax/corporations', 'AjaxController@getCorporationByOrganizationId')->name('ajax.corporations');
        Route::get('/ajax/roles', 'AjaxController@getRolesByOrganizationId')->name('ajax.roles');
        Route::post('/ajax/enqueue-sms', 'AjaxController@enqueueSms')->name('ajax.enqueue.sms');

        //product type
        Route::resource('product', 'ProductTypeController')->middleware('auth');

        //payment
        Route::resource('payment', 'PaymentController')->middleware('auth');
        Route::put('/payment/cancel/{id}', 'PaymentController@cancelPayment')->name('payment.cancel');

        // Role
        Route::resource('role', 'RoleController')->middleware('auth');

        // Permission
        Route::resource('permission', 'PermissionController')->middleware('auth');

    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('test-sms', 'SmsController@enqueue');


Route::get('/payment/success', 'PaymentController@success')->name('payments.success');
Route::get('/payment/fail', 'PaymentController@fail')->name('payments.fail');
Route::get('/payment/{code}', 'PaymentController@payment')->name('payments.index');


// finansbank
Route::get('/bank/finansbank', 'BankController@finansbank')->name('bank.finansbank');
Route::any('/bank/finansbank/return', 'BankController@finansbankReturn')->name('bank.finansbankReturn');

//  ziraatbankasi
Route::get('/bank/ziraatbankasi', 'BankController@ziraatbankasi')->name('bank.ziraatbankasi');
Route::any('/bank/ziraatbankasi/return', 'BankController@ziraatbankasiReturn')->name('bank.ziraatbankSuccess');
//Route::post('/paytest', 'BankController@ziraatbank')->name('bank.paytest');

//  isbankasi
Route::get('/bank/isbankasi', 'BankController@isbankasi')->name('bank.ziraatbankasi');
Route::any('/bank/isbankasi/return', 'BankController@isbankasiReturn')->name('bank.isbankasiSuccess');

// denizbank
Route::get('/bank/denizbank', 'BankController@denizbank')->name('bank.denizbank');
Route::any('/bank/denizbank/return', 'BankController@denizbankReturn')->name('bank.denizbankReturn');

//  vakifbank
Route::get('/bank/vakifbank', 'BankController@vakifbank')->name('bank.vakifbank');
Route::post('/bank/vakifbank', 'BankController@vakifbank')->name('bank.vakifbank');
Route::any('/bank/vakifbank/return', 'BankController@vakifbankGetResponse')->name('bank.vakifbankSuccess');

//  akbank
Route::get('/bank/akbank', 'BankController@akbank')->name('bank.akbank');
Route::any('/bank/akbank/return', 'BankController@akbankReturn')->name('bank.akbankReturn');

Route::get('test', 'Admin\AjaxController@index');

Route::get('test2', 'TestController@index');
Route::get('test3', 'TestController@test3');

Route::get('404', function () {
    return view('404');
})->name('404');

