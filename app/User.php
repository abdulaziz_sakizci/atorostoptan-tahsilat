<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Overrides\Traits\HasRoles;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class User
 * @package App
 *
 * @property int id
 * @property string name
 * @property string email
 * @property Carbon email_verified_at
 * @property string password
 * @property string remember_token
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @property-read Corporation corporation
 * @property-read Collection|Payment payment
 */
class User extends Authenticatable
{

    //Activity Log
    use LogsActivity;
    protected static $logAttributes = [];

    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'corporation_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function corporation()
    {
        return $this->hasOne(Corporation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payment()
    {
        return $this->hasMany(Payment::class);
    }

    /**
     * @return array
     */
    public function getOrganizationHierarchy()
    {
        $userRole = $this->roles()->first();
        $organizationTypeId = $userRole->organization_type_id;

        $organizationType = OrganizationType::find($organizationTypeId);

        $organizationId = DB::table('model_has_roles')
            ->where([
                'role_id' => $userRole->id,
                'model_id' => $this->id
            ])
            ->first()
            ->organization_id;

        $organization = Organization::find($organizationId);

        $organizationHierarchy = [
            "organization_type_id" => $organizationType->id,
            "organization_type" => $organizationType->code
        ];

        if (!$organization) {
            $organizationHierarchy["description"] = "Hatalı Kayıt";
            $organizationHierarchy["kurum"] = "Hatalı Kayıt";
            $organizationHierarchy["mintika"] = "Hatalı Kayıt";
            $organizationHierarchy["bolge"] = "Hatalı Kayıt";
            $organizationHierarchy["description"] = "Hatalı Kayıt";;
            return $organizationHierarchy;
        }

        switch ($organizationTypeId) {

            case 1:
                $organizationHierarchy["description"] = "Merkez";
                break;

            case 2:
                $organizationHierarchy["bolge"] = $organization->name;
                $organizationHierarchy["description"] = "Bölge: " . $organization->name;
                break;

            case 3:
                $organizationHierarchy["mintika"] = $organization->name;
                $organizationHierarchy["bolge"] = $organization->parent->name;
                $organizationHierarchy["description"] = "Mıntıka: " . $organization->name . " -> Bölge: " . $organization->parent->name;
                break;

            case 4:
                $organizationHierarchy["kurum"] = $organization->name;
                $organizationHierarchy["mintika"] = $organization->parent->name;
                $organizationHierarchy["bolge"] = $organization->parent->parent->name;
                $organizationHierarchy["description"] = "Kurum: " . $organization->name . " -> Mıntıka: " . $organization->parent->name . " -> Bölge: " . $organization->parent->parent->name;
                break;
        }

        return $organizationHierarchy;
    }
}
