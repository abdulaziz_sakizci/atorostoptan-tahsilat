<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class GeographicBoundaryType
 * @package App
 *
 * @property int id
 * @property string name
 * @property int level
 * @property string code
 * @property string status
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @property-read GeographicBoundary geographicBoundaries
 */
class GeographicBoundaryType extends Model
{

    //Activity Log
    use LogsActivity;
    protected static $logAttributes = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function geographicBoundaries()
    {
        return $this->hasMany(GeographicBoundary::class);
    }
}
