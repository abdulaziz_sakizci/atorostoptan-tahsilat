<?php

namespace App\Scopes;

use App\Corporation;
use App\Organization;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $userRole = Auth::user()->roles()->first();

        switch ($userRole->organization_type_id) {

            case 1:
                break;

            case 2:

                if (Auth::user()->hasRole('Bölge Muhasebe')) {

                    $organizationId = DB::table('model_has_roles')
                        ->where([
                            'role_id' => $userRole->id,
                            'model_id' => Auth::user()->id
                        ])
                        ->first()
                        ->organization_id;

                    $region = Organization::find($organizationId);
                    $zones = $region->children;
                    $corporationsIds = [];
                    foreach ($zones as $zone) {

                        foreach ($zone->children as $organization)

                            foreach ($organization->corporations as $corporation) {

                                $corporationsIds[] = $corporation->id;
                            }
                    }

                    $builder->whereIn('corporation_id', $corporationsIds);
                }

                break;

            case 3:
                if (Auth::user()->hasRole('Mıntıka Muhasebe')) {

                    $organizationId = DB::table('model_has_roles')
                        ->where([
                            'role_id' => $userRole->id,
                            'model_id' => Auth::user()->id
                        ])
                        ->first()
                        ->organization_id;

                    $zone = Organization::find($organizationId);
                    $corporations = $zone->children;
                    $corporationsIds = [];
                    foreach ($corporations as $corporation) {

                        $corporationsIds[] = $corporation->id;
                    }

                    $builder->whereIn('corporation_id', $corporationsIds);
                }

                break;

            case 4:
                $organizationId = DB::table('model_has_roles')
                    ->where([
                        'role_id' => $userRole->id,
                        'model_id' => Auth::user()->id
                    ])
                    ->first()
                    ->organization_id;
                $corporation = Corporation::where('organization_id', $organizationId)->first();

                $builder->where('corporation_id', '=', $corporation->id);
                break;

            default:
                throw new \Exception();
        }

    }
}
