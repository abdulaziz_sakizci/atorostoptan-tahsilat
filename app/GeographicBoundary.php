<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class GeographicBoundary
 * @package App
 *
 * @property int id
 * @property string name
 * @property string description
 * @property int geographic_boundary_type_id
 * @property int parent_id
 * @property boolean is_leaf
 * @property string status
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @property-read GeographicBoundaryType geographicBoundaryType
 * @property-read Collection|Corporation corporations
 * @property-read GeographicBoundary parent
 */
class GeographicBoundary extends Model
{

    //Activity Log
    use LogsActivity;
    protected static $logAttributes = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function geographicBoundaryType()
    {
        return  $this->belongsTo(GeographicBoundaryType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function corporations()
    {
        return $this->hasMany(Corporation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parent()
    {
        return $this->hasOne(GeographicBoundary::class,'id','parent_id');
    }
}
