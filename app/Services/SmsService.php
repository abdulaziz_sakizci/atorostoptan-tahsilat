<?php

namespace App\Services;

use App\Jobs\SendSms;
use Illuminate\Support\Facades\Log;

class SmsService
{
    protected $userName;
    protected $password;
    protected $originator;
    protected $charset;
    protected $url;

    public function __construct()
    {
        $this->userName   = env('MUTLUCELL_USERNAME');
        $this->password   = env('MUTLUCELL_PASSWORD');
        $this->originator = env('MUTLUCELL_ORIGINATOR');
        $this->charset    = env('MUTLUCELL_CHARSET');
        $this->url        = env('MUTLUCELL_URL');
    }

    private function generateXMLStringForSending($messages = [])
    {
        $smsXMLElement = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><smspack/>');
        $smsXMLElement->addAttribute('ka', $this->userName);
        $smsXMLElement->addAttribute('pwd', $this->password);
        $smsXMLElement->addAttribute('org', $this->originator);
        $smsXMLElement->addAttribute('charset', $this->charset);

        $message = $smsXMLElement->addChild('mesaj');
        $message->addChild('metin', $messages['text']);
        $message->addChild('nums', $messages['phone_number']);

        return str_replace(array("\r\n", "\n", "\r", "\t"), '', $smsXMLElement->asXML());
    }

    /**
     * @param $phoneNumber
     * @param $text
     * @return void
     */
    public function send($messages)
    {
        $xml_data = $this->generateXMLStringForSending($messages);

        // TODO try cathh içerisne alınmalı
        $ch = curl_init($this->url);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($ch);

        // output un il karakterini alıyoruz. dönen değerin ilk karakteri $ ile başlamıyorsa hata alınmış demektir.
        // eğer hata alınmış ise output çıktısı bu hatanın kodunu döndürmektedir.
        $firstCharacter = substr($output, 0,1);

        if ( $firstCharacter != '$' ) {
            $this->logError($output);
        }

        curl_close($ch);
    }

    /**
     * @param $output
     * @return void
     */
    private function logError($output)
    {
        $message = 'SMS Gönderim Hatası : Hata Kodu ';
        switch ($output) {

            case 20:
                Log::channel('smslog')->error($message . $output . ' - Post edilen xml eksik veya hatalı.');
                break;

            case 21:
                Log::channel('smslog')->error($message . $output . ' - Kullanılan originatöre sahip değilsiniz.');
                break;

            case 22:
                Log::channel('smslog')->error($message . $output . ' - Kontörünüz yetersiz.');
                break;

            case 23:
                Log::channel('smslog')->error($message . $output . ' - Kullanıcı adı ya da parolanız hatalı.');
                break;

            case 24:
                Log::channel('smslog')->error($message . $output . ' - Şu anda size ait başka bir işlem aktif.');
                break;

            case 25:
                Log::channel('smslog')->error($message . $output . ' - SMSC Stopped (Bu hatayı alırsanız, işlemi 1-2 dk sonra tekrar deneyin).');
                break;

            case 30:
                Log::channel('smslog')->error($message . $output . ' - Hesap Aktivasyonu sağlanmamış.');
                break;

            default:
                Log::channel('smslog')->error($message . $output . ' - Tanımlanamayan bir hata meydana geldi.');
        }
    }

    public function enqueuePaymentLink($phoneNumber, $paymentCode, $guardianName)
    {
        $smsBody = "Sayın " . $guardianName . ", ödeme linkiniz aşağıdadır. https://camlicatahsilat.com/payment/" . $paymentCode;

        $details = [
            'phone_number' => $phoneNumber,
            'text'         => $smsBody
        ];

        SendSms::dispatch($details);
    }

    public function enqueuePaymentSuccessfull($phoneNumber, $paymentCode, $guardianName)
    {
        $smsBody = "Sayın " . $guardianName . ", ödemeniz başarılı bir şekilde gerçekleşmiştir. Ödeme Kodu: " . $paymentCode;

        $details = [
            'phone_number' => $phoneNumber,
            'text'         => $smsBody
        ];

        SendSms::dispatch($details);
    }
}
