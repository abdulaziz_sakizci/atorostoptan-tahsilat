<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Organization
 * @package App
 *
 * @property int id
 * @property string name
 * @property string description
 * @property int organization_type_id
 * @property int parent_id
 * @property boolean is_leaf
 * @property string status
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @property-read OrganizationType organizationType
 * @property-read Collection|Corporation corporations
 * @property-read Organization parent
 * @property-read Collection getOrganizationsByParentId
 */
class Organization extends Model
{

    //Activity Log
    use LogsActivity;
    protected static $logAttributes = [];

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organizationType()
    {
        return $this->belongsTo(OrganizationType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function corporations()
    {
        return $this->hasMany(Corporation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parent()
    {
        return $this->hasOne(Organization::class, 'id', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function children()
    {
        return $this->hasMany(Organization::class, 'parent_id', 'id');
    }

    /**
     * @param $parentId
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getOrganizationsByParentId($parentId)
    {
        return self::query()
            ->where([
                ['parent_id', '=', $parentId ?? 1],
                ['status', '=', 'active']
            ])->get();
    }
}
