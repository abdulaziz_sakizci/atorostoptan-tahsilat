<?php

namespace App\Exports;

use App\Payment;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PaymentExport implements FromCollection ,WithHeadings,WithMapping
{
    use Exportable;
    private  $i=0;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($payment)
    {
        $this->payment = $payment;
    }

    public function collection()
    {

        return $this->payment;
    }

    public function map($payments): array
    {
        // This example will return 3 rows.
        // First row will have 2 column, the next 2 will have 1 column

        if ($payments->status == 'pending')
        {
            $status = __('admin/payment.index.pending');
        }
        if ($payments->status == 'paid')
        {
            $status = __('admin/payment.index.paid');
        }
        if ($payments->status == 'failed')
        {
            $status = __('admin/payment.index.failed');
        }
        if ($payments->status == 'cancelled')
        {
            $status = __('admin/payment.index.cancelled');
        }
        return [
            [
                ++$this->i,
                $payments->paid_at,
                $payments->corporation->code,
                $payments->corporation->organization->name,
                $payments->amount,
                $payments->corporation->description,
                $payments->student_name,
                $payments->bank_name,
                ($payments->installment == null || $payments->installment == 0) ? "Tek Çekim"  : $payments->installment,
                $status
            ],

        ];
    }

    public function headings(): array
    {
        return [
            'Sıra',
            'Tarih',
            'Cari Kod',
            'Üst Etiket',
            'Tutar',
            'Tabela Ünvanı',
            'Öğrenci Adı',
            'Hangi Banka',
            'Taksit Sayısı',
            'Durum',
        ];
    }

}
