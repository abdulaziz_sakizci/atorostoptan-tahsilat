<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProductType extends Model
{

    //Activity Log
    use LogsActivity;
    protected static $logAttributes = [];
    protected static $recordEvents = ['created','updated','deleted'];
    protected $guarded = [];

}
