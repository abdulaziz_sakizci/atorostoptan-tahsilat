<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Models\Role;
/**
 * Class OrganizationType
 * @package App
 *
 * @property int id
 * @property string name
 * @property int level
 * @property string code
 * @property string status
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @property-read Organization organizations
 */
class OrganizationType extends Model
{

    //Activity Log
    use LogsActivity;
    protected static $logAttributes = [];

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function organizations()
    {
        return  $this->hasMany(Organization::class);
    }
    public function roles()
    {
        return  $this->hasMany(Role::class);
    }
}
