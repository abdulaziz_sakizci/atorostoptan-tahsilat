<?php

namespace App;

use App\Scopes\PaymentScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Payment
 * @package App
 *
 * @property int id
 * @property string code
 * @property int corporation_id
 * @property string student_name
 * @property string guardian_name
 * @property double amount
 * @property string guardian_phone
 * @property string product_type_name
 * @property string description
 * @property int user_id
 * @property string bank_response
 * @property string status
 * @property Carbon paid_at
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @property-read Corporation corporation
 * @property-read User user
 */
class Payment extends Model
{

    //Activity Log
    use LogsActivity;
    protected static $logAttributes = [];
    protected static $recordEvents = ['created','updated','deleted'];
    protected $guarded = [];

    protected static function booted()
    {
        static::addGlobalScope(new PaymentScope());
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function corporation()
    {
        return $this->belongsTo(Corporation::class, 'corporation_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
