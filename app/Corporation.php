<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Corporation
 * @package App
 *
 * @property int id
 * @property string name
 * @property string description
 * @property int corporation_type_id
 * @property int organization_id
 * @property int geographic_boundary_id
 * @property int user_id
 * @property string status
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @property-read CorporationType corporationType
 * @property-read User user
 * @property-read Collection getCorporationByZoneId
 */
class Corporation extends Model
{

    //Activity Log
    use LogsActivity;
    protected static $logAttributes = [];
    protected static $recordEvents = ['created','updated','deleted'];

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function corporationType()
    {
        return $this->belongsTo(CorporationType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function geographicBoundary()
    {
        return $this->belongsTo(GeographicBoundary::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    /**
     * @param $zoneId
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getCorporationByOrganizationId($zoneId)
    {
        return self::query()
            ->where([
                ['organization_id', '=', $zoneId],
                ['status', '=', 'active']
            ])->get();
    }

}
