<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PaymentEmailForQueuing extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ( getenv('APP_ENV') == 'production' ) {

            return $this->from(getenv('MAIL_FROM_ADDRESS'), getenv('MAIL_FROM_NAME'))
                ->subject(getenv('MAIL_FROM_SUBJECT'))
                ->view('mail.template');

        }

        return $this->from(getenv('MAIL_FROM_ADDRESS_TEST'), getenv('MAIL_FROM_NAME_TEST'))
            ->subject(getenv('MAIL_FROM_SUBJECT_TEST'))
            ->view('mail.template');
    }
}
