<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        // finasnbank
        '/bank/finansbank/return',

        // denizbank
        '/bank/denizbank/return',

        // ziraatbank
        '/bank/ziraatbankasi/return',

        // isbankasi
        '/bank/isbankasi/return',

        // vakifbank
        '/bank/vakifbank/return',

        // akbank
        '/bank/akbank/return'
    ];
}
