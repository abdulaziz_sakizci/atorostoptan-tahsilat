<?php

namespace App\Http\Controllers;

use App\Exceptions\PaymentAmountDoesNotMatchException;
use App\Exceptions\PaymentNotFoundException;
use App\Payment;
use App\Services\SmsService;
use Carbon\Carbon;
use CardDetect\Detector;
use DOMDocument;
use Illuminate\Http\Request;

class BankController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function finansbank(Request $request)
    {
        $ccName = $request->input('ccName');
        $ccNumber = $request->input('ccNumber');
        $ccCVC = $request->input('ccCVC');
        $ccExpMonth = $request->input('ccExpMonth');
        $ccCVC = $request->input('ccCVC');
        $ccExpYear = $request->input('ccExpYear');
        $amount = $request->input('amount');

        $code = $request->input('code');

        $installmentCount = empty($request->input('installmentCount')) ? 0 : $request->input('installmentCount');

        saveBankRequest($code, $installmentCount, $ccNumber, $ccName, "FinansBank");

        return view('bank.finansbank',
            compact('ccName', 'ccNumber', 'ccCVC', 'ccExpMonth', 'ccExpYear', 'code', 'amount', 'installmentCount'));
    }


    public function finansbankReturn(Request $request, SmsService $smsService)
    {
        $payment = Payment::withoutGlobalScopes()->where('code', '=', $request->get('code'))->first();

        throw_unless($payment, new PaymentNotFoundException('Ödeme Bulunamadı'));
        throw_if('paid' == $payment->status, new PaymentNotFoundException('Bu Ödeme Gerçekleşmiş'));

        $bankErrorMessage = "";
        $bankSuccessMessage = "";

        $success = false;

        // 3d doğrulama başarılı
        if ($request->post('3DStatus') == "1") {
            $bankSuccessMessage = '3D Doğrulama Başarılı, ';
        } else {
            $bankErrorMessage = '3D Doğrulama Başarısız, ';
        }

        // ödeme başarılı
        if ($request->post('ProcReturnCode') == "00") {
            throw_if(
                str_replace(',', '.', $request->post('PurchAmount'))
                != $payment->amount,
                PaymentAmountDoesNotMatchException::class
            );

            $payment->status = 'paid';
            $payment->installment = $request->post('InstallmentCount') ?? 0;
            $payment->paid_at = Carbon::now();
            $bankSuccessMessage = 'Ödeme Başarılı';
            $success = true;

            $smsService->enqueuePaymentSuccessfull($payment->guardian_phone, $payment->code, $payment->guardian_name);
        } else {
            $payment->status = 'failed';
            $bankErrorMessage .= " Ödeme Başarısız";
        }

        $code = $request->get('code');
        $bankResponseJson = json_encode($request->all());

        $bank_request = json_decode($payment->bank_request);
        $installmentCount = $bank_request->installmentCount;

        $payment->bank_name = 'Finansbank';
        $payment->installment = ($installmentCount == null || $installmentCount == "TEK CEKIM") ? 0 : $installmentCount;
        $payment->bank_success_message = $bankSuccessMessage;
        $payment->bank_error_message = $bankErrorMessage;
        $payment->bank_response_code = $request->post('ProcReturnCode');
        $payment->bank_response = $bankResponseJson;
        $payment->save();

        return view('bank.finansbankReturn',
            compact("bankSuccessMessage", "bankErrorMessage", "bankResponseJson", "code", "success"));
    }

    public function akbank(Request $request, Detector $detector)
    {
        $ccName = $request->input('ccName');
        $ccNumber = $request->input('ccNumber');
        $ccExpMonth = $request->input('ccExpMonth');
        $ccCVC = $request->input('ccCVC');
        $ccExpYear = $request->input('ccExpYear');
        $amount = $request->input('amount');

        $code = $request->input('code');
        $installmentCount = empty($request->input('installmentCount')) ? 0 : $request->input('installmentCount');

        saveBankRequest($code, $installmentCount, $ccNumber, $ccName, "akbank");

        return view('bank.akbank',
            compact('ccName', 'ccNumber', 'ccCVC', 'ccExpYear', 'ccExpMonth', 'code', 'amount', 'installmentCount'));
    }

    public function akbankReturn(Request $request, SmsService $smsService)
    {
        $payment = Payment::withoutGlobalScopes()->where('code', '=', $request->get('code'))->first();

        throw_unless($payment, new PaymentNotFoundException('Ödeme Bulunamadı'));
        throw_if('paid' == $payment->status, new PaymentNotFoundException('Bu Ödeme Gerçekleşmiş'));

        $bankErrorMessage = "";
        $bankSuccessMessage = "";

        $success = false;

        // 3d doğrulama başarılı
        if ($request->post('mdStatus') == "1") {
            $bankSuccessMessage = '3D Doğrulama Başarılı, ';
        } else {
            $bankErrorMessage = '3D Doğrulama Başarısız, ';
        }

        // ödeme başarılı
        if ($request->post('Response') == "Approved") {

            throw_if(
                str_replace(',', '.', $request->post('amount'))
                != $payment->amount,
                PaymentAmountDoesNotMatchException::class
            );

            $payment->status = 'paid';
            $payment->paid_at = Carbon::now();
            $bankSuccessMessage = 'Ödeme Başarılı';
            $success = true;

            $smsService->enqueuePaymentSuccessfull($payment->guardian_phone, $payment->code, $payment->guardian_name);
        } else {
            $payment->status = 'failed';
            $bankErrorMessage .= " Ödeme Başarısız";
        }

        $code = $request->get('code');
        $bankResponseJson = json_encode($request->all());

        $payment->bank_name = 'AkBank';
        $payment->installment = ($request->post('taksit') == null) ? 0 : $request->post('taksit');
        $payment->bank_success_message = $bankSuccessMessage;
        $payment->bank_error_message = $bankErrorMessage;
        $payment->bank_response_code = $request->post('ProcReturnCode');
        $payment->bank_response = $bankResponseJson;

        $payment->save();

        return view('bank.akbankReturn',
            compact("bankSuccessMessage", "bankErrorMessage", "bankResponseJson", "code", "success"));
    }

    public function denizbank(Request $request, Detector $detector)
    {
        $ccName = $request->input('ccName');
        $ccNumber = $request->input('ccNumber');
        $ccExpMonth = $request->input('ccExpMonth');
        $ccCVC = $request->input('ccCVC');
        $ccExpYear = $request->input('ccExpYear');
        $amount = $request->input('amount');

        $detectedCcType = $detector->detect($ccNumber);

        $cardType = 0;
        if ($detectedCcType == "Visa") {
            $cardType = 0;
        } else {
            $cardType = 1;
        }
        $code = $request->input('code');
        $installmentCount = empty($request->input('installmentCount')) ? "" : $request->input('installmentCount');

        saveBankRequest($code, $installmentCount, $ccNumber, $ccName, "Denizbank");


        return view('bank.denizbank',
            compact('ccName', 'ccNumber', 'ccCVC', 'ccExpMonth', 'ccExpYear', 'code', 'amount', 'installmentCount', 'cardType'));
    }

    public function denizbankReturn(Request $request, SmsService $smsService)
    {
        $payment = Payment::withoutGlobalScopes()->where('code', '=', $request->get('code'))->first();

        throw_unless($payment, new PaymentNotFoundException('Ödeme Bulunamadı'));
        throw_if('paid' == $payment->status, new PaymentNotFoundException('Bu Ödeme Gerçekleşmiş'));

        $bankErrorMessage = "";
        $bankSuccessMessage = "";

        $success = false;

        // 3d doğrulama başarılı
        if ($request->post('3DStatus') == "1" ||
            $request->post('3DStatus') == "2" ||
            $request->post('3DStatus') == "3" ||
            $request->post('3DStatus') == "4"
        ) {
            $bankSuccessMessage = '3D Doğrulama Başarılı, ';
        } else {
            $bankErrorMessage = '3D Doğrulama Başarısız, ';
        }

        // ödeme başarılı
        if ($request->post('ProcReturnCode') == "00") {
            throw_if(
                str_replace(',', '.', $request->post('PurchAmount'))
                != $payment->amount,
                PaymentAmountDoesNotMatchException::class
            );

            $payment->status = 'paid';
            $payment->paid_at = Carbon::now();
            $bankSuccessMessage = 'Ödeme Başarılı';
            $success = true;

            $smsService->enqueuePaymentSuccessfull($payment->guardian_phone, $payment->code, $payment->guardian_name);
        } else {
            $payment->status = 'failed';
            $bankErrorMessage .= " Ödeme Başarısız";
        }

        $bank_request = json_decode($payment->bank_request);
        $installmentCount = $bank_request->installmentCount;

        $code = $request->get('code');
        $bankResponseJson = json_encode($request->all());

        $payment->bank_name = 'Denizbank';
        $payment->installment = ($installmentCount == null || $installmentCount == "TEK CEKIM") ? 0 : $installmentCount;
        $payment->bank_success_message = $bankSuccessMessage;
        $payment->bank_error_message = $bankErrorMessage;
        $payment->bank_response_code = $request->post('ProcReturnCode');
        $payment->bank_response = $bankResponseJson;
        $payment->save();

        return view('bank.denizbankReturn',
            compact("bankSuccessMessage", "bankErrorMessage", "bankResponseJson", "code", "success"));
    }

    public function ziraatbankasi(Request $request, Detector $detector)
    {
        $ccName = $request->input('ccName');
        $ccNumber = $request->input('ccNumber');
        $ccExpMonth = $request->input('ccExpMonth');
        $ccCVC = $request->input('ccCVC');
        $ccExpYear = $request->input('ccExpYear');
        $amount = $request->input('amount');

        $detectedCcType = $detector->detect($ccNumber);

        $cardType = 1;
        if ($detectedCcType == "Visa") {
            $cardType = 1;
        } else {
            $cardType = 2;
        }
        $code = $request->input('code');
        $installmentCount = empty($request->input('installmentCount')) ? 0 : $request->input('installmentCount');

        saveBankRequest($code, $installmentCount, $ccNumber, $ccName, "Ziraatbankasi");

        return view('bank.ziraatbankasi',
            compact('ccName', 'ccNumber', 'ccCVC', 'ccExpYear', 'ccExpMonth', 'cardType', 'code', 'amount', 'installmentCount'));
    }

    public function ziraatbankasiReturn(Request $request, SmsService $smsService)
    {

        $payment = Payment::withoutGlobalScopes()->where('code', '=', $request->get('code'))->first();

        throw_unless($payment, new PaymentNotFoundException('Ödeme Bulunamadı'));
        throw_if('paid' == $payment->status, new PaymentNotFoundException('Bu Ödeme Gerçekleşmiş'));

        $bankErrorMessage = "";
        $bankSuccessMessage = "";

        $success = false;

        // 3d doğrulama başarılı
        if ($request->post('mdStatus') == "1" ||
            $request->post('mdStatus') == "2" ||
            $request->post('mdStatus') == "3" ||
            $request->post('mdStatus') == "4"
        ) {
            $bankSuccessMessage = '3D Doğrulama Başarılı, ';
        } else {
            $bankErrorMessage = '3D Doğrulama Başarısız, ';
        }

        // ödeme başarılı
        if ($request->post('Response') == "Approved") {


            throw_if(
                str_replace(',', '.', $request->post('amount'))
                != $payment->amount,
                PaymentAmountDoesNotMatchException::class
            );


            $payment->status = 'paid';
            $payment->paid_at = Carbon::now();
            $bankSuccessMessage = 'Ödeme Başarılı';
            $success = true;

            $smsService->enqueuePaymentSuccessfull($payment->guardian_phone, $payment->code, $payment->guardian_name);
        } else {
            $payment->status = 'failed';
            $bankErrorMessage .= " Ödeme Başarısız";
        }

        $code = $request->get('code');
        $bankResponseJson = json_encode($request->all());

        $payment->bank_name = 'Ziraat Bankası';
        $payment->installment = ($request->post('taksit') == null) ? 0 : $request->post('taksit');
        $payment->bank_success_message = $bankSuccessMessage;
        $payment->bank_error_message = $bankErrorMessage;
        $payment->bank_response_code = $request->post('ProcReturnCode');
        $payment->bank_response = $bankResponseJson;

        $payment->save();

        return view('bank.ziraatbankasiReturn',
            compact("bankSuccessMessage", "bankErrorMessage", "bankResponseJson", "code", "success"));
    }

    public function isbankasi(Request $request, Detector $detector)
    {
        $ccName = $request->input('ccName');
        $ccNumber = $request->input('ccNumber');
        $ccExpMonth = $request->input('ccExpMonth');
        $ccCVC = $request->input('ccCVC');
        $ccExpYear = $request->input('ccExpYear');
        $amount = $request->input('amount');

        $detectedCcType = $detector->detect($ccNumber);

        $cardType = 1;
        if ($detectedCcType == "Visa") {
            $cardType = 1;
        } else {
            $cardType = 2;
        }
        $code = $request->input('code');
        $installmentCount = empty($request->input('installmentCount')) ? 0 : $request->input('installmentCount');

        saveBankRequest($code, $installmentCount, $ccNumber, $ccName, "İsbankasi");

        return view('bank.isbankasi',
            compact('ccName', 'ccNumber', 'ccCVC', 'ccExpYear', 'ccExpMonth', 'cardType', 'code', 'amount', 'installmentCount'));
    }

    public function isbankasiReturn(Request $request, SmsService $smsService)
    {
        $payment = Payment::withoutGlobalScopes()->where('code', '=', $request->get('code'))->first();

        throw_unless($payment, new PaymentNotFoundException('Ödeme Bulunamadı'));
        throw_if('paid' == $payment->status, new PaymentNotFoundException('Bu Ödeme Gerçekleşmiş'));

        $bankErrorMessage = "";
        $bankSuccessMessage = "";

        $success = false;

        // 3d doğrulama başarılı
        if ($request->post('mdStatus') == "1" ||
            $request->post('mdStatus') == "2" ||
            $request->post('mdStatus') == "3" ||
            $request->post('mdStatus') == "4"
        ) {
            $bankSuccessMessage = '3D Doğrulama Başarılı, ';
        } else {
            $bankErrorMessage = '3D Doğrulama Başarısız, ';
        }

        // ödeme başarılı
        if ($request->post('Response') == "Approved") {


            throw_if(
                str_replace(',', '.', $request->post('amount'))
                != $payment->amount,
                PaymentAmountDoesNotMatchException::class
            );


            $payment->status = 'paid';
            $payment->paid_at = Carbon::now();
            $bankSuccessMessage = 'Ödeme Başarılı';
            $success = true;

            $smsService->enqueuePaymentSuccessfull($payment->guardian_phone, $payment->code, $payment->guardian_name);
        } else {
            $payment->status = 'failed';
            $bankErrorMessage .= " Ödeme Başarısız";
        }

        $code = $request->get('code');
        $bankResponseJson = json_encode($request->all());

        $payment->bank_name = 'İş Bankası';
        $payment->installment = ($request->post('taksit') == null) ? 0 : $request->post('taksit');
        $payment->bank_success_message = $bankSuccessMessage;
        $payment->bank_error_message = $bankErrorMessage;
        $payment->bank_response_code = $request->post('ProcReturnCode');
        $payment->bank_response = $bankResponseJson;
        $payment->save();

        return view('bank.isbankasiReturn',
            compact("bankSuccessMessage", "bankErrorMessage", "bankResponseJson", "code", "success"));
    }

    public function vakifbank(Request $request, Detector $detector)
    {
        $MerchantId = "000000001225754"; //Üye İşyeri No
        $MerchantPassword = "w3JBy2p0"; //Üye İş Yeri Şifre

//        $MerchantId = "000100000013506"; //Üye İşyeri No
//        $MerchantPassword = "123456"; //Üye İş Yeri Şifre

        //Enrlloment Adresi
//        $mpiServiceUrl = "https://3dsecuretest.vakifbank.com.tr:4443/MPIAPI/MPI_Enrollment.aspx";
        $mpiServiceUrl = "https://3dsecure.vakifbank.com.tr:4443/MPIAPI/MPI_Enrollment.aspx";

        $payment = Payment::withoutGlobalScopes()->where('code', '=', $request->code)->first();

        $ccName = $request->ccName;
        $ccNumber = $request->ccNumber;
        $ccExpMonth = $request->ccExpMonth;
        $ccCVC = $request->ccCVC;
        $ccExpYear = $request->ccExpYear;
        $amount = $payment->amount;
        $code = $request->code;
        $unique_code = $request->code . "-" . time(); //code unique olmalıdır.bu yüzden her denemede değişmesi bankaya giden kodun için yapıyoruz.

        $amount = number_format(floatval($amount), 2, '.', '');

        $detectedCcType = $detector->detect($ccNumber);

        //Bankadan Gönderilen Cevap Mesajının Alındığı Fonksiyon
        $returnUrl = url('/bank/vakifbank/return?code=' . $code);

        // son kullanım tarihi //YIL:22 AY:11 İSE YIL.AY = 2211
        $ExpiryDate = $ccExpYear . $ccExpMonth;

        $cardType = 100;
        if ($detectedCcType == "Visa") {
            $cardType = 100;
        }
        if ($detectedCcType == "MasterCard") {
            $cardType = 200;
        }
        if ($detectedCcType == "Troy") {
            $cardType = 300;
        }

        $installmentCount = empty($request->input('installmentCount')) ? 0 : $request->input('installmentCount');

        //OPSİYONEL OLMAKLA BERABER BURADAKİ BİLGİ VPOST AŞAMASINDA KULLANILIYOR.
        $SessionInfo = "Pan=$ccNumber&Month=$ccExpMonth&Year=$ccExpYear&Cvv=$ccCVC&Amount=$amount&BrandName=$cardType";
        $SessionInfo = base64_encode($SessionInfo);

        $data = [
            'MerchantId' => $MerchantId,
            'MerchantPassword' => $MerchantPassword,
            'Pan' => $ccNumber,
            'ExpiryDate' => $ExpiryDate,
            'PurchaseAmount' => $amount,
            'Currency' => 949,//tl
            'BrandName' => $cardType,
            'SessionInfo' => $SessionInfo,
            'SuccessUrl' => $returnUrl,
            'FailureUrl' => $returnUrl,
            'VerifyEnrollmentRequestId' => $unique_code,
            'InstallmentCount' => $installmentCount,
        ];

        $sendEnrollmentData =
            "Pan=$ccNumber"
            . "&ExpiryDate=$ExpiryDate"
            . "&PurchaseAmount=$amount"
            . "&Currency=949"
            . "&BrandName=$cardType"
            . "&VerifyEnrollmentRequestId=$unique_code"
            . "&SessionInfo=$SessionInfo"
            . "&MerchantId=$MerchantId"
            . "&MerchantPassword=$MerchantPassword"
            . "&SuccessUrl=$returnUrl"
            . "&FailureUrl=$returnUrl";
        if ($installmentCount > 1) {
            $sendEnrollmentData .= "&InstallmentCount=$installmentCount";
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $mpiServiceUrl);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type" => "application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sendEnrollmentData);

        // İşlem isteği MPI'a gönderiliyor
        $result = curl_exec($ch);

        //$xml = simplexml_load_string($result, "SimpleXMLElement", LIBXML_NOCDATA);
        //$data = $xml->Message->VERes;

        $data = $this->ReadXml($result);

        if ($data['Status'] != "Y") {
            $success = false;
            $bankSuccessMessage = "";
            $bankErrorMessage = "Bir Hata Oluştu";


            return view('bank.vakifbankReturn',
                compact("bankSuccessMessage", "bankErrorMessage", "code", "success"));
        } else {
            saveBankRequest($code, $installmentCount, $ccNumber, $ccName, "Vakifbank");
        }

        return view('bank.vakifbankPARes', compact('data'));
    }

    //Bankadan Gelen Cevap FOnksiyonu
    public function vakifbankGetResponse(Request $request, SmsService $smsService)
    {
        $success = false;
        $bankErrorMessage = "";
        $bankSuccessMessage = "";

        //VPOST TEST ADRESİ
//        $PostUrl = "https://onlineodemetest.vakifbank.com.tr:4443/VposService/v3/Vposreq.aspx";

        //VPOST TEST ADRESİ
        $PostUrl = "https://onlineodeme.vakifbank.com.tr:4443/VposService/v3/Vposreq.aspx";

        $MerchantId = "000000001225754";
        $MerchantPassword = "w3JBy2p0";
        $TerminalNo = "VP290984";

//       TEST POST
//        $MerchantId = "000100000013506";
//        $MerchantPassword = "123456";
//        $TerminalNo = "VP000579";

        $status = $request->Status;

        $VerifyEnrollmentRequestId = $request->VerifyEnrollmentRequestId; //işlem numarası unique
        $code = explode("-", $VerifyEnrollmentRequestId);
        $code = $code[0];

        $Pan = $request->Pan; // Kredi Kartı numarası
        $Expiry = $request->Expiry;

        $PurchCurrency = $request->PurchCurrency;// Para kodu:
        // 949: TRY
        // 840: USD
        // 978: EUR
        // 826: GBP
        // JPY: 392

        $installmentCount = $request->InstallmentCount;// Taksit Sayısı

        $Xid = $request->Xid;// MPI tarafından üretilen 20 byte değerindeki alan. İşlem Sanal POS a gönderilir.
        $ECI = $request->Eci;// Bankalar arası kart merkezinden gelen Elektronik ticaret belirteci. Status Y ise 05 A ise 06 döner.
        $CAVV = $request->Cavv;// Bankalar arası kart merkezinden gelen (ACS) 28 byte büyüklüğünde kart sahibi doğrulama değeri.
        $PurchAmount = $request->PurchAmount;

        $payment = Payment::withoutGlobalScopes()->where('code', '=', $code)->first();

        // Bankadan geri dönüş oldu:
        if (isset($status) && !empty($status) && $status == "Y") {

            // Y:Doğrulama başarılı
            // A:Doğrulama tamamlanamadı ancak doğrulama denemesini kanıtlayan CAVV üretildi
            // U:Doğrulama tamamlanamadı
            // E:Doğrulama başarısız.
            // N:Doğrulama başarısız, işlem reddedildi

            //SessionInfo içindeki bilgileri alıyoruz.
            $sessionInfo = base64_decode($request->SessionInfo);
            parse_str($sessionInfo, $session);

            $ExpiryDate = "20" . $session['Year'] . $session['Month']; //2022 ve 11 ise 202211 şeklinde gönderilmeli
            $Cvv = $session['Cvv'];

            if ($session['BrandName'] == 100) {
                $installmentCount = ($installmentCount > 1) ? $installmentCount : 1;
            } else {
                $installmentCount = ($installmentCount > 1) ? $installmentCount : null;
            }

            $sendVposRequestData = 'prmstr=<VposRequest><MerchantId>'
                . $MerchantId . '</MerchantId><Password>'
                . $MerchantPassword . '</Password><TerminalNo>'
                . $TerminalNo . '</TerminalNo><TransactionType>Sale</TransactionType><CurrencyAmount>'
                . $session['Amount'] . '</CurrencyAmount><CurrencyCode>'
                . $PurchCurrency . '</CurrencyCode><Pan>'
                . $Pan . '</Pan><Expiry>'
                . $ExpiryDate . '</Expiry><Cvv>'
                . $Cvv . '</Cvv><ECI>'
                . $ECI . '</ECI><CAVV>'
                . $CAVV . '</CAVV><MpiTransactionId>'
                . $VerifyEnrollmentRequestId . '</MpiTransactionId><ClientIp>'
                . getRealIpAddr() . '</ClientIp><TransactionDeviceSource>0</TransactionDeviceSource>';

            if ($installmentCount > 1) {
                $sendVposRequestData .= '<NumberOfInstallments>' . $installmentCount . '</NumberOfInstallments></VposRequest>';
            } else {
                $sendVposRequestData .= '</VposRequest>';
            }

            if ($payment->status != "paid")
            {
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $PostUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $sendVposRequestData);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 5);
                curl_setopt($ch, CURLOPT_TIMEOUT, 59);
                curl_setopt($ch, CURLOPT_SSLVERSION, 5);

                $response = curl_exec($ch);
                curl_close($ch);

                $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
                $json = json_encode($xml);
                $result = json_decode($json, TRUE);//Bankadan gelen son mesajın arrayı

                $convert = (string)$payment->amount;

                if ($result['ResultCode'] == "0000") {
                    throw_if(
                        str_replace(',', '.', $result['CurrencyAmount'])
                        != $convert,
                        PaymentAmountDoesNotMatchException::class
                    );
                    $success = true;
                    $payment->status = 'paid';
                    $payment->paid_at = Carbon::now();
                    $bankSuccessMessage = 'Ödeme Başarılı';

                    $smsService->enqueuePaymentSuccessfull($payment->guardian_phone, $payment->code, $payment->guardian_name);
                } else {
                    $payment->status = 'failed';
                    $bankErrorMessage .= " Ödeme Başarısız";
                }

                $bankResponseJson = $json;

                $payment->bank_name = 'Vakıfbank';
                $payment->installment = ($installmentCount == null) ? 0 : $installmentCount;
                $payment->bank_success_message = $bankSuccessMessage;
                $payment->bank_error_message = $bankErrorMessage;
                $payment->bank_response_code = $result['ResultCode'];
                $payment->bank_response = $bankResponseJson;
                $payment->save();

                return view('bank.vakifbankReturn',
                    compact("bankSuccessMessage", "bankErrorMessage", "bankResponseJson", "code", "success"));
            }

        } else {
            $bankSuccessMessage = "";
            $bankErrorMessage = "Bir Hata Oluştu";

            return view('bank.vakifbankReturn',
                compact("bankSuccessMessage", "bankErrorMessage", "code", "success"));
        }
    }

    private function readXml($result)
    {
        $resultDocument = new DOMDocument();
        $resultDocument->loadXML($result);

        //Status Bilgisi okunuyor
        $statusNode = $resultDocument->getElementsByTagName("Status")->item(0);
        $status = "";
        if ($statusNode != null)
            $status = $statusNode->nodeValue;

        //PAReq Bilgisi okunuyor
        $PAReqNode = $resultDocument->getElementsByTagName("PaReq")->item(0);
        $PaReq = "";
        if ($PAReqNode != null)
            $PaReq = $PAReqNode->nodeValue;

        //ACSUrl Bilgisi okunuyor
        $ACSUrlNode = $resultDocument->getElementsByTagName("ACSUrl")->item(0);
        $ACSUrl = "";
        if ($ACSUrlNode != null)
            $ACSUrl = $ACSUrlNode->nodeValue;

        //Term Url Bilgisi okunuyor
        $TermUrlNode = $resultDocument->getElementsByTagName("TermUrl")->item(0);
        $TermUrl = "";
        if ($TermUrlNode != null)
            $TermUrl = $TermUrlNode->nodeValue;

        //MD Bilgisi okunuyor
        $MDNode = $resultDocument->getElementsByTagName("MD")->item(0);
        $MD = "";
        if ($MDNode != null)
            $MD = $MDNode->nodeValue;

        //MessageErrorCode Bilgisi okunuyor
        $messageErrorCodeNode = $resultDocument->getElementsByTagName("MessageErrorCode")->item(0);
        $messageErrorCode = "";
        if ($messageErrorCodeNode != null)
            $messageErrorCode = $messageErrorCodeNode->nodeValue;

        // Sonuç dizisi oluşturuluyor
        $result = array
        (
            "Status" => $status,
            "PaReq" => $PaReq,
            "ACSUrl" => $ACSUrl,
            "TermUrl" => $TermUrl,
            "MerchantData" => $MD,
            "MessageErrorCode" => $messageErrorCode
        );
        return $result;
    } // readXml sonu

}
