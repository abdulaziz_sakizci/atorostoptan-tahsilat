<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use Illuminate\Http\Request;

class JobController extends Controller
{
    /**
     * @param Request $request
     */
    public function enqueue(Request $request)
    {
        $details = [
            'email' => $request->mail
        ];

        SendEmail::dispatch($details);
    }
}
