<?php

namespace App\Http\Controllers;

use App\Jobs\SendSms;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    public function enqueue(Request $request)
    {
        $details = [
            'phone_number' => $request->phoneNumber,
            'text'         => $request->text
        ];

        SendSms::dispatch($details);
    }
}
