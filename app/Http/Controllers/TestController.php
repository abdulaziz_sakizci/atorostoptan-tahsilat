<?php

namespace App\Http\Controllers;

use App\Corporation;
use App\CorporationType;
use App\GeographicBoundary;
use App\Jobs\SendEmail;
use App\Organization;
use App\OrganizationType;
use App\User;
use Illuminate\Http\Request;
use Omnipay\Omnipay;

class TestController extends Controller
{
    /**
     * @param Request $request
     * @return string
     */
    public function index(Request $request)
    {
//        $gateway = Omnipay::create('NestPay');
//
//        $gateway->setBank("finansbank");
//        $gateway->setUserName("aurora");
//        $gateway->setClientId("V1700029");
//        $gateway->setPassword("3u4Jw");
//        //$gateway->setTestMode(TRUE);
//
//        $options = [
//            'number' => '5406675406675403',
//            'expiryMonth' => '12',
//            'expiryYear' => '2022',
//            'cvv' => '000',
//            'email' => 'yasinkuyu@gmail.com',
//            'firstname' => 'Yasin',
//            'lastname' => 'Kuyu'
//        ];
//
//        try {
//            $response = $gateway->purchase(
//                [
//                    //'installment'  => '', # Taksit
//                    //'moneypoints'  => 1.00, // Set money points (Maxi puan gir)
//                    'amount' => 1.00,
//                    'type' => 'Auth',
//                    'transactionId' => 'ORDER-3651233ss',
//                    'currency' => 'TRY',
//                    'card' => $options
//                ]
//            )->send();
//        } catch (\Exception $e) {
//            exit($e->getMessage());
//        }
//        // Debug
//        var_dump($response);
//        return 'asd';
    }

    public function test3()
    {

        $listPath = storage_path('app/import/list.csv');

        //Open the file.
        $fileHandle = fopen($listPath, "r");

        //Loop through the CSV rows.
        $rowNumber = 1;
        while (($row = fgetcsv($fileHandle, 0, ";")) !== FALSE) {

            if ($rowNumber++ == 1) {
                continue;
            }

            if ($rowNumber == 500) {
                die();
            }

            echo $row[1] . "<br>";

            $corpType = CorporationType::firstOrCreate(
                ['name' => $row[0]],
                ['name' => $row[0]]
            );

            // echo $row[4];

            $bolge = Organization::where('name', $row[4])->first();

            $mintika = Organization
                ::where('name', '=', $row[5])
                ->where('parent_id', '=', $bolge->id)
                ->first();

            if (!$mintika->name) {
                die('mintika bulunamadı !');
            }

            echo $bolge->name . " -> " . $mintika->name . "<br>";

            $il = GeographicBoundary
                ::where('name', $this->trStrtoupper($row[6]))
                ->where('parent_id', 1)
                ->first();

            $ilce = GeographicBoundary
                ::where('name', $this->trStrtoupper($row[7]))
                ->where('parent_id', $il->id)
                ->first();

            if (!$ilce->name) {
                die('ilçe bulunamadı');
            }

            echo $il->name . " -> " . $ilce->name . "<br>";


            $organization = Organization::firstOrCreate(
                [
                    'name' => $row[3],
                    'parent_id' => $mintika->id
                ],
                [
                    'name' => $row[3],
                    'parent_id' => $mintika->id,
                    'description' => $row[1],
                    'organization_type_id' => 4,
                    'is_leaf' => 1,
                    'status' => 'active'
                ]
            );

            echo $organization->name . "<br>";


            $corp = Corporation::firstOrCreate(
                [
                    'name' => $row[3],
                    'organization_id' => $organization->id
                ],
                [
                    'name' => $row[3],
                    'code' => $row[2],
                    'description' => $row[1],
                    'corporation_type_id' => $corpType->id,
                    'organization_id' => $organization->id,
                    'geographic_boundary_id' => $ilce->id,
                    'status' => 'active'
                ]
            );

            $user = User::firstOrCreate(
                [
                    'email' => $row[9]
                ],
                [
                    'name' => $row[8],
                    'email' => $row[9],
                    'password' => bcrypt($row[10]),
                    'corporation_id' => $corp->id,
                ]
            );

            $user->assignRole($organization->id, 'Kurum Muhasebe');


            //echo ($corpType->name);
            echo '<br>';
            echo "-------------<br>";
            //die();

        }

        // return $listPath;
    }

    public function trStrtoupper($text)
    {
        $search = array("ç", "i", "ı", "ğ", "ö", "ş", "ü");
        $replace = array("Ç", "İ", "I", "Ğ", "Ö", "Ş", "Ü");
        $text = str_replace($search, $replace, $text);
        $text = strtoupper($text);
        return $text;
    }

}
