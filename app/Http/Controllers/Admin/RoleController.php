<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EditRolePost;
use App\Http\Requests\StoreRolePost;
use App\OrganizationType;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if ( !Auth::user()->can('role operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $roles = Role::all();

        return view('admin.role.index')->with(compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        if ( !Auth::user()->can('role operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $permissions = Permission::all();
        $organizationTypes = OrganizationType::all();

        return view('admin.role.create')->with(compact('permissions', 'organizationTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRolePost $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreRolePost $request)
    {
        if ( !Auth::user()->can('role operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $role = Role::create([
            'name' => $request->role,
            'organization_type_id' => $request->organization_type
        ]);

        if($role){

            $permissions = Permission::whereIn('id', $request->permissions)->get();

            $role->syncPermissions($permissions);

            activity()
                ->performedOn($role)
                ->causedBy(auth()->user())
                ->log('created');


            return redirect(route('role.index'))->with('success','İşlem başarılı.');
        }

        return back()->with('error','İşlem Başarısız.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        if ( !Auth::user()->can('role operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $role = Role::findById($id);

        $permissions = Permission::all();
        $rolePermissionIds = $role->permissions->pluck('id')->toArray();
        $organizationTypes = OrganizationType::all();

        $organizationTypesName = OrganizationType::where('id',$role->organization_type_id)->first()->name;

        return view('admin.role.edit')->with(compact('role', 'rolePermissionIds', 'permissions','organizationTypes',
            'organizationTypesName'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditRolePost $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditRolePost $request, $id)
    {
        if ( !Auth::user()->can('role operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }


        $role = Role::findById($id);

        $role->name = $request->role;
        $role->organization_type_id = $request->organization_type;
        $role->save();

        activity()
            ->performedOn($role)
            ->causedBy(auth()->user())
            ->log('updated');



        if ($role) {

            $permissions = Permission::whereIn('id', $request->permissions)->get();

            $role->syncPermissions($permissions);

            return back()->with('success', 'İşlem Başarılı');
        }

        return back()->with('error', 'İşlem Başarısız');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if ( !Auth::user()->can('role operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $role = Role::findById($id);

        if ($role->delete()) {

            return true;

            activity()
                ->performedOn($role)
                ->causedBy(auth()->user())
                ->log('deleted');
        }

        return false;
    }
}
