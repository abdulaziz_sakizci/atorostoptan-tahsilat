<?php

namespace App\Http\Controllers\Admin;

use App\Payment;
use App\Http\Controllers\Controller;
use App\Corporation;
use App\Organization;
use App\GeographicBoundary;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App;
use App\ProductType;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Integer;
use App\Http\Requests\StorePaymentPost;
use App\Http\Requests\UpdatePaymentPost;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Services\SmsService;

use App\User;
use Spatie\Activitylog\Models\Activity;
use Spatie\Permission\Models\Role;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if (!Auth::user()->can('list payments')) {

            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $organizationTypeId = Auth::user()->roles()->first()->organization_type_id;

        $bolge_id = intval($request->get('bolge_id')) ?? 1;

        $mintika_id = intval($request->get('mintika_id')) ?? 1;

        $status = $request->get('payment_status') ?? null;


        //dd($payment_status);

        $bolges = Organization::where([
            ['parent_id', '=', 1],
            ['status', '=', 'active']
        ])->get();

        $get_mintika=Organization::where('parent_id',$bolge_id)->get();

        if ($bolge_id) //bölge ile beraber mıntıka veya durum seçildiyse
        {
            if ($mintika_id)//bölge ve mıntıka seçildiyse
            {
                //Organizasyon tablosundan seçilen mıntıka ve bu mıntıkaya bağlı kurumların id si alınıyor
                $organizations=Organization::where('id',$mintika_id)->orWhere('parent_id',
                    $mintika_id)->pluck('id');

                //kurumların(anaokulu,özel okul vs.) bağlı oldukları mıntıka veya kurumların yukardan gelen id lere göre bulunması
                $corparation=Corporation::whereIn('organization_id',$organizations)->pluck('id');

                //Eğer bölge , mıntıka ve durum hepsi birlikte seçildiyse
                if ($status)
                {
                    //idlere göre alınan kurumlar üzerinden giden ödemelerin alınması
                    $payments= Payment::whereIn('corporation_id',$corparation)->where('status',
                        $status)->paginate(20);
                }
                else//sadece bölge ve mıntıka birlikte seçildiyse
                {
                    $payments= Payment::whereIn('corporation_id',$corparation)->paginate(20);
                    //dd($payments);
                }

            }
            else//sadece bölge veya bölge ile beraber durum seçildiyse
            {
                //bölgenin altındaki mıntıkalar alınıyor
                $mintikalar=Organization::where('parent_id',$bolge_id)->pluck('id');

                //seçili bölge ,bölgenin altındaki mıntıkalar ve o mımtıkaların altındaki kurumlar alınıyor
                $organizations=Organization::where('id',$bolge_id)->orWhere('parent_id',
                    $bolge_id)->whereIn('parent_id',$mintikalar,'or')->pluck('id');

                //alınan yerlere göre kurumlar filtreleniyor.
                $corparation=Corporation::whereIn('organization_id',$organizations)->pluck('id');

                //eğer bölge ve durum seçildiyse mıntıka seçilmediyse
                if ($status)
                {
                    //alınan kurum bilgilerine göre bölgeler ve durum bilgisine göre ödemeler listeleniyor
                    $payments= Payment::whereIn('corporation_id',$corparation)->where('status',
                        $status)->paginate(20);
                }
                else// sadece bölge seçildiyse
                {
                    $payments= Payment::whereIn('corporation_id',$corparation)->paginate(20);
                }
            }
            $payments->setPath('payment?bolge_id='.$bolge_id.'&mintika_id='.$mintika_id.'&payment_status='.$status.'');

        }//sadece durum seçildiyse
        elseif ($status)
        {
            //dd($payment_status);
            $payments= Payment::where('status',$status)->paginate(20);
            $payments->setPath('payment?bolge_id='.$bolge_id.'&mintika_id='.$mintika_id.'&payment_status='.$status.'');
        }
        else
        {
            $payments = Payment::paginate(20);
        }

        $skipped = ($payments->currentPage() * $payments->perPage()) - $payments->perPage();

        return view('admin.payment.index')->with(compact('payments', 'skipped','bolges',
            'bolge_id','status','get_mintika','mintika_id','organizationTypeId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        if (!Auth::user()->can('create update payment')) {

            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $products = ProductType::where([
            ['status', '=', 'active']
        ])->get();

        // gb tablosundan illeri alir
        $cities = GeographicBoundary::where([
            ['parent_id', '=', 1],
            ['status', '=', 'active']
        ])->get();

        return view('admin.payment.create',
            [
                'products' => $products,

            ]
        );
    }

    /**
     * @param StorePaymentPost $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(StorePaymentPost $request, SmsService $smsService)
    {
        if (!Auth::user()->can('create update payment')) {

            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        do {
            $code = Str::random(10);
        } while (DB::table('payments')->where('code', $code)->exists());

        $userRole = Auth::user()->roles()->first();
        $organizationId = DB::table('model_has_roles')
            ->where([
                'role_id' => $userRole->id,
                'model_id' => Auth::user()->id
            ])
            ->first()
            ->organization_id;

        $corporation = Corporation::where('organization_id', $organizationId)->first();

        $payment = Payment::create([
            "code" => $code,
            "corporation_id" => $corporation->id,
            "student_name" => $request->student_name,
            "guardian_name" => $request->guardian_name,
            "amount" => $request->amount,
            "guardian_phone" => $request->guardian_phone,
            "product_type_name" => $request->product_type_name,
            "description" => $request->description,
            "user_id" =>Auth::id()
        ]);

        if (!empty($request->smsGonder)) {
            $smsService->enqueuePaymentLink($request->guardian_phone, $code, $request->guardian_name);
        }

        if ($payment) {
            return redirect(route('payment.index'))
                ->with('success', 'işlem başarılı');

        }
        return back()->with('error', 'İşlem Başarısız');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::user()->can('list payments')) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $payment = Payment::find($id);

        if ($payment->status != 'paid' && $payment->status != 'failed') {
            return back()->with('error', 'Henüz ödemesi yapılmayan bir işlemin detayına ulaşamazsınız.');
        }

        $bank_request = empty($payment->bank_request) ? '[]' : $payment->bank_request;
        $bank_request = json_decode($bank_request, true);

        $bankResponse = empty($payment->bank_response) ? '[]' : $payment->bank_response;
        $bankResponse = json_decode($bankResponse, true);

        return view('admin.payment.details', compact('payment', 'bankResponse','bank_request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $products = ProductType::where([
            ['status', '=', 'active']
        ])->get();

        $payment = Payment::where('id', '=', $id)->first();

        if ($payment->status != 'paid') {
            return view('admin.payment.edit')
                ->with('payments', $payment)
                ->with('products', $products);
        }

        return back();

    }


    public function update(UpdatePaymentPost $request, $id)
    {
        $payments = Payment::where('id', $id)->update(
            [
                "student_name" => $request->student_name,
                "guardian_name" => $request->guardian_name,
                "amount" => $request->amount,
                "guardian_phone" => $request->guardian_phone,
                "product_type_name" => $request->product_type_name,
                "description" => $request->description,
                "user_id" => Auth::id(),

            ]);

        if ($payments) {
            //activity log
            $modal = Payment::where('id', $id)->first();
            activity()
                ->performedOn($modal)
                ->causedBy(auth()->user())
                ->log('updated');

            return back()->with('success', 'İşlem Başarılı');
        }
        return back()->with('error', 'İşlem Başarısız');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payment = Payment::find($id);
        if ($payment->status != 'paid') {
            $payment->delete();
            return true;
        }
        return false;

    }

    /**
     * @param $id
     * @return bool
     */
    public function cancelPayment($id)
    {
        $payment = Payment::find($id);
        if ($payment->status == 'pending') {
            $payment->status = "cancelled";
            $payment->save();
            return true;
        }
        return false;
    }
}
