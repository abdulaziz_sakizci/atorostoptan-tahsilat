<?php

namespace App\Http\Controllers\Admin;

use App\ProductType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( !Auth::user()->can('product operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $data = ProductType::paginate(10);
        return view('admin.product.index')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( !Auth::user()->can('product operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( !Auth::user()->can('product operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $rules = [
            'name' => 'required',
            'status' => 'required',
        ];
        $customMessages = [
            'name.required' => 'Lütfen Ürün Tipi Alanını Doldurunuz! ',
            'status.required' => 'Lütfen Durum Seçeneğini  İşaretleyiniz!'
        ];
        $this->validate($request, $rules, $customMessages);




        $product = ProductType::create([
            "name" => $request->name,
            "status" => $request->status,
        ]);


        if ($product) {
            return redirect(route('product.index'))->with('success', 'işlem başarılı');
        }
        return back()->with('error', 'İşlem Başarısız');
    }


    /**
     * Display the specified resource.
     *
     * @param \App\ProductType $productType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\ProductType $productType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( !Auth::user()->can('product operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $products = ProductType::where('id', $id)->first();
        return view('admin.product.edit')->with('products', $products);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\ProductType $productType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( !Auth::user()->can('product operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $rules = [
            'name' => 'required',
            'status' => 'required',
        ];
        $customMessages = [
            'name.required' => 'Lütfen Ürün Tipi Alanını Doldurunuz! ',
            'status.required' => 'Lütfen Durum Seçeneğini  İşaretleyiniz!'
        ];
        $this->validate($request, $rules, $customMessages);

        $productType = ProductType::where('id', $id)->update(
            [
                "name" => $request->name,
                "status" => $request->status,
            ]
        );
        if ($productType) {
            $modal = ProductType::where('id', $id)->first();
            activity()
                ->performedOn($modal)
                ->causedBy(auth()->user())
                ->log('updated');

            return back()->with('success', 'İşlem Başarılı');
        }
        return back()->with('error', 'İşlem Başarısız');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\ProductType $productType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( !Auth::user()->can('product operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $product = ProductType::find($id);
        if ($product->delete()) {
            return true;
        }
        return false;

    }
}
