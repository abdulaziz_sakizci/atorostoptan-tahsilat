<?php

namespace App\Http\Controllers\Admin;

use App\Exports\PaymentExport;
use App\Payment;
use App\Http\Controllers\Controller;
use App\Corporation;
use App\Organization;
use App\GeographicBoundary;
use Illuminate\Http\Request;
use App;
use App\ProductType;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use phpDocumentor\Reflection\Types\Integer;
use App\Http\Requests\StorePaymentPost;
use App\Http\Requests\UpdatePaymentPost;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Services\SmsService;

use App\User;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if (!Auth::user()->can('report')) {

            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $organizationTypeId = Auth::user()->roles()->first()->organization_type_id;

        $bolge_id = intval($request->get('bolge_id')) ?? 1;

        $mintika_id = intval($request->get('mintika_id')) ?? 1;

        $status = $request->get('payment_status') ?? null;

        $export = $request->get('excel') ?? null;


        //dd($payment_status);

        $bolges = Organization::where([
            ['parent_id', '=', 1],
            ['status', '=', 'active']
        ])->get();

        $get_mintika = Organization::where('parent_id', $bolge_id)->get();

        if ($bolge_id) //bölge ile beraber mıntıka veya durum seçildiyse
        {
            if ($mintika_id)//bölge ve mıntıka seçildiyse
            {
                //Organizasyon tablosundan seçilen mıntıka ve bu mıntıkaya bağlı kurumların id si alınıyor
                $organizations = Organization::where('id', $mintika_id)->orWhere('parent_id',
                    $mintika_id)->pluck('id');

                //kurumların(anaokulu,özel okul vs.) bağlı oldukları mıntıka veya kurumların yukardan gelen id lere göre bulunması
                $corparation = Corporation::whereIn('organization_id', $organizations)->pluck('id');

                //Eğer bölge , mıntıka ve durum hepsi birlikte seçildiyse
                if ($status) {
                    if ($export) {
                        $payments = Payment::whereIn('corporation_id', $corparation)->where('status',
                            $status)->get();
                    } else {
                        $payments = Payment::whereIn('corporation_id', $corparation)->where('status',
                            $status)->paginate(20);
                        $payments->setPath('report?bolge_id=' . $bolge_id . '&mintika_id=' . $mintika_id . '&payment_status=' . $status . '');

                    }
                    //idlere göre alınan kurumlar üzerinden giden ödemelerin alınması


                } else//sadece bölge ve mıntıka birlikte seçildiyse
                {
                    if ($export) {
                        $payments = Payment::whereIn('corporation_id', $corparation)->get();
                    } else {
                        $payments = Payment::whereIn('corporation_id', $corparation)->paginate(20);
                        $payments->setPath('report?bolge_id=' . $bolge_id . '&mintika_id=' . $mintika_id . '&payment_status=' . $status . '');

                    }
                    //dd($payments);
                }

            } else//sadece bölge veya bölge ile beraber durum seçildiyse
            {
                //bölgenin altındaki mıntıkalar alınıyor
                $mintikalar = Organization::where('parent_id', $bolge_id)->pluck('id');

                //seçili bölge ,bölgenin altındaki mıntıkalar ve o mımtıkaların altındaki kurumlar alınıyor
                $organizations = Organization::where('id', $bolge_id)->orWhere('parent_id',
                    $bolge_id)->whereIn('parent_id', $mintikalar, 'or')->pluck('id');

                //alınan yerlere göre kurumlar filtreleniyor.
                $corparation = Corporation::whereIn('organization_id', $organizations)->pluck('id');

                //eğer bölge ve durum seçildiyse mıntıka seçilmediyse
                if ($status) {
                    if ($export) {
                        $payments = Payment::whereIn('corporation_id', $corparation)->where('status',
                            $status)->get();
                    } else {
                        $payments = Payment::whereIn('corporation_id', $corparation)->where('status',
                            $status)->paginate(20);
                        $payments->setPath('report?bolge_id=' . $bolge_id . '&mintika_id=' . $mintika_id . '&payment_status=' . $status . '');

                    }
                    //alınan kurum bilgilerine göre bölgeler ve durum bilgisine göre ödemeler listeleniyor

                } else// sadece bölge seçildiyse
                {
                    if ($export) {
                        $payments = Payment::whereIn('corporation_id', $corparation)->get();
                    } else {
                        $payments = Payment::whereIn('corporation_id', $corparation)->paginate(20);
                        $payments->setPath('report?bolge_id=' . $bolge_id . '&mintika_id=' . $mintika_id . '&payment_status=' . $status . '');
                    }
                }

            }
        }//sadece durum seçildiyse
        elseif ($status) {
            //dd($payment_status);
            if ($export) {
                $payments = Payment::where('status', $status)->get();
            } else {
                $payments = Payment::where('status', $status)->paginate(20);
                $payments->setPath('report?bolge_id=' . $bolge_id . '&mintika_id=' . $mintika_id . '&payment_status=' . $status . '');
            }

        } else {
            if ($export) {
                $payments = Payment::all();
            } else {
                $payments = Payment::paginate(20);
            }

        }

        if ($export) {
            return Excel::download(new PaymentExport($payments), 'users.xlsx');
        }
        $skipped = ($payments->currentPage() * $payments->perPage()) - $payments->perPage();

        return view('admin.report.index')->with(compact('payments', 'skipped', 'bolges',
            'bolge_id', 'status', 'get_mintika', 'mintika_id', 'organizationTypeId'));
    }

}
