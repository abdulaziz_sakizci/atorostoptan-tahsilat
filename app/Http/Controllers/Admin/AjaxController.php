<?php

namespace App\Http\Controllers\Admin;

use App;
use App\GeographicBoundary;
use App\Http\Controllers\Controller;
use App\Jobs\SendSms;
use App\Organization;
use App\Corporation;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use App\Services\SmsService;

/**
 * Class AjaxController
 * @package App\Http\Controllers\Admin
 *
 * @property-read Collection|GeographicBoundary getGeographicBoundriesByParentId
 * @property-read Collection|Organization getOrganizationsByParentId
 * @property-read Collection|Role getRolesByOrganizationId
 * @property-read Collection|Corporation getCorporationByOrganizationId
 */
class AjaxController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|string
     */
    public function getGeographicBoundriesByParentId(Request $request)
    {
        if (!$request->ajax()){
            return "Ajax isteğinde bir hata oluştu.";
        }
        // gb tablosundan verileri alir
        $geographicBoundaries = GeographicBoundary::where([
            ['parent_id', '=', $request->parent_id ?? 1],
            ['status', '=', 'active']
        ])->get();

        return $geographicBoundaries;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|string
     */
    public function getOrganizationsByParentId(Request $request)
    {
        if (!$request->ajax()){
            return "Ajax isteğinde bir hata oluştu.";
        }
        // organizations tablosundan verileri alir
        $organizations = Organization::getOrganizationsByParentId($request->parent_id);

        return $organizations;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|string
     */
    public function getRolesByOrganizationId(Request $request)
    {
        if (!$request->ajax()){
            return "Ajax isteğinde bir hata oluştu.";
        }

        $role_id = Auth::user()->roles()->first()->id;

        if ($role_id != 1)//kullanıcı çamlıca muhasebe ise rol inputuna sadece çamlıca muhasebe gelsin
        {
            $roles = Role::where('organization_type_id', $request->organization_type_id)->where('id','!=',1)->get();
        }
        else
        {
            $roles = Role::where('organization_type_id', $request->organization_type_id)->get();
        }

        return $roles;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|string
     */
    public function getCorporationByOrganizationId(Request $request)
    {
        if (!$request->ajax()){
            return "Ajax isteğinde bir hata oluştu.";
        }
        // corporations tablosundan verileri alir
        $corporations = Corporation::getCorporationByOrganizationId($request->zone_id);

        return $corporations;
    }

    public function enqueueSms(Request $request, SmsService $smsService)
    {
        if (!$request->ajax()){
            return "Ajax isteğinde bir hata oluştu.";
        }

        $smsService->enqueuePaymentLink($request->phoneNumber, $request->paymentCode, $request->guardianName);

        return [
            "status" => "success"
        ];
    }
}
