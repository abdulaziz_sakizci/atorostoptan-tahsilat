<?php

namespace App\Http\Controllers\Admin;

use App;
use App\Corporation;
use App\GeographicBoundary;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCorporationPost;
use App\Http\Requests\UpdateCorporationPost;
use App\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class CorporationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */

    public function index(Request $request)
    {
        if (!Auth::user()->can('corporation operations')) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }
        $sort_criterion = $request->get('sort');
        $sorting = $request->get('sorting');
        $organizationTypeId = Auth::user()->roles()->first()->organization_type_id;
        $bolge_id = intval($request->get('bolge_id')) ?? 1;
        $mintika_id = intval($request->get('mintika_id')) ?? 1;
        $status = $request->get('corporation_status') ?? null;

        $bolges = Organization::where([
            ['parent_id', '=', 1],
            ['status', '=', 'active']
        ])->get();

        $get_mintika = Organization::where('parent_id', $bolge_id)->get();

        if ($bolge_id) //bölge ile beraber mıntıka veya durum seçildiyse
        {
            if ($mintika_id)//bölge ve mıntıka seçildiyse
            {
                //Organizasyon tablosundan seçilen mıntıka ve bu mıntıkaya bağlı kurumların id si alınıyor
                $organizations = Organization::where('id', $mintika_id)->orWhere('parent_id',
                    $mintika_id)->pluck('id');

                //kurumların(anaokulu,özel okul vs.) bağlı oldukları mıntıka veya kurumların yukardan gelen id lere göre bulunması
                $corparation = Corporation::whereIn('organization_id', $organizations)->pluck('id');

                //Eğer bölge , mıntıka ve durum hepsi birlikte seçildiyse
                if ($status) {
                    //idlere göre alınan kurumlar üzerinden giden ödemelerin alınması
                    if ($sorting == 'asc' || $sorting == 'desc') {
                        if ($request->get('sort') == "code_sort") {
                            $corporations = Corporation::whereIn('id', $corparation)->where('status', $status)->orderBy('code', $sorting)->paginate(10);
                        } elseif ($request->get('sort') == "ust_sort") {
                            $corporations = Corporation::whereIn('id', $corparation)->where('status', $status)->orderBy('organization_id', $sorting)->paginate(10);
                        }
                    } else {
                        $corporations = Corporation::whereIn('id', $corparation)->where('status', $status)->paginate(20);
                    }


                } else//sadece bölge ve mıntıka birlikte seçildiyse
                {
                    if ($sorting == 'asc' || $sorting == 'desc') {
                        if ($request->get('sort') == "code_sort") {
                            $corporations = Corporation::whereIn('id', $corparation)->orderBy('code', $sorting)->paginate(10);
                        } elseif ($request->get('sort') == "ust_sort") {
                            $corporations = Corporation::whereIn('id', $corparation)->orderBy('organization_id', $sorting)->paginate(10);
                        }
                    } else {
                        $corporations = Corporation::whereIn('id', $corparation)->paginate(20);
                    }

                }
            } else//sadece bölge veya bölge ile beraber durum seçildiyse
            {
                //bölgenin altındaki mıntıkalar alınıyor
                $mintikalar = Organization::where('parent_id', $bolge_id)->pluck('id');

                //seçili bölge ,bölgenin altındaki mıntıkalar ve o mımtıkaların altındaki kurumlar alınıyor
                $organizations = Organization::where('id', $bolge_id)->orWhere('parent_id',
                    $bolge_id)->whereIn('parent_id', $mintikalar, 'or')->pluck('id');

                //alınan yerlere göre kurumlar filtreleniyor.
                $corparation = Corporation::whereIn('organization_id', $organizations)->pluck('id');

                //eğer bölge ve durum seçildiyse mıntıka seçilmediyse
                if ($status) {
                    //alınan kurum bilgilerine göre bölgeler ve durum bilgisine göre ödemeler listeleniyor
                    if ($sorting == 'asc' || $sorting == 'desc') {
                        if ($request->get('sort') == "code_sort") {
                            $corporations = Corporation::whereIn('id', $corparation)->where('status', $status)->orderBy('code', $sorting)->paginate(10);
                        } elseif ($request->get('sort') == "ust_sort") {
                            $corporations = Corporation::whereIn('id', $corparation)->where('status', $status)->orderBy('organization_id', $sorting)->paginate(10);
                        }
                    } else {
                        $corporations = Corporation::whereIn('id', $corparation)->where('status', $status)->paginate(20);
                    }


                } else// sadece bölge seçildiyse
                {
                    if ($sorting == 'asc' || $sorting == 'desc') {
                        if ($request->get('sort') == "code_sort") {
                            $corporations = Corporation::whereIn('id', $corparation)->orderBy('code', $sorting)->paginate(10);
                        } elseif ($request->get('sort') == "ust_sort") {
                            $corporations = Corporation::whereIn('id', $corparation)->orderBy('organization_id', $sorting)->paginate(10);
                        }
                    } else {
                        $corporations = Corporation::whereIn('id', $corparation)->paginate(20);
                    }

                }
            }
        }//sadece durum seçildiyse
        elseif ($status) {
            if ($sorting == 'asc' || $sorting == 'desc') {
                if ($request->get('sort') == "code_sort") {
                    $corporations = Corporation::where('status', $status)->orderBy('code', $sorting)->paginate(10);
                } elseif ($request->get('sort') == "ust_sort") {
                    $corporations = Corporation::where('status', $status)->orderBy('organization_id', $sorting)->paginate(10);
                }
            } else {
                $corporations = Corporation::where('status', $status)->paginate(10);
            }

        } else {
            if ($sorting == 'asc' || $sorting == 'desc') {
                if ($request->get('sort') == "code_sort") {
                    $corporations = Corporation::orderBy('code', $sorting)->paginate(10);
                } elseif ($request->get('sort') == "ust_sort") {
                    $corporations = Corporation::orderBy('organization_id', $sorting)->paginate(10);
                }
            } else {
                $corporations = Corporation::paginate(10);
            }
        }

        // Sıralama İşleminin yapıldığı kısım 'code_sort' cari kodu 'üst_sort' ise üst etiketi sıralamaktadır.
        $codeSort = $request->get('code_sort');
        $ustSort = $request->get('ust_sort');

        if (!empty($codeSort) || !empty($ustSort)) {
            if ($codeSort == 'asc' || $codeSort == 'desc') {
                $corporations = Corporation::orderBy('code', $codeSort)->paginate(10);
            } elseif ($ustSort == 'asc' || $ustSort == 'desc') {
                $corporations = Corporation::orderBy('organization_id', $ustSort)->paginate(10);
            } else {
                $corporations = Corporation::paginate(10);
            }
        }
        $skipped = ($corporations->currentPage() * $corporations->perPage()) - $corporations->perPage();


        return view('admin.corporation.index')
            ->with(compact('corporations', 'skipped', 'bolges', 'bolge_id', 'status', 'get_mintika', 'mintika_id', 'organizationTypeId'))
            ->with('sort_criterion', $sort_criterion)
            ->with('sorting', $sorting)
            ->with('codesort', $codeSort)
            ->with('ustSort', $ustSort);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        if (!Auth::user()->can('corporation operations')) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $bolges = Organization::where([
            ['parent_id', '=', 1],
            ['status', '=', 'active']
        ])->get();

        // gb tablosundan illeri alir
        $cities = GeographicBoundary::where([
            ['parent_id', '=', 1],
            ['status', '=', 'active']
        ])->get();

        $corporation_type_ids = App\CorporationType::all();

        return view('admin.corporation.create',
            [
                'cities' => $cities,
                'bolges' => $bolges,
                'corporation_type_ids' => $corporation_type_ids,

            ]
        );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCorporationPost $request)
    {
        if (!Auth::user()->can('corporation operations')) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }
        if ($request->status == null) {
            $status = 'passive';
        } else {
            $status = 'active';
        }

        $organization = Organization::insert([
            "name" => $request->name,
            "description" => $request->description,
            "organization_type_id" => 4,
            "parent_id" => $request->mintika,
            "status" => $status,
            "is_leaf" => 1
        ]);

        if ($request->status == null) {
            $status = 'passive';
        } else {
            $status = 'active';
        }


        $corporation = Corporation::create([
            "corporation_type_id" => $request->corporation_type_id,
            "name" => $request->name,
            "code" => $request->code,
            "description" => $request->description,
            "organization_id" => Organization::all()->last()->id,
            "geographic_boundary_id" => $request->district,
            "status" => $status
        ]);


        if ($organization && $corporation) {
            return redirect(route('corporation.index'))->with('success', 'işlem başarılı');
        }
        return back()->with('error', 'İşlem Başarısız');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        if (!Auth::user()->can('corporation operations')) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $corporations = Corporation::where('id', $id)->first();

        $corporation_type_ids = App\CorporationType::all();

        $bolges = Organization::where([
            ['parent_id', '=', 1],
            ['status', '=', 'active']
        ])->get();

        $mintikas = Organization::where([
            ['parent_id', '=', $corporations->organization->parent->parent->id],
            ['status', '=', 'active']
        ])->get();

        // gb tablosundan illeri alir
        $cities = GeographicBoundary::where([
            ['parent_id', '=', 1],
            ['status', '=', 'active']
        ])->get();

        $districts = GeographicBoundary::where([
            ['parent_id', '=', $corporations->geographicBoundary->parent->id],
            ['status', '=', 'active']
        ])->get();

        return view('admin.corporation.edit', [
            'cities' => $cities,
            'districts' => $districts,
            'bolges' => $bolges,
            'mintikas' => $mintikas,
            'corporation_type_ids' => $corporation_type_ids,


        ])->with('corporations', $corporations);
    }


    public function update(UpdateCorporationPost $request, $id)
    {
        if (!Auth::user()->can('corporation operations')) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        if ($request->status == null) {
            $status = 'passive';
        } else {
            $status = 'active';
        }

        $corporations_id = Corporation::where('id', $id)->get();
        foreach ($corporations_id as $corporation_id) {
            $data1 = $corporation_id->organization_id;
        }

        $organizations = Organization::where('id', $data1)->update([
            "name" => $request->name,
            "description" => $request->description,
            "organization_type_id" => 4,
            "parent_id" => $request->mintika,
            "status" => $status,
            "is_leaf" => 1
        ]);



        $corporations = Corporation::where('id',$id)->update(
            [
                "corporation_type_id" => $request->corporation_type_id,
                "name" => $request->name,
                "code" => $request->code,
                "description" => $request->description,
                "organization_id" =>  $data1,
                "geographic_boundary_id" => $request->district,
                "status" => $status
            ]);

        if ($corporations && $organizations) {

            $modal = Corporation::where('id', $id)->first();
            $modal_o = Organization::where('id', $data1)->first();

            activity()
                ->performedOn($modal)
                ->causedBy(auth()->user())
                ->log('updated');

            activity()
                ->performedOn($modal_o)
                ->causedBy(auth()->user())
                ->log('updated');

            return back()->with('success', 'İşlem Başarılı');
        }
        return back()->with('error', 'İşlem Başarısız');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( !Auth::user()->can('corporation operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $corporation = Corporation::find($id);

        $corporations_id = Corporation::where('id', $id)->get();
        foreach ($corporations_id as $corporation_id){
            $data1 = $corporation_id->organization_id;
        }
        $organization = Organization::find($data1);

        if ( $organization->delete() && $corporation->delete() ) {
            return true;
        }
        return false;

    }

}
