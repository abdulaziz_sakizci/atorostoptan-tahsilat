<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App;
use App\Organization;
use App\Http\Requests\EditUserRequest;
use App\OrganizationType;
use Spatie\Permission\Models\Role;
use App\Http\Requests\StoreUserPost;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( !Auth::user()->can('user operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $data = User::paginate(10);
        $skipped = ($data->currentPage() * $data->perPage()) - $data->perPage();
        return view('admin.users.index')->with(compact('data', 'skipped'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( !Auth::user()->can('user operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $organizationTypes = OrganizationType::all();

        return view('admin.users.create', compact('organizationTypes'));
    }

    /**
     * @param StoreUserPost $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Throwable
     */
    public function store(StoreUserPost $request)
    {
        if ( !Auth::user()->can('user operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $role = Role::findByName($request->role);

        throw_if($request->organization_type != $role->organization_type_id, App\Exceptions\RoleOrganizationTypeMismatchException::class);

        if (Auth::user()->roles()->first()->name != 'Admin' && $request->role == 'Admin') {

            throw new App\Exceptions\AdminRoleCreationException("Yetkisiz işlem.");
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->corporation_id = 1;
        $user->save();

        if ($user) {

            switch ($request->organization_type) {

                case 1:
                    $user->assignRole($request->organization_type, $request->role);
                    break;
                case 2:
                    $user->assignRole($request->region, $request->role);
                    break;
                case 3:
                    $user->assignRole($request->zone, $request->role);
                    break;
                case 4:
                    $user->assignRole($request->corporation, $request->role);
                    break;
            }

            return redirect(route('user.index'))->with('success', 'işlem başarılı');
        }

        return back()->with('error', 'İşlem Başarısız');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( Auth::user()->id != $id && !Auth::user()->can('user operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $user = User::where('id', $id)->first();

        if ($user->roles()->first()->name == 'Admin' && Auth::user()->roles()->first()->name != 'Admin') {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $organizationTypes = OrganizationType::all();
        $roles = Role::all();

        $organizationId = DB::table('model_has_roles')
            ->where([
                'role_id' => $user->roles()->first()->id,
                'model_id' => $user->id
            ])
            ->first()
            ->organization_id;

        $organizationTypeId = $user->roles()->first()->organization_type_id;
        $role_id = Auth::user()->roles()->first()->id;

        $regions = $zones = $corporations = [];

        switch ($organizationTypeId) {

            case 1:

                if ($role_id != 1)//kullanıcı çamlıca muhasebe ise rol inputuna sadece çamlıca muhasebe gelsin
                {
                    $rolesOrganizationsTypes = Role::where('organization_type_id', $organizationTypeId)->where('id','!=',1)->get();
                }
                else
                {
                    $rolesOrganizationsTypes = Role::where('organization_type_id', $organizationTypeId)->get();
                }

                $merkez = Organization::find(1);

                $organizationRegion = Organization::where('id', $organizationId)->first();

                return view('admin.users.edit', compact(
                    'user',
                    'organizationTypes',
                    'roles',
                    'rolesOrganizationsTypes',
                    'zones',
                    'corporations',
                    'organizationRegion',
                    'regions',
                    'merkez',
                    'organizationId'
                ));
            case 2:
                $rolesOrganizationsTypes = Role::where('organization_type_id', $organizationTypeId)->get();

                $organizationRegion = Organization::where('id', $organizationId)->first();
                $regions = Organization::where('parent_id', $organizationRegion->parent_id)->get();
                $merkez = Organization::find(1);

                return view('admin.users.edit', compact(
                    'user',
                    'organizationTypes',
                    'roles',
                    'rolesOrganizationsTypes',
                    'zones',
                    'corporations',
                    'organizationRegion',
                    'regions',
                    'merkez',
                    'organizationId'
                ));
            case 3:
                $rolesOrganizationsTypes = Role::where('organization_type_id', $organizationTypeId)->get();

                $organizationZone = Organization::where('id', $organizationId)->first();
                $zones = Organization::where('parent_id', $organizationZone->parent_id)->get();
                $organizationRegion = Organization::where('id', $organizationZone->parent_id)->first();
                $regions = Organization::where('parent_id', $organizationRegion->parent_id)->get();
                $merkez = Organization::find(1);

                return view('admin.users.edit', compact(
                    'user',
                    'organizationTypes',
                    'roles',
                    'rolesOrganizationsTypes',
                    'organizationZone',
                    'zones',
                    'corporations',
                    'organizationRegion',
                    'regions',
                    'merkez',
                    'organizationId'
                ));

            case 4:
                $rolesOrganizationsTypes = Role::where('organization_type_id', $organizationTypeId)->get();
                $corporation = Organization::where('id', $organizationId)->first();
                $corporations = Organization::where('parent_id', $corporation->parent_id)->get();
                $organizationZone = $corporation->parent;
                $zones = Organization::where('parent_id', $organizationZone->parent_id)->get();
                $organizationRegion = Organization::where('id', $organizationZone->parent_id)->first();
                $regions = Organization::where('parent_id', $organizationRegion->parent_id)->get();
                $merkez = Organization::find(1);

                return view('admin.users.edit', compact(
                    'user',
                    'organizationTypes',
                    'roles',
                    'rolesOrganizationsTypes',
                    'corporation',
                    'corporations',
                    'organizationZone',
                    'zones',
                    'organizationRegion',
                    'regions',
                    'merkez',
                    'organizationId'
                ));
        }

        return view('admin.users.edit', compact('user', 'organizationTypes', 'roles', 'organizationId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditUserRequest $request, $id)
    {
        if ( Auth::user()->id != $id && !Auth::user()->can('user operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        if (Auth::user()->roles()->first()->name != 'Admin' && $request->role == 'Admin') {

            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }
        if (Auth::user()->roles()->first()->name != 'Admin' &&  $request->role == 1) {

            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $user = User::find($id);

        if ($user->roles()->first()->name == 'Admin' && Auth::user()->roles()->first()->name != 'Admin') {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        if (!empty($request->password)) {

            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();
        } else {

            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();
        }

        if (!$user) {

            return back()->with('error', 'İşlem Başarısız');
        }
        elseif ($user && Auth::user()->can('user operations')) {

            switch ($request->organization_type) {

                case 1:
                    $user->syncRoles($request->organization_type, $request->role);
                    break;
                case 2:
                    $user->syncRoles($request->region, $request->role);
                    break;
                case 3:
                    $user->syncRoles($request->zone, $request->role);
                    break;
                case 4:
                    $user->syncRoles($request->corporation, $request->role);
                    break;
            }


        }

        return back()->with('success', 'İşlem Başarılı');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( !Auth::user()->can('user operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $user = User::find($id);

        if ($user->delete()) {

            return true;
        }

        return false;
    }

}
