<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EditPermissionPost;
use App\Http\Requests\StorePermissionPost;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if ( !Auth::user()->can('permission operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $permissions = Permission::paginate(20);
        $skipped = ($permissions->currentPage() * $permissions->perPage()) - $permissions->perPage();

        return view('admin.permission.index')->with(compact('permissions','skipped'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        if ( !Auth::user()->can('permission operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        return view('admin.permission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePermissionPost $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StorePermissionPost $request)
    {
        if ( !Auth::user()->can('permission operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $permission = Permission::create([
            'name' => $request->permission
        ]);

        if($permission){

            activity()
                ->performedOn($permission)
                ->causedBy(auth()->user())
                ->log('created');

            return redirect(route('permission.index'))->with('success','İşlem başarılı.');
        }

        return back()->with('error','İşlem Başarısız.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        if ( !Auth::user()->can('permission operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $permission = Permission::findById($id);

        return view('admin.permission.edit')->with('permission',$permission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditPermissionPost $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditPermissionPost $request, $id)
    {
        if ( !Auth::user()->can('permission operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $permission = Permission::findById($id);

        $permission->name = $request->permission;
        $permission->save();

        if ($permission) {

            //activity log
            $modal = Permission::where('id', $id)->first();
            activity()
                ->performedOn($modal)
                ->causedBy(auth()->user())
                ->log('updated');

            return back()->with('success', 'İşlem Başarılı');
        }

        return back()->with('error', 'İşlem Başarısız');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if ( !Auth::user()->can('permission operations') ) {
            return abort(403, 'Yetkiniz bulunmamaktadır!');
        }

        $permission = Permission::findById($id);

        if ($permission->delete()) {

            activity()
                ->performedOn($permission)
                ->causedBy(auth()->user())
                ->log('deleted');

            return true;
        }

        return false;
    }
}
