<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('payment.index');
    }

    public function payment($code)
    {
        $payments = Payment::withoutGlobalScopes()->where([
            ['code', $code],
        ])->first();

        if ($payments != null) {
            if ($payments->status != "paid")
                return view('payment.index')->with('payments', $payments);

            return view('payment.completed')->with('payments', $payments)->with('code',$code)->with('message',"$code. İşlem Numaralı Ödeme Zaten Yapılmış.");
        }
        else{
            return view('payment.completed')->with('payments', $payments)->with('code',$code)->with('message', $code .' İşlem Numaralı Ödeme Bulunamadı.');;
        }
    }

    public function success(Request $request)
    {
        $code=$request->get('code');
        $result = Payment::withoutGlobalScopes()->where('code', '=', $code)->first();
        if ($result != null)
            return view('payment.success',compact('result'));

        return view('payment.completed')->with('payments', $code)->with('code',$code)->with('message', $code .' İşlem Numaralı Ödeme Bulunamadı.');;
    }

    public function fail(Request $request)
    {
        $code = $request->input('code');
        return view('payment.fail', compact('code'));
    }
}
