<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRolePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role' =>'required',
            'permissions' =>'required',
            'organization_type' => 'required'
        ];
    }

    /**
     * @return array|string[]
     */
    public function messages()
    {
        return [
            'role.required' => 'Rol alanı zorunludur!',
            'permissions.required' => 'Yetki seçmeden işleme devam edemezsiniz!',
            'organization_type.required' => 'Organizasyon Tipi alanı zorunludur!'
        ];
    }
}
