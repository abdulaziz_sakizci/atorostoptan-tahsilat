<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email:rfc,dns|unique:users,email,' . $this->user_id
        ];

        if (isset($this->password)) {
            $rules['password'] = 'min:6';
        }

        if (Auth::user()->can('user operations')) {

            $rules['organization_type'] = 'required';
            $rules['role'] = 'required';

            switch ($this->organization_type) {

                case 2:
                    $rules['region'] = 'required';
                    break;

                case 3:
                    $rules['region'] = 'required';
                    $rules['zone'] = 'required';
                    break;

                case 4:
                    $rules['region'] = 'required';
                    $rules['zone'] = 'required';
                    $rules['corporation'] = 'required';
                    break;
            }
        }


        return $rules;
    }

    /**
     * @return array|string[]
     */
    public function messages()
    {
        return [
            'name.required' => 'Ad ve Soyad alanını zorunludur!',
            'email.required' => 'Email alanı zorunludur!',
            'email.email' => 'Lütfen mail formatına uygun, geçerli bir mail adresi giriniz!',
            'email.unique' => 'Bu email ile daha önce kayıt işlemi yapılmıştır. Lütfen email adresinizi kontrol ediniz!',
            'password.min' => 'Lütfen minimum 6 karakter şifre belirleyiniz!',
            'organization_type.required' => 'Organizasyon Tipi alanı zorunludur!',
            'role.required' => 'Rol alanı zorunludur!',
            'region.required' => 'Bölge alanı zorunludur!',
            'zone.required' => 'Mıntıka alanı zorunludur!',
            'corporation.required' => 'Kurum alanı zorunludur!'
        ];
    }
}
