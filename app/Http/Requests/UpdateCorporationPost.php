<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCorporationPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'corporation_type_id' =>'required',
            'name' => 'required',
            'code' => 'required',
            'description' => 'required',
            'bolge' => 'required',
            'mintika' => 'required',
            'city' => 'required',
            'district' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'corporation_type_id.required' =>'Kurum Tipi alanı zorunludur!',
            'name.required' => 'Kurum İsmi alanı zorunludur!',
            'description.required' => 'Açıklama alanı zorunludur!',
            'bolge.required' => 'Bölge alanı zorunludur',
            'mintika.required' => 'Mıntıka alanı zorunludur',
            'city.required' => 'İl alanı zorunludur',
            'district.required' => 'İlçe alanı zorunludur',

        ];
    }
}
