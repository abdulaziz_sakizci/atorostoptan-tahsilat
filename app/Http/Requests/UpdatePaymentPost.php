<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePaymentPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (env('APP_ENV') == "development" || env('APP_ENV') == "local" )
        {
            return [
                'student_name' => 'required',
                'guardian_name' => 'required',
                'amount' => 'required',
                'guardian_phone' => 'required',
                'product_type_name' => 'required',
                'description' => 'required',
            ];
        }
        else
        {
            return [
                'student_name' => 'required',
                'guardian_name' => 'required',
                'amount' => 'required|numeric|between:500,3000',
                'guardian_phone' => 'required',
                'product_type_name' => 'required',
                'description' => 'required',
            ];
        }
    }
    public function messages()
    {
        return [
            'student_name.required' => 'Öğrencinin Adı alanı zorunludur',
            'guardian_name.required' => 'Velinin Adı alanı zorunludur',
            'amount.required' => 'Miktar alanı zorunludur',
            'guardian_phone.required' => 'Velinin Telefonu alanı zorunludur',
            'product_type_name.required' => 'Ürünün Tipi Adı alanı zorunludur',
            'description.required' => 'Açıklama alanı zorunludur',
            'amount.between' => 'Min. Tutar 500 TL , Max. Tutar 3000 TL Olmalıdır'

        ];
    }
}
