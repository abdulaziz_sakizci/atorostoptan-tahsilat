<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePermissionPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'permission' =>'required'
        ];
    }

    /**
     * @return array|string[]
     */
    public function messages()
    {
        return [
            'permission.required' => 'Yetki alanı zorunludur!'
        ];
    }
}
