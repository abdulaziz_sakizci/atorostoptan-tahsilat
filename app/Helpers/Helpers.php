<?php

use App\Payment;
use Carbon\Carbon;

function ccNumberMasking($number, $maskingCharacter)
{
    return substr($number, 0, 4) . str_repeat($maskingCharacter, 8) . substr($number, -4);
}

function ccNameMasking($name, $maskingCharacter = '*')
{
    if (strlen($name) > 3)
        return substr($name, 0, 2) . str_repeat($maskingCharacter, strlen($name) - 4) . substr($name, -2);
    return $name;
}

function saveBankRequest($code, $installmentCount, $ccNumber, $ccName, $bankName)
{
    $payment = Payment::withoutGlobalScopes()->where('code', '=', $code)->first();

    $bank_request = [
        "Bank" => $bankName,
        "ccNumber" => ccNumberMasking($ccNumber, "*"),
        "ccName" => ccNameMasking($ccName, "*"),
        "installmentCount" => ($installmentCount == "" || $installmentCount == 0) ? "TEK CEKIM" : $installmentCount,
        "paidAt" => Carbon::now()
    ];

    $payment->bank_request = json_encode($bank_request);
    $payment->save();
}

function getRealIpAddr()
{
    // Client Ip adresini getir.
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

?>
