<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class CorporationType
 * @package App
 *
 * @property int id
 * @property string name
 * @property string status
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @property-read Corporation corporations
 * @property-read Organization organization
 */
class CorporationType extends Model
{

    //Activity Log
    use LogsActivity;
    protected static $logAttributes = [];
    protected static $recordEvents = ['created','updated','deleted'];
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function corporations()
    {
        return $this->hasMany(Corporation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return  $this->belongsTo(Organization::class);
    }
}
