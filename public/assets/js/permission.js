$(document).ready(function() {

    var data_id = "";

    $(".permissionDelete").click(function () {

        data_id = $(this).val();

    });

    $("#continueDeletePermission").click(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'DELETE',
            url: '/admin/permission/' + data_id,

            success: function (data) {

                window.location.reload();
            },
            error: function (xhr) {

                alert('Yetki silme işlemi sırasında bir hata meydana geldi.');
            }
        });
    });
});
