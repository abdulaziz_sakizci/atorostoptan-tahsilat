function makePayment() {
    let hasError = false;

    // sayfayı kapatmayı yenilemeyi engeller
    window.onbeforeunload = function () {
        return "Lütfen İşlemin Tamamlanmasını bekleyin..";
    }

    // modali temizler
    $('#error-modal-body').empty();
    $('input.cc-number').payment('formatCardNumber');

    if (!$.payment.validateCardNumber($('input.cc-number').val())) {
        $('#error-modal-body').append('<div class="alert alert-danger" role="alert">\n' +
            ' <li>Kredi Karti No Gecerli Olmalidir\n</li>' +
            '</div>');
        hasError = true;
    }

    $('input.cc-exp').payment('formatCardExpiry');

    if (!$.payment.validateCardExpiry($('input.cc-exp').payment('cardExpiryVal'))) {
        $('#error-modal-body').append(
            '<div class="alert alert-danger" role="alert">\n' +
            ' <li>Lütfen Kartınızın Son Kullanma Tarihini Kontrol ediniz\n</li>' +
            '</div>');
        hasError = true
    }

    $('input.cc-cvc').payment('formatCardCVC');

    if (!$.payment.validateCardCVC($('input.cc-cvc').val())) {
        $('#error-modal-body').append(
            '<div class="alert alert-danger" role="alert">\n' +
            ' <li>Lütfen Kartınızın CVC Alanını Kontrol ediniz \n</li>' +
            '</div>');
        hasError = true;
    }

    if ($('input#cc-name').val().length < 3) {
        $('#error-modal-body').append(
            '<div class="alert alert-danger" role="alert">\n' +
            ' <li>Lütfen Kartın Üzerinde Yazan İsmi Giriniz \n</li>' +
            '</div>');
        hasError = true;
    }

    if (!$('input#contract').is(':checked')) {
        $('#error-modal-body').append(
            '<div class="alert alert-danger" role="alert">\n' +
            ' <li>Lütfen Hizmet Sözleşmesini Onaylayınız!</li>' +
            '</div>');
        hasError = true;
    }

    if ($('#credit-cart-type').val() == 0) {
        $('#error-modal-body').append(
            '<div class="alert alert-danger" role="alert">\n' +
            ' <li>Lütfen Kart Türü Seçiniz \n</li>' +
            '</div>');
        hasError = true;
    }

    if (hasError) {
        $('#error-modal').modal();
    } else {
        // 3d işlemleri için qs
        let qs = {
            ccName: $('input#cc-name').val(),
            ccCVC: $('input#cc-cvc').val(),
            ccNumber: $('input#cc-number').val().replace(/\s/g, ''),
            ccExpMonth: ("0" + $.payment.cardExpiryVal($('input#cc-exp').val()).month.toString()).substr(-2),
            ccExpYear: $.payment.cardExpiryVal($('input#cc-exp').val()).year.toString().substr(-2),
            code: code,
            amount: amount,
            installmentCount: $('select#installment-count').val()
        };

        console.log($.param(qs));
        let url;

        //alert($('#credit-cart-type').val());

        // bonus -> denizbank ile
        if ($('#credit-cart-type').val() == 2) {
            url = "/bank/denizbank?" + $.param(qs);
        }
        // cardfinans -> finasnabk ile
        else if ($('#credit-cart-type').val() == 3) {
            url = "/bank/finansbank?" + $.param(qs);
        }
        // bankkard -> ziraat
        else if ($('#credit-cart-type').val() == 4) {
            url = "/bank/ziraatbankasi?" + $.param(qs);
        }
        // maximum -> isbankasi
        else if ($('#credit-cart-type').val() == 5) {
            url = "/bank/isbankasi?" + $.param(qs);
        }
        else if ($('#credit-cart-type').val() == 6) {
            url = "/bank/vakifbank?" + $.param(qs);
        }
        else if ($('#credit-cart-type').val() == 7) {
            url = "/bank/akbank?" + $.param(qs);
        }
        // diğer tümüne tek çekim finasnbank üzerinden yapılacak
        else {
            url = "/bank/finansbank?" + $.param(qs);
        }

        console.log(url);

        $("#three-d-pay-modal").find('iframe').attr("src", url);
        $("#three-d-pay-modal").find('iframe').attr("height", $(window).height() - 60);
        $('#three-d-pay-modal').modal({backdrop: 'static', keyboard: false});

        $("#three-d-iframe").on("load", function () {
            $('#loading').hide();
            $('#three-d-iframe').contents().find('#form').submit();
        });
    }
}

// iframe den modal'ı kapatmak için
window.closeIframeModal = function () {
    $('#three-d-pay-modal').modal('hide');
};

window.redirectToSuccessPage = function (code) {
    window.onbeforeunload = null;
    $(location).attr('href', '/payment/success?code=' + code);
};

window.redirectToFailPage = function (code = "") {
    window.onbeforeunload = null;
    $(location).attr('href', '/payment/fail?code=' + code);
};

$(function ($) {
    $('[data-numeric]').payment('restrictNumeric');
    $('.cc-number').payment('formatCardNumber');
    $('.cc-exp').payment('formatCardExpiry');
    $('.cc-cvc').payment('formatCardCVC');
});

function openContractModal() {
    $('#contract-modal').modal();
}

function openPaymentTypeModal() {
    $('#payment-type-modal').modal();
}

function openAboutModal() {
    $('#about-modal').modal();
}

function openContactModal() {
    $('#contact-modal').modal();
}

function openKvkkModal() {
    $('#kvkk-modal').modal();
}

function openPrivacyModal() {
    $('#privacy-modal').modal();
}

$(document).ready(function () {

    $(".payment-type").hide();

    $('#credit-cart-type').on('change', function () {

        $("#payment-type").val(0);
        var creditCartType = $(this).val();

        //kart değiştiğinde taksit resetleniyor
        document.getElementById(
            "installment-count").selectedIndex = "0";

        if (creditCartType == 0) {
            $(".payment-type").hide();
        } else if (creditCartType == 100) {

            //banka ve diğer kartlar seçeneği seçildiğinde sadece Tek Çekim seçilebiliyor
            document.getElementById(
                "installment-count").selectedIndex = "0";

            $(".payment-type").show();
            $(".payment-type option[value='2']").hide();
            $(".payment-type option[value='3']").hide();
            $(".payment-type option[value='4']").hide();
            $(".payment-type option[value='5']").hide();
            $(".payment-type option[value='6']").hide();
        } else {
            $(".payment-type").show();
            $(".payment-type option[value='2']").show();
            $(".payment-type option[value='3']").show();
            $(".payment-type option[value='4']").show();
            $(".payment-type option[value='5']").show();
            $(".payment-type option[value='6']").show();
        }
    });

});
