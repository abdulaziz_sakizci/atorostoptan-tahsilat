$('#filter_button_report').on('click', function () {

    var bolgeId = document.getElementById("bolge-id").value;
    var mintika_id = document.getElementById("mintika-id").value;
    var status = document.getElementById("payment-status").value;

    var url = "report?bolge_id=" + encodeURIComponent(bolgeId) + "&mintika_id=" + encodeURIComponent(mintika_id)
        + "&payment_status=" + encodeURIComponent(status);
    window.location.href = url;

});

$('#export_to_report').on('click', function () {

    var bolgeId = document.getElementById("bolge-id").value;
    var mintika_id = document.getElementById("mintika-id").value;
    var status = document.getElementById("payment-status").value;
    var excel = "export";

    var url = "report?bolge_id=" + encodeURIComponent(bolgeId) + "&mintika_id=" + encodeURIComponent(mintika_id)
        + "&payment_status=" + encodeURIComponent(status) + "&excel=" + encodeURIComponent(excel);
    window.location.href = url;

});
