
var phoneNumber = "";
var paymentCode = "";
var guardianName = "";

function sendSmsBefore(phone_number, payment_code, guardian_name) {

    phoneNumber = phone_number;
    paymentCode = payment_code;
    guardianName = guardian_name;
}

function sendSMS() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: '/admin/ajax/enqueue-sms',
        data: {'phoneNumber': phoneNumber, 'paymentCode': paymentCode, 'guardianName': guardianName},
        dataType: 'json',

        success: function (data) {

            $('#smsModal').modal('hide')
        },
        error: function (xhr) {
            console.log(xhr);
            alert('Bilinmeyen bir hata meydana geldi.');
        }
    });
}

$(document).ready(function() {

    var paymentId = "";

    $(".paymentDelete").click(function () {

        paymentId = $(this).data('payment-id');
    });

    $("#continueDeletePayment").click(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'DELETE',
            url: '/admin/payment/' + paymentId,

            success: function (data) {
                window.location.reload();
            },
            error: function (xhr) {
                console.log(xhr);
                alert('Ödeme silme işlemi sırasında bir hata meydana geldi.');
            }
        });
    });
});

$(document).ready(function() {

    var paymentCancelId = "";

    $(".paymentCancel").click(function () {

        paymentCancelId = $(this).data('payment-id');
    });

    $("#continueCancelPayment").click(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'PUT',
            url: '/admin/payment/cancel/' + paymentCancelId,

            success: function (data) {
                window.location.reload();
            },
            error: function (xhr) {
                console.log(xhr);
                alert('Ödeme iptali sırasında bir hata meydana geldi.');
            }
        });
    });
});

$(document).ready(function () {

    $('#bolge-id').on('change', function () {

        var bolgeId = $(this).val();
        if (bolgeId == "null") {
            $("#mintika-id").html('<option value = "" selected>Tüm Mıntıkalar</option>');
            document.getElementById("mintika-id").disabled = true;
        }
        $.ajax({
            type: 'GET',
            url: '/admin/ajax/organizations',
            data: {'parent_id': bolgeId},
            dataType: 'json',

            success: function (data) {
                if (bolgeId != "null") {
                    $("#mintika-id").removeAttr('disabled').html('<option value = "" selected>Tüm Mıntıkalar</option>');
                }
                // $("#mintika-id").empty();
                // $("#mintika-id").prop('disabled', false);
                // $("#mintika-id").append('<option value="0">Mıntıka Seçiniz</option>');
                $.each(data, function (key, value) {
                    $("#mintika-id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                })
            },
            error: function (xhr) {
                //todo core ui nin hata modal i acilacak
                alert('HATA: Şehir aktarımı sırasında bir hata meydana geldi.');
            }
        });
    });


    $('#filter_button').on('click', function () {

        var bolgeId = document.getElementById("bolge-id").value;
        var mintika_id = document.getElementById("mintika-id").value;
        var status = document.getElementById("payment-status").value;

        var url = "payment?bolge_id=" + encodeURIComponent(bolgeId) + "&mintika_id=" + encodeURIComponent(mintika_id)
            + "&payment_status=" + encodeURIComponent(status);
        window.location.href = url;

    });


});
