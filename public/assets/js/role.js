$(document).ready(function() {

    var data_id = "";

    $(".roleDelete").click(function () {

        data_id = $(this).val();

    });

    $("#continueDeleteRole").click(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'DELETE',
            url: '/admin/role/' + data_id,

            success: function (data) {

                window.location.reload();
            },
            error: function (xhr) {

                alert('Rol silme işlemi sırasında bir hata meydana geldi.');
            }
        });
    });
});
