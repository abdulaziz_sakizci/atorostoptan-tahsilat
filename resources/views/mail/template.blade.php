<!DOCTYPE html>
<html>
<head>
    <title>{{ getenv('APP_NAME') }}</title>
    <style>
        .img-center {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
</head>
<body>
    <div>
        <img class="img-center" src="{{ URL::asset('/assets/images/camlicakitap.png') }}">
    </div>
    <p>Merhaba;</p>
    <p>Bu bir test mailidir.</p>
</body>
</html>
