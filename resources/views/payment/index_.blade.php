<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/payment.css') }}"/>

    <title>Çamlıca Tahsilat Sistemi</title>
</head>
<body>

<div class="corner-ribbon top-left sticky red shadow"><strong>TEST</strong></div>

<div class="container my-container ">
    <div class="card bg-white px-5">

        <div class="text-center">
            <img src="{{asset('assets/images/camlicakitap-01.png')}}" class="rounded" alt="..."
                 style="margin-top: 30px; max-width: 300px; margin-bottom: 30px">
        </div>
        <div class="card-body">
            <div class="row ">

                <div class="col-lg-5 col-md-12 order-lg-12">

                    <div class="card">
                        <div class="card-header">
                            Ödeme Detayları
                        </div>
                        <div class="card-body">

                            <table class="table">
                                <tbody>
                                <tr>
                                    <th scope="row">Kırtasiye Malzemesi</th>
                                    <td>19₺</td>

                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <br>
                    <div class="card">
                        <div class="card-header">
                            Veli Bilgileri
                        </div>
                        <div class="card-body">

                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <i class="fa fa-user"></i> Ömer Seyfettin
                                </li>
                                <li class="list-group-item">
                                    <i class="fa fa-phone"></i> +90537798772
                                </li>
                                <li class="list-group-item">
                                    <i class="fa fa-at"></i> test@gmail.com
                                </li>

                            </ul>

                        </div>
                    </div>
                    <br>
                    <div class="card">
                        <div class="card-header">
                            Öğrenci Bilgileri
                        </div>
                        <div class="card-body">

                            <ul class="list-group list-group-flush">

                                <li class="list-group-item">
                                    <i class="fa fa-building"></i> Gülbahçe Anaokulu
                                </li>
                                <li class="list-group-item">
                                    <i class="fa fa-user"></i> Ömer Seyfettin
                                </li>

                            </ul>

                        </div>
                    </div>

                </div>
                <div class="d-lg-none m-3"></div>

                <div class="col-lg-7 col-md-12 order-lg-1">

                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6"><span>Kredi / Banka Kartı Bilgileri</span></div>
                            </div>

                        </div>

                        <div class="card-body">
                            <div class="form-v2-content">

                            <div class="form-group">
                                <label for="cc-number" class="control-label">Kart Numarası</label>
                                <input id="cc-number"  name="cc_number" type="tel" class="input-lg form-control cc-number"
                                       autocomplete="cc-number" placeholder="•••• •••• •••• ••••" required></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cc-exp" class="control-label">Son Kullanma Tarihi</label>
                                        <input id="cc-exp" name="cc_exp" type="tel" class="input-lg form-control cc-exp"
                                               autocomplete="cc-exp" placeholder="•• / ••••" required></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cc-cvc" class="control-label">CVC</label>
                                        <input id="cc-cvc" name="cc_cvc" type="tel" class="input-lg form-control cc-cvc"
                                               autocomplete="off" placeholder="••••" required></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="control-label">Kartın Üzerindeki İsim</label>
                                <input id="cc-name" name="cc_name" type="text" autocomplete="cc-name" class="input-lg form-control">
                            </div>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" id="contract" name="contact" class="custom-control-input">
                                <label class="custom-control-label" for="contract">
                                    Hizmet Sözleşmesini Okudum ve Kabul Ediyorum
                                </label>
                                <br>
                                <span onclick="openContractModal()" class="text-primary cursor-pointer"><u>Hizmet Sözleşmesini Okumak için Tıklayınız</u> </span>
                            </div>
                            <br>

                            <div class="form-group">
                                <button onclick="makePayment()" class="btn btn-success btn-block">
                                    <i class="fa fa-lock"></i>
                                    3D Secure ile Ödeme Yap
                                </button>
                            </div>

                            </div>
                            <div class="col-md-12 text-center">
                                <img src="https://img.icons8.com/color/36/000000/visa.png">
                                <img src="https://img.icons8.com/color/36/000000/mastercard.png">
                                <img src="https://img.icons8.com/color/36/000000/amex.png">
                            </div>

                        </div>
                    </div>

                </div>

                <!-- footer -->
                <div class="col-12 order-lg-12 mt-5">
                    <ul class="nav justify-content-center">
                        <li class="nav-item">
                            <a onclick="openAboutModal()" class="nav-link" href="#">Hakkımızda</a>
                        </li>
                        <li class="nav-item">
                            <a onclick="openKvkkModal()" class="nav-link" href="#">KVKK</a>

                        </li>
                        <li class="nav-item">
                            <a onclick="openContactModal()" class="nav-link" href="#">İletişim</a>
                        </li>
                        <li class="nav-item">
                            <a onclick="openPrivacyModal()" class="nav-link" href="#">İptal, İade Koşulları ve Gizlilik</a>
                        </li>
                    </ul>
                </div>

                <div class="col-12 order-lg-12 text-center text-muted">
                    <p>Çamlıca Basım Yayın ve Tic. A.Ş. Her Hakkı Saklıdır ©</p>
                </div>

            </div>
        </div>
    </div>


</div>


<!-- Hizmet Sözleşmesi -->
<div class="modal fade" id="contract-modal" tabindex="-1" role="dialog" aria-labelledby="contract-modal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sözleşme</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <p><strong>KULLANIM ŞARTLARI</strong></p>

                <p><a href="http://camlicatahsilat.com/" >http://camlicatahsilat.com/</a>  web sitesine hoş geldiniz.</p>

                <p>Bu web sitesinde sunulan hizmetlerin kullanım şartlarını, sitemize üye olarak veya hizmetlerimizden yararlanma aşamasında, bu sözleşmeyi kabul ettiğinizi beyan eden kutucuğu işaretleyerek kabul etmiş oluyorsunuz. <a href="http://camlicatahsilat.com/" >http://camlicatahsilat.com/</a>  web sitesinde bulunan hizmetlerde kullanım özellik ve şartlarında önceden bir bildirimde bulunmaksızın, herhangi bir zamanda değişiklik
                    yapma, yürürlükten kaldırma ve güncelleme haklarını saklı tutar.</p>

                <p><strong>TAHSİLAT İŞLEMLERİ</strong></p>


                <p><a href="http://camlicatahsilat.com/" >http://camlicatahsilat.com/</a>  web sitesinde sağlanan hizmetler için kayıt sırasında kayıt formunda istenen bilgileri tam ve doğru olarak sağlamanız gerekmektedir.</p>

                <p>Bilgilerinizdeki doğruluğu sizin sorumluluğunuzdadır. Eksik bilgilerden dolayı size ulaşamadığımız zaman sorumluluk size aittir.</p>

                <p>Eğer bu hizmetlerden yararlanırken başkasının adına hesabına kullanıyorsanız, bu kullanım şartlarını onun adına kabul etmeye yetkili olduğunuz anlamına gelmektedir.</p>

                <p>Kayıt esnasında tarafınızca yapılan bütün işlemlerin sorumluluğu tamamıyla size aittir. Bu sözleşmeyi onaylanarak bunu kabul ve teyit etmiş oluyorsunuz.</p>

                <p>Bu sözleşmeyi onaylayarak hesabınızın haksız kullanımı durumunda  <a href="http://camlicatahsilat.com/" >http://camlicatahsilat.com/</a>  personelini hemen bilgilendirmeyi de peşinen kabul ediyorsunuz.</p>

                <p>http://camlicatahsilat.com/, hesabınızın veya şifrenizin başkası tarafından kullanımı nedeniyle, bilginiz olsun veya olmasın, oluşacak zararınızla ilgili herhangi bir yükümlülük kabul etmez. Ancak, hesabınızın veya şifrenizin başka bir kişi tarafından kullanımından <a href="http://camlicatahsilat.com/" >http://camlicatahsilat.com/</a>  veya başka bir kişi veya kuruluş zarar görürse, bu zarardan siz sorumlu tutulabilirsiniz. Hesap sahibinin izni olmaksızın başka bir hesabı kullanmanız yasaktır.</p>

                <p><strong>GARANTİ VE FERAGAT</strong>


                <p><a href="http://camlicatahsilat.com/" >http://camlicatahsilat.com/</a>  ve sizin aranızda yapılan ayrı bir sözleşme ile açıkça belirlenmediği sürece, bu web sitesinden edinilen bütün hizmetler size olduğu haliyle sağlanmış olup, herhangi bir amaca uygunluk açısından veya başka bir açıdan herhangi bir garantiye tabi değildir.</p>

                <p><a href="http://camlicatahsilat.com/" >http://camlicatahsilat.com/</a>  hiçbir şekilde tarafınız veya başka bir üçüncü kişi tarafından bu web sitesine erişimden, bu web sitesinin veya bu web sitesinden linkle yönlendirilmiş başka bir web sitesine erişimden veya bu web sitesinde yer alan hizmetlerin kullanımı vasıtasıyla uğranılmış dolaysız veya dolaylı bir zarardan, gelir veya veri kaybından veya başka bir zarardan sorumlu değildir.</p>

                <p><strong>BU WEB SİTESİNDE SAĞLANAN ÜÇÜNCÜ KİŞİLERE AİT İÇERİK</strong>


                <p>Bu web sitesi üçüncü kişiler tarafından sağlanmış içerik veya yazılımlar bulunmaktadır. Bu web sitesinde yer alan bütün  üçüncü kişilere ait içerik ve yazılımlar için de yukarıda yer alan  “GARANTİ VE FERAGAT” bölümü hükümleri geçerlidir.</p>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
            </div>
        </div>
    </div>
</div>

<!-- KVKK -->
<div class="modal fade" id="kvkk-modal" tabindex="-1" role="dialog" aria-labelledby="contract-modal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" ><strong>KVKK BİLGİLENDİRME FORMU</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div style="display: none;"></div>
                <p style="text-align: center;"><b>KİŞİSEL VERİLERİN ELDE EDİLMESİ, İŞLENMESİ VE KORUNMASINA YÖNELİK BİLGİLENDİRME FORMU</b></p>
                <p align="justify">Çamlıca Basım Yayın Ve Tic. A.Ş. olarak kişisel verilerinizin güvenliğine büyük önem vermekteyiz. Bu sorumluluğumuzun bilinciyle 6698 Sayılı Kişisel Verilerin Korunması Kanunu (“KVK Kanunu”) kapsamında Veri Sorumlusu sıfatıyla, kişisel verileriniz, yalnızca aşağıda açıklanan amaç ve kapsam dâhilinde, 6698 sayılı Kişisel Verilerin Korunması Kanunu ile Çalışma Ve Sosyal Güvenlik Bakanlığı düzenlemeleri ve ilgili diğer sair mevzuata uygun olarak işlenebilecektir.</p>

                <ol>
                    <li><b>İşlenen Kişisel Veriler;</b></li>
                </ol>
                <p align="justify">Çamlıca Basım Yayın Ve Tic. A.Ş. tarafından işlenen kişisel verileriniz yapılan kültür faaliyetlerine bağlı olarak çağrı merkezi, internet sitesi, online hizmetler ve benzeri vasıtalarla sözlü, yazılı yada elektronik olarak toplanmaktadır. Ayrıca Çamlıca Basım Yayın Ve Tic. A.Ş.’nin düzenlediği eğitim, seminer veya organizasyonlara katıldığınızda da kişisel verileriniz toplanabilmektedir. Kişisel verileriniz, aşağıda yer alan kişisel verileriniz, Çamlıca Basım Yayın Ve Tic. A.Ş. tarafından aşağıda yer alanlar dâhil ve bunlarla sınırlı olmaksızın bu maddede belirtilen amaçlar ile bağlantılı ve ölçülü şekilde işlenebilmektedir:</p>
                <p align="justify">Kişisel verileriniz: ad, soyad, TC kimlik numarası, medeni durum, meslek, cinsiyet, adres, telefon numarası, elektronik posta adresi ve sair iletişim verilerinizi, müşteri temsilcileri tarafından çağrı merkezi standartları gereği tutulan sesli görüşme kayıtları ile elektronik posta, mektup ve/veya sair vasıtalar aracılığı ile tarafımızla iletişime geçtiğinizde elde edilen kişisel verilerinizi; anket, teşekkür, şikâyet mektupları, memnuniyet sonuçları gibi bildirimlerinizi; otoparkı kullanmanız halinde araç plaka bilginizi, şirketimizde mevzuat gereği, ortak alanlarda sürekli kayıt halinde olan kamera kayıtlarından elde edilen görüntülerinizi, Çamlıca Basım Yayın Ve Tic. A.Ş.’ye ait tüm web siteleri ve online hizmetler aracılığıyla gönderdiğiniz, iş başvurusunda bulunmanız halinde bu hususta temin edilen özgeçmiş dâhil sair kişisel verileriniz ile hizmet akdiniz ile ilgili her türlü kişisel verilerinizi kapsamakta olup Çamlıca Basım Yayın Ve Tic. A.Ş. tarafından 2. Maddede belirtilen amaçlar ile bağlantılı ve ölçülü şekilde işlenebilmekte ve 3. Maddede belirtilen kişi, kurum ve kuruluşlara aktarılabilmektedir.</p>

                <ol start="2">
                    <li><b>İşleme Amaçları;</b></li>
                </ol>
                <p align="justify">Kişisel verileriniz, Veri tabanı oluşturarak, listeleme, raporlama, doğrulama, analiz ve değerlendirmeler yapmak, istatistiki bilgiler üretmek ve gerektiğinde bunları işin uzmanları ile paylaşmak, kurumumuz hakkında bilgilendirme amacıyla doğrudan bizimle paylaşmış olduğunuz iletişim kanalları üzerinden sizinle irtibat kurmak, kurumumuza personel alımı için gerekli olan süreçlerde kullanmak ve ihtiyaç duyulduğunda sizinle irtibat kurmak, iletişim alanında şirketimizle ilgili haber, gelişme ve bilgilendirme amaçlı dijital ve basılı ortamlarda kullanmak üzere araştırma ve benzeri amaçlarla işlenebilecektir.</p>

                <ol start="3">
                    <li><b>Kişisel Verilerin Aktarımı;</b></li>
                </ol>
                <p align="justify">KVK Kanunu ve ilgili mevzuat uyarınca uygun güvenlik düzeyini temin etmeye yönelik gerekli her türlü teknik ve idari tedbirlerin alınmasını sağlayarak, kişisel verilerinizi 2. Bölüm’de yer alan amaçlar doğrultusunda; 6698 Sayılı Kişisel Verilerin Korunması Kanunu ve ilgili diğer mevzuat hükümlerinin izin verdiği kişi/kurum ve/veya kuruluşlar; bankalar, sandıklar, vakıflar; doğrudan/dolaylı yurtiçi/yurtdışı hissedarlarımız, bağlı ortaklıklarımız ve/veya iştiraklerimiz; grup şirketlerimiz; denetçiler; danışmanlar; iş ortakları; faaliyetlerimizi yürütmek üzere sözleşmesel olarak hizmet aldığımız ve/veya hizmet verdiğimiz, işbirliği yaptığımız, yurt içi/yurt dışı kuruluşlar ile diğer gerçek ve/veya tüzel üçüncü kişilere aktarabiliriz.</p>

                <ol start="4">
                    <li><b>Kişisel Veri Toplamanın Yöntemi ve Hukuki Sebebi;</b></li>
                </ol>
                <p align="justify">Kişisel verileriniz; 2. Bölüm’de belirtilen amaçlarla; Çamlıca Basım Yayın Ve Tic. A.Ş. bünyesinde yer alan işyerlerine gelişiniz esnasında ve/veya öncesinde ve/veya sonrasında; sözlü, yazılı, görsel ya da elektronik ortamda, telefon, sms, mms vb. telekomünikasyon iletişim vasıtalarıyla, Sosyal Güvenlik Kurumu sistemi üzerinden online olarak, özel sigorta şirketinden yararlanma halinde paylaşılan kayıtlardan, kendinizin iletmesi halinde, gönderdiğiniz e-postalar, çağrı merkezi arama kayıtları, internet sitesi, sözlü, basılı ve benzeri kanallar aracılığıyla toplanmakta ve fiziki ve dijital ortamda saklanmaktadır.</p>
                <p align="justify">Kişisel verilerinizin, KVK Kanunu md. 7/f.1.’e göre işlenmesini gerektiren amaç ortadan kalktığında ve/veya mevzuat uyarınca verilerinizi işlememiz için zorunlu kılındığımız zamanaşımı/saklama süreleri dolduğunda, Kişisel verileriniz silinecek, yok edilecek veya anonim hale getirilecektir.</p>

                <ol start="5">
                    <li><b>Kişisel Verilerin Korunmasına Yönelik Haklarınız;</b></li>
                </ol>
                <p align="justify">Kişisel verilerinizin Çamlıca Basım Yayın Ve Tic. A.Ş. Veri Sorumlusu sıfatı ile işlendiği ölçüde KVK Kanunu’nun 11. maddesi gereği, aşağıda yer alan “KVK Kanunu Başvuru Formu”nu doldurup hizmet almış olduğunuz şirket adresine elden teslim ederek, noter kanalıyla göndererek, şahsınıza ait güvenli elektronik imza ile imzalanmış bir e-posta ile mail atarak yahut yine güvenli elektronik imza ile imzalanmış bir “Word veya PDF” uzantılı dosyayı <span style="color: #0563c1;"><u><a href="mailto:hisarsaglik@hs01.kep.tr">bilgi@camlicabasim.com</a> </u></span>adresine e-posta ile göndererek herhangi bir kişisel verilerinizin işlenip işlenmediğini öğrenme, işlenme faaliyetlerine ilişkin olarak bilgi talep etme, işlenme amaçlarını öğrenme, yurt içinde veya yurt dışında üçüncü kişi/kurum ve/veya kuruluşa aktarılmış olması durumunda bu kişileri/kurum ve/veya kuruluşları öğrenme, eksik veya yanlış işlenmiş olması halinde bunların düzeltilmesini/güncellenmesini isteme, işlenmesini gerektiren sebeplerin ortadan kalkması veya Çamlıca Basım Yayın Ve Tic. A.Ş.’nin söz konusu verileri işleyebilmek için hukuki dayanağı veya meşru menfaatinin bulunmaması halinde kişisel verilerinizin silinmesini veya yok edilmesini isteme, otomatik sistemler vasıtasıyla işlenmesi sonucu ortaya çıkabilecek aleyhte sonuçlara itiraz etme, kanuna aykırı bir şekilde işlenmesi sebebiyle zarara uğramanız halinde bu zararın tazmin edilmesini isteme hakkına sahipsiniz.</p>
                <p align="justify"><b>KVK Kanunu uyarınca Kişisel Verilerin Açık Rıza Olmaksızın İşleyebileceği Haller;</b></p>
                <p align="justify">KVK Kanunu’nun 5. maddesi uyarınca, aşağıdaki hallerde açık rızanız aranmaksızın aşağıda belirtilen Kişisel verileriniz işlenebilir:</p>

                <ul>
                    <li>Kanunlarda açıkça öngörülen hallerde,</li>
                    <li>Fiili imkânsızlık nedeniyle veri sahibi olarak rızanızı açıklayamayacak durumda olmanız veya rızanıza hukuki geçerlilik tanınmayan hallerde kendinizin ya da bir başkasının hayatı veya beden bütünlüğünün korunması için kişisel verinizin işlenmesinin zorunlu olması,</li>
                    <li>Bir sözleşmenin kurulması veya ifasıyla doğrudan doğruya ilgili olması kaydıyla, sözleşmenin taraflarına ait kişisel verilerinizin işlenmesinin gerekli olması,</li>
                    <li>Bir hukuki yükümlülüğün yerine getirilebilmesi için zorunlu olması,</li>
                    <li>Kişisel verilerinizin tarafınızca alenileştirilmiş olması,</li>
                    <li>Bir hakkın tesisi, kullanılması veya korunması için veri işlemenin zorunlu olması,</li>
                    <li>Sahip olduğunuz temel hak ve özgürlüklerinize zarar vermemek kaydıyla, Çamlıca Basım Yayın Ve Tic. A.Ş.’nin meşru menfaatleri için veri işlenmesinin zorunlu olması.</li>
                </ul>
                <ol start="6">
                    <li><b>Veri Güvenliği;</b></li>
                </ol>
                Çamlıca Basım Yayın Ve Tic. A.Ş. kişisel verilerinizi bilgi güvenliği standartları ve prosedürleri gereğince alınması gereken tüm teknik ve idari güvenlik kontrollerine tam uygunlukla ve muhtemel risklere karşı uygun bir düzeyde korumaktadır.


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
            </div>
        </div>
    </div>
</div>


<!-- hakkımızda -->
<div class="modal fade" id="about-modal" tabindex="-1" role="dialog" aria-labelledby="about-modal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><strong>HAKKIMIZDA</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div style="display: none;"></div>

                “<strong>Doğru bilgi doğru kaynaktan alınır.</strong>” düsturuyla 2005 yılında faaliyete başlayan Çamlıca Basım Yayın, titizlikle hazırladığı yayınlarla kısa zamanda Türkiye’nin ve dünyanın dört bir yanındaki her seviyeden okuyucu kitlesine ulaştı.
                <p>Kültür ve medeniyetimize dair birbirinden kıymetli eserler yayınlayarak fikir hayatımızın zenginleşmesine katkıda bulunmayı hedefleyen yayınevimiz, daha iyiye ve daha güzele ulaşmaya çalışmaktadır.</p>

                <p>Bu minvalde siz kıymetli okurlarımızın tenkitleri, teklifleri, tebrikleri bizi yüreklendirecek ve yeni çalışmalarımızda bize güç verecektir.</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
            </div>
        </div>
    </div>
</div>


<!-- iletişim -->
<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="contact-modal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><strong>İLETİŞİM</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <p><strong>Çamlıca Basım Yayın ve Tic. A.Ş.</strong></p>
                <p>Bağlar Mah. Mimar Sinan Cad. No:54</p>
                <p>Güneşli, Bağcılar İSTANBUL</p>
                <p><strong>Müşteri Hizmetleri:</strong> 0850 811 9 811</p>
                <p><strong>Mail: </strong>bilgi@camlicabasim.com</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
            </div>
        </div>
    </div>
</div>

<!-- iptal iade gizlilik -->
<div class="modal fade" id="privacy-modal" tabindex="-1" role="dialog" aria-labelledby="contact-modal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><strong>İPTAL,İADE KOŞULLARI VE GİZLİLİK</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body ">

                <p><strong>İptal İade Şartları</strong></p>
                <p>Online tahsilat sisteminden 3d veya sanal pos ile yapılan tahsilatlarda itiraz süreci 10 iş günüdür. Kart sahibinin tarafımıza iade talebinde bulunmasından 7 iş günü içerisinde firmamız tarafından değerlendirme yapıp müşteriye bilgi verilecektir.</p>
                 
                <p><strong>Kişisel Bilgi Güvenliği</strong></p>
                <p><a href="http://camlicatahsilat.com/" >http://camlicatahsilat.com/</a> müşterilerinden üyelik ve alışveriş sırasında alınan tüm kişisel bilgiler mevcut en yüksek elektronik ve fiziksel güvenlik sistemleriyle korunmakta, sadece yetki sahibi personel ve gerekli durumlarda kullanıcı onayı ile görüntülenebilecek bir ortamda saklanmaktadır. Bu bilgiler yalnızca Türkiye Cumhuriyeti kanunları ve uluslararası kanunlar çerçevesinde kullanılmaktadır..</p>
                <p>Kişisel bilgilerin kullanıcı onayı olmaksızın hiçbir şekilde açıklanmaması,yayınlanmaması,üçüncü şahıslarla paylaşılmaması firmamız tarafından taahhüt ve garanti edilmiştir.</p>
                <p><a href="http://camlicatahsilat.com/" >http://camlicatahsilat.com/</a> kişisel bilgilerin dışındaki genel kullanıcı bilgilerini kullanıcı onayı verildiği takdirde; bilgilendirme, tanıtım, duyuru ve teklif amacıyla kullanma haklarını saklı tutar.</p>
                 
                <p><strong>Ödeme Bilgileri Güvenliği</strong></p>
                <p><a href="http://camlicatahsilat.com/" >http://camlicatahsilat.com/</a> bünyesinde oluşturulmuş olan online güvenlik departmanı;ilk kez alışveriş yapacak olan müşterilerimizin kimlik ve adres bilgilerinin kontrolünü yaparak,gerekli durumlarda müşterilerle telefonla irtibata geçerek kredi kartı sahtekarlığı gibi yasal olmayan kullanımları önlemek için önlem almaktadır.</p>
                <p><a href="http://camlicatahsilat.com/" >http://camlicatahsilat.com/</a> 256bit güvenlik sertifikasına sahiptir. http:// http://camlicatahsilat.com/ dan yapılan alışverişlerde verilen kredi kartı bilgileri siteden bağımsız olarak Globalsign tarafından sağlanan 256bit SSL şifreleme protokolü kullanılarak anlaşmalalı bankaların sistemine aktarılır ve banka tarafından sorgulama işlemi yapılır.Sorgulma sonucu anında müşteriye iletilir ve ödeme işlemi tamamlanır.Girilmiş olan kart bilgileri direk olatak bankaya iletilmekte ; tüm işlemler müşteri ve banka arasında gerçekleşmektedir.Bu bilgiler hiçbir şekilde site tarafından görüntülenemediği ve kaydedilmediği için bilgilerin üçüncü şahısların eline geçmesi mümkün değildir..</p>
                <p>Ödeme sayfasının sağ alt köşesinde bulunan kilit resmi sayfanın SSL ile şifrelenerek güvenlik altına alındığını göstermektedir.</p>
                 
                <p><strong>GÜVENLİK UYARISI</strong></p>
                <p>Kredi kartı bilgilerinin çalınması gibi durumlar büyük çoğunlukla fiziki ortamlarda gerçekleşmektedir.Bu nedenle restoran vb. yerlerde kredi kartlarının kullanılması veya kart numarasının başkalarına verilmesi tavsiye edilmemektedir.</p>
                <p>Kredi kartının çalınması,kaybolması veya izinsiz kullanıldığının fark edilmesi gibi durumlarda zaman kaybedilmeksizin kartın ait olduğu banka ile irtibata geçilmeli ve durum bildirilmelidir.</p>
                <p><a href="http://camlicatahsilat.com/" >http://camlicatahsilat.com/</a> tarafından tespit edilen kredi kartı sahtekarlığı durumlarında ilgili banka ve kart sahibi durum hakkında bilgilendirilmektedir.</p>
                <p>İnternet üzerinden yapılan alışverişlerde alışveriş yapılan sitenin adres ve telefon bilgilerinin doğruluğundan emin olunmalı, gerekirse sitedeki müşteri hizmetleri numarası aranarak doğrulama yapılmalıdır.</p>
                <p>İş bu sözleşmeden doğabilecek tüm uyuşmazlıkların giderilmesinde İl merkez mahkemeleri ve icra daireleri yetkilidir.</p>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
            </div>
        </div>
    </div>
</div>




<!-- hata mseaji -->
<div class="modal fade" id="error-modal" tabindex="-1" role="dialog" aria-labelledby="error-modal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><strong>HATA!!! <br> <small>Lütfen aşağıdaki uyarıları dikkate alarak tekrar deneyiniz.</small></strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body " id="error-modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
            </div>
        </div>
    </div>
</div>



<!-- hata mseaji -->
<div class="modal fade" id="payment-modal" tabindex="-1" role="dialog" aria-labelledby="payment-modal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body " id="payment-modal-body">
                Bankaya yönlendiriliyorsunuz...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
            </div>
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/3.0.0/jquery.payment.min.js"></script>
<script src="{{asset('assets/js/payment.js')}}"></script>

{{--<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>--}}
{{--<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>--}}
{{--<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>--}}


<style>

    /* The ribbons */

    .corner-ribbon{
        width: 200px;
        background: #e43;
        position: absolute;
        top: 25px;
        left: -50px;
        text-align: center;
        line-height: 50px;
        letter-spacing: 1px;
        color: #f0f0f0;
        transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
        z-index: 1;
    }

    /* Custom styles */

    .corner-ribbon.sticky{
        position: fixed;
    }

    .corner-ribbon.shadow{
        box-shadow: 0 0 3px rgba(0,0,0,.3);
    }

    /* Different positions */

    .corner-ribbon.top-left{
        top: 25px;
        left: -50px;
        transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
    }


    /* Colors */

    .corner-ribbon.red{background: #ec1600;}


</style>

</body>
</html>
