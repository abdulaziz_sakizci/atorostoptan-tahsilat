<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1.0,user-scalable=0, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/payment.css') }}"/>
    <link href="/@coreui/coreui/dist/css/coreui.min.css" rel="stylesheet">

    <title>Akdeniz Toros Toptan Tahsilat Sistemi</title>
    <link rel="icon" href="{{asset('assets/images/Akdeniz_Toros_Transparan_Normal.png')}}">
</head>

<body class="c-app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card-group">
                <div class="card p-4">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="card-body">

                            <div class="text-center">
                                <img src="{{asset('assets/images/Akdeniz_Toros_Transparan_Normal.png')}}" class="rounded" alt="..."
                                     style="margin-top: 0px; max-width: 200px; margin-bottom: 10px">
                                <h5>{{__('admin/login.camlica') }}</h5>
                                <p class="text-muted">{{__('admin/login.sign in to your account') }}</p>
                            </div>
                            <br>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend"><span class="input-group-text">
                      <svg class="c-icon">
                        <use xlink:href="/@coreui/sprites/free.svg#cil-user"></use>
                      </svg></span></div>
                                <input id="email" type="email" placeholder="{{__('admin/login.email') }}"
                                       class="form-control @error('email') is-invalid @enderror" name="email"
                                       value="{{ old('email') }}" required
                                       oninvalid="this.setCustomValidity('Lütfen geçerli ve formatına uygun bir email adresi giriniz')"
                                       oninput="this.setCustomValidity('')" autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-group mb-4">
                                <div class="input-group-prepend"><span class="input-group-text">
                      <svg class="c-icon">
                        <use xlink:href="/@coreui/sprites/free.svg#cil-lock-locked"></use>
                      </svg></span></div>
                                <input id="password" type="password" placeholder="{{__('admin/login.password') }}"
                                       class="form-control  @error('password') is-invalid @enderror" name="password"
                                       required oninvalid="this.setCustomValidity('Lütfen Şifrenizi Giriniz')"
                                       oninput="this.setCustomValidity('')" autocomplete="current-password">


                            </div>


                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <div class="row">
                                <div class="col-6">
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link px-0" href="{{ route('password.request') }}">
                                            {{__('admin/login.forgot password')}}

                                        </a>
                                    @endif
                                </div>

                                <div class="col-6 text-right">
                                    <button type="submit" class="btn btn-primary px-4">
                                        {{__('admin/login.login') }}
                                    </button>

                                </div>


                                <div class="col-md-12 text-center">
                                    <img src="{{asset('assets/images/guvenlik.png')}}" class="rounded" alt="3d secure"
                                         style="margin-top: 30px; max-width: 300px; margin-bottom: 10px; max-width: 250px">
                                    <br>
                                    <img src="https://img.icons8.com/color/36/000000/visa.png">
                                    <img src="https://img.icons8.com/color/36/000000/mastercard.png">
                                    <img src="https://img.icons8.com/color/36/000000/amex.png">
                                    <img src="{{asset('assets/images/troy.png')}}" class="rounded" alt="3d secure" style="max-width: 40px">
                                </div>
                            </div>

                        </div>

                        <ul class="nav justify-content-center">
                            <li class="nav-item">
                                <a onclick="openAboutModal()" class="nav-link" href="#">Hakkımızda</a>
                            </li>
                            <li class="nav-item">
                                <a onclick="openKvkkModal()" class="nav-link" href="#">KVKK</a>

                            </li>
                            <li class="nav-item">
                                <a onclick="openContactModal()" class="nav-link" href="#">İletişim</a>
                            </li>
                            <li class="nav-item">
                                <a onclick="openPrivacyModal()" class="nav-link" href="#">İptal, İade Koşulları ve
                                    Gizlilik</a>
                            </li>
                        </ul>
                        <div class="col-12 order-lg-12 text-center text-muted">
                            <p>Çamlıca Basım Yayın ve Tic. A.Ş. Her Hakkı Saklıdır ©</p>
                        </div>

                    </form>
                </div>


            </div>
        </div>
    </div>
    <!-- KVKK -->
    <div class="modal fade" id="kvkk-modal" tabindex="-1" role="dialog" aria-labelledby="contract-modal"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>KVKK BİLGİLENDİRME FORMU</strong></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div style="display: none;"></div>
                    <p style="text-align: center;"><b>KİŞİSEL VERİLERİN ELDE EDİLMESİ, İŞLENMESİ VE KORUNMASINA YÖNELİK
                            BİLGİLENDİRME FORMU</b></p>
                    <p align="justify">Çamlıca Basım Yayın Ve Tic. A.Ş. olarak kişisel verilerinizin güvenliğine büyük
                        önem vermekteyiz. Bu sorumluluğumuzun bilinciyle 6698 Sayılı Kişisel Verilerin Korunması Kanunu
                        (“KVK Kanunu”) kapsamında Veri Sorumlusu sıfatıyla, kişisel verileriniz, yalnızca aşağıda
                        açıklanan amaç ve kapsam dâhilinde, 6698 sayılı Kişisel Verilerin Korunması Kanunu ile Çalışma
                        Ve Sosyal Güvenlik Bakanlığı düzenlemeleri ve ilgili diğer sair mevzuata uygun olarak
                        işlenebilecektir.</p>

                    <ol>
                        <li><b>İşlenen Kişisel Veriler;</b></li>
                    </ol>
                    <p align="justify">Çamlıca Basım Yayın Ve Tic. A.Ş. tarafından işlenen kişisel verileriniz yapılan
                        kültür faaliyetlerine bağlı olarak çağrı merkezi, internet sitesi, online hizmetler ve benzeri
                        vasıtalarla sözlü, yazılı yada elektronik olarak toplanmaktadır. Ayrıca Çamlıca Basım Yayın Ve
                        Tic. A.Ş.’nin düzenlediği eğitim, seminer veya organizasyonlara katıldığınızda da kişisel
                        verileriniz toplanabilmektedir. Kişisel verileriniz, aşağıda yer alan kişisel verileriniz,
                        Çamlıca Basım Yayın Ve Tic. A.Ş. tarafından aşağıda yer alanlar dâhil ve bunlarla sınırlı
                        olmaksızın bu maddede belirtilen amaçlar ile bağlantılı ve ölçülü şekilde işlenebilmektedir:</p>
                    <p align="justify">Kişisel verileriniz: ad, soyad, TC kimlik numarası, medeni durum, meslek,
                        cinsiyet, adres, telefon numarası, elektronik posta adresi ve sair iletişim verilerinizi,
                        müşteri temsilcileri tarafından çağrı merkezi standartları gereği tutulan sesli görüşme
                        kayıtları ile elektronik posta, mektup ve/veya sair vasıtalar aracılığı ile tarafımızla
                        iletişime geçtiğinizde elde edilen kişisel verilerinizi; anket, teşekkür, şikâyet mektupları,
                        memnuniyet sonuçları gibi bildirimlerinizi; otoparkı kullanmanız halinde araç plaka bilginizi,
                        şirketimizde mevzuat gereği, ortak alanlarda sürekli kayıt halinde olan kamera kayıtlarından
                        elde edilen görüntülerinizi, Çamlıca Basım Yayın Ve Tic. A.Ş.’ye ait tüm web siteleri ve online
                        hizmetler aracılığıyla gönderdiğiniz, iş başvurusunda bulunmanız halinde bu hususta temin edilen
                        özgeçmiş dâhil sair kişisel verileriniz ile hizmet akdiniz ile ilgili her türlü kişisel
                        verilerinizi kapsamakta olup Çamlıca Basım Yayın Ve Tic. A.Ş. tarafından 2. Maddede belirtilen
                        amaçlar ile bağlantılı ve ölçülü şekilde işlenebilmekte ve 3. Maddede belirtilen kişi, kurum ve
                        kuruluşlara aktarılabilmektedir.</p>

                    <ol start="2">
                        <li><b>İşleme Amaçları;</b></li>
                    </ol>
                    <p align="justify">Kişisel verileriniz, Veri tabanı oluşturarak, listeleme, raporlama, doğrulama,
                        analiz ve değerlendirmeler yapmak, istatistiki bilgiler üretmek ve gerektiğinde bunları işin
                        uzmanları ile paylaşmak, kurumumuz hakkında bilgilendirme amacıyla doğrudan bizimle paylaşmış
                        olduğunuz iletişim kanalları üzerinden sizinle irtibat kurmak, kurumumuza personel alımı için
                        gerekli olan süreçlerde kullanmak ve ihtiyaç duyulduğunda sizinle irtibat kurmak, iletişim
                        alanında şirketimizle ilgili haber, gelişme ve bilgilendirme amaçlı dijital ve basılı ortamlarda
                        kullanmak üzere araştırma ve benzeri amaçlarla işlenebilecektir.</p>

                    <ol start="3">
                        <li><b>Kişisel Verilerin Aktarımı;</b></li>
                    </ol>
                    <p align="justify">KVK Kanunu ve ilgili mevzuat uyarınca uygun güvenlik düzeyini temin etmeye
                        yönelik gerekli her türlü teknik ve idari tedbirlerin alınmasını sağlayarak, kişisel
                        verilerinizi 2. Bölüm’de yer alan amaçlar doğrultusunda; 6698 Sayılı Kişisel Verilerin Korunması
                        Kanunu ve ilgili diğer mevzuat hükümlerinin izin verdiği kişi/kurum ve/veya kuruluşlar;
                        bankalar, sandıklar, vakıflar; doğrudan/dolaylı yurtiçi/yurtdışı hissedarlarımız, bağlı
                        ortaklıklarımız ve/veya iştiraklerimiz; grup şirketlerimiz; denetçiler; danışmanlar; iş
                        ortakları; faaliyetlerimizi yürütmek üzere sözleşmesel olarak hizmet aldığımız ve/veya hizmet
                        verdiğimiz, işbirliği yaptığımız, yurt içi/yurt dışı kuruluşlar ile diğer gerçek ve/veya tüzel
                        üçüncü kişilere aktarabiliriz.</p>

                    <ol start="4">
                        <li><b>Kişisel Veri Toplamanın Yöntemi ve Hukuki Sebebi;</b></li>
                    </ol>
                    <p align="justify">Kişisel verileriniz; 2. Bölüm’de belirtilen amaçlarla; Çamlıca Basım Yayın Ve
                        Tic. A.Ş. bünyesinde yer alan işyerlerine gelişiniz esnasında ve/veya öncesinde ve/veya
                        sonrasında; sözlü, yazılı, görsel ya da elektronik ortamda, telefon, sms, mms vb.
                        telekomünikasyon iletişim vasıtalarıyla, Sosyal Güvenlik Kurumu sistemi üzerinden online olarak,
                        özel sigorta şirketinden yararlanma halinde paylaşılan kayıtlardan, kendinizin iletmesi halinde,
                        gönderdiğiniz e-postalar, çağrı merkezi arama kayıtları, internet sitesi, sözlü, basılı ve
                        benzeri kanallar aracılığıyla toplanmakta ve fiziki ve dijital ortamda saklanmaktadır.</p>
                    <p align="justify">Kişisel verilerinizin, KVK Kanunu md. 7/f.1.’e göre işlenmesini gerektiren amaç
                        ortadan kalktığında ve/veya mevzuat uyarınca verilerinizi işlememiz için zorunlu kılındığımız
                        zamanaşımı/saklama süreleri dolduğunda, Kişisel verileriniz silinecek, yok edilecek veya anonim
                        hale getirilecektir.</p>

                    <ol start="5">
                        <li><b>Kişisel Verilerin Korunmasına Yönelik Haklarınız;</b></li>
                    </ol>
                    <p align="justify">Kişisel verilerinizin Çamlıca Basım Yayın Ve Tic. A.Ş. Veri Sorumlusu sıfatı ile
                        işlendiği ölçüde KVK Kanunu’nun 11. maddesi gereği, aşağıda yer alan “KVK Kanunu Başvuru
                        Formu”nu doldurup hizmet almış olduğunuz şirket adresine elden teslim ederek, noter kanalıyla
                        göndererek, şahsınıza ait güvenli elektronik imza ile imzalanmış bir e-posta ile mail atarak
                        yahut yine güvenli elektronik imza ile imzalanmış bir “Word veya PDF” uzantılı dosyayı <span
                            style="color: #0563c1;"><u><a
                                    href="mailto:hisarsaglik@hs01.kep.tr">bilgi@camlicabasim.com</a> </u></span>adresine
                        e-posta ile göndererek herhangi bir kişisel verilerinizin işlenip işlenmediğini öğrenme, işlenme
                        faaliyetlerine ilişkin olarak bilgi talep etme, işlenme amaçlarını öğrenme, yurt içinde veya
                        yurt dışında üçüncü kişi/kurum ve/veya kuruluşa aktarılmış olması durumunda bu kişileri/kurum
                        ve/veya kuruluşları öğrenme, eksik veya yanlış işlenmiş olması halinde bunların
                        düzeltilmesini/güncellenmesini isteme, işlenmesini gerektiren sebeplerin ortadan kalkması veya
                        Çamlıca Basım Yayın Ve Tic. A.Ş.’nin söz konusu verileri işleyebilmek için hukuki dayanağı veya
                        meşru menfaatinin bulunmaması halinde kişisel verilerinizin silinmesini veya yok edilmesini
                        isteme, otomatik sistemler vasıtasıyla işlenmesi sonucu ortaya çıkabilecek aleyhte sonuçlara
                        itiraz etme, kanuna aykırı bir şekilde işlenmesi sebebiyle zarara uğramanız halinde bu zararın
                        tazmin edilmesini isteme hakkına sahipsiniz.</p>
                    <p align="justify"><b>KVK Kanunu uyarınca Kişisel Verilerin Açık Rıza Olmaksızın İşleyebileceği
                            Haller;</b></p>
                    <p align="justify">KVK Kanunu’nun 5. maddesi uyarınca, aşağıdaki hallerde açık rızanız aranmaksızın
                        aşağıda belirtilen Kişisel verileriniz işlenebilir:</p>

                    <ul>
                        <li>Kanunlarda açıkça öngörülen hallerde,</li>
                        <li>Fiili imkânsızlık nedeniyle veri sahibi olarak rızanızı açıklayamayacak durumda olmanız veya
                            rızanıza hukuki geçerlilik tanınmayan hallerde kendinizin ya da bir başkasının hayatı veya
                            beden bütünlüğünün korunması için kişisel verinizin işlenmesinin zorunlu olması,
                        </li>
                        <li>Bir sözleşmenin kurulması veya ifasıyla doğrudan doğruya ilgili olması kaydıyla, sözleşmenin
                            taraflarına ait kişisel verilerinizin işlenmesinin gerekli olması,
                        </li>
                        <li>Bir hukuki yükümlülüğün yerine getirilebilmesi için zorunlu olması,</li>
                        <li>Kişisel verilerinizin tarafınızca alenileştirilmiş olması,</li>
                        <li>Bir hakkın tesisi, kullanılması veya korunması için veri işlemenin zorunlu olması,</li>
                        <li>Sahip olduğunuz temel hak ve özgürlüklerinize zarar vermemek kaydıyla, Çamlıca Basım Yayın
                            Ve Tic. A.Ş.’nin meşru menfaatleri için veri işlenmesinin zorunlu olması.
                        </li>
                    </ul>
                    <ol start="6">
                        <li><b>Veri Güvenliği;</b></li>
                    </ol>
                    Çamlıca Basım Yayın Ve Tic. A.Ş. kişisel verilerinizi bilgi güvenliği standartları ve prosedürleri
                    gereğince alınması gereken tüm teknik ve idari güvenlik kontrollerine tam uygunlukla ve muhtemel
                    risklere karşı uygun bir düzeyde korumaktadır.


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>


    <!-- hakkımızda -->
    <div class="modal fade" id="about-modal" tabindex="-1" role="dialog" aria-labelledby="about-modal"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>HAKKIMIZDA</strong></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div style="display: none;"></div>

                    “<strong>Doğru bilgi doğru kaynaktan alınır.</strong>” düsturuyla 2005 yılında faaliyete başlayan
                    Çamlıca Basım Yayın, titizlikle hazırladığı yayınlarla kısa zamanda Türkiye’nin ve dünyanın dört bir
                    yanındaki her seviyeden okuyucu kitlesine ulaştı.
                    <p>Kültür ve medeniyetimize dair birbirinden kıymetli eserler yayınlayarak fikir hayatımızın
                        zenginleşmesine katkıda bulunmayı hedefleyen yayınevimiz, daha iyiye ve daha güzele ulaşmaya
                        çalışmaktadır.</p>

                    <p>Bu minvalde siz kıymetli okurlarımızın tenkitleri, teklifleri, tebrikleri bizi yüreklendirecek ve
                        yeni çalışmalarımızda bize güç verecektir.</p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>


    <!-- iletişim -->
    <div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="contact-modal"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>İLETİŞİM</strong></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <p><strong>Çamlıca Basım Yayın ve Tic. A.Ş.</strong></p>
                    <p>Bağlar Mah. Mimar Sinan Cad. No:54</p>
                    <p>Güneşli, Bağcılar İSTANBUL</p>
                    <p><strong>Müşteri Hizmetleri:</strong> 0850 811 9 811</p>
                    <p><strong>Mail: </strong>bilgi@camlicabasim.com</p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>

    <!-- iptal iade gizlilik -->
    <div class="modal fade" id="privacy-modal" tabindex="-1" role="dialog" aria-labelledby="contact-modal"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>İPTAL,İADE KOŞULLARI VE GİZLİLİK</strong>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body ">

                    <p><strong>İptal İade Şartları</strong></p>
                    <p>Online tahsilat sisteminden 3d veya sanal pos ile yapılan tahsilatlarda itiraz süreci 10 iş
                        günüdür. Kart sahibinin tarafımıza iade talebinde bulunmasından 7 iş günü içerisinde firmamız
                        tarafından değerlendirme yapıp müşteriye bilgi verilecektir.</p>
                     
                    <p><strong>Kişisel Bilgi Güvenliği</strong></p>
                    <p><a href="http://camlicatahsilat.com/">http://camlicatahsilat.com/</a> müşterilerinden üyelik ve
                        alışveriş sırasında alınan tüm kişisel bilgiler mevcut en yüksek elektronik ve fiziksel güvenlik
                        sistemleriyle korunmakta, sadece yetki sahibi personel ve gerekli durumlarda kullanıcı onayı ile
                        görüntülenebilecek bir ortamda saklanmaktadır. Bu bilgiler yalnızca Türkiye Cumhuriyeti
                        kanunları ve uluslararası kanunlar çerçevesinde kullanılmaktadır..</p>
                    <p>Kişisel bilgilerin kullanıcı onayı olmaksızın hiçbir şekilde açıklanmaması,yayınlanmaması,üçüncü
                        şahıslarla paylaşılmaması firmamız tarafından taahhüt ve garanti edilmiştir.</p>
                    <p><a href="http://camlicatahsilat.com/">http://camlicatahsilat.com/</a> kişisel bilgilerin
                        dışındaki genel kullanıcı bilgilerini kullanıcı onayı verildiği takdirde; bilgilendirme,
                        tanıtım, duyuru ve teklif amacıyla kullanma haklarını saklı tutar.</p>
                     
                    <p><strong>Ödeme Bilgileri Güvenliği</strong></p>
                    <p><a href="http://camlicatahsilat.com/">http://camlicatahsilat.com/</a> bünyesinde oluşturulmuş
                        olan online güvenlik departmanı;ilk kez alışveriş yapacak olan müşterilerimizin kimlik ve adres
                        bilgilerinin kontrolünü yaparak,gerekli durumlarda müşterilerle telefonla irtibata geçerek kredi
                        kartı sahtekarlığı gibi yasal olmayan kullanımları önlemek için önlem almaktadır.</p>
                    <p><a href="http://camlicatahsilat.com/">http://camlicatahsilat.com/</a> 256bit güvenlik
                        sertifikasına sahiptir. http:// http://camlicatahsilat.com/ dan yapılan alışverişlerde verilen
                        kredi kartı bilgileri siteden bağımsız olarak Globalsign tarafından sağlanan 256bit SSL
                        şifreleme protokolü kullanılarak anlaşmalalı bankaların sistemine aktarılır ve banka tarafından
                        sorgulama işlemi yapılır.Sorgulma sonucu anında müşteriye iletilir ve ödeme işlemi
                        tamamlanır.Girilmiş olan kart bilgileri direk olatak bankaya iletilmekte ; tüm işlemler müşteri
                        ve banka arasında gerçekleşmektedir.Bu bilgiler hiçbir şekilde site tarafından görüntülenemediği
                        ve kaydedilmediği için bilgilerin üçüncü şahısların eline geçmesi mümkün değildir..</p>
                    <p>Ödeme sayfasının sağ alt köşesinde bulunan kilit resmi sayfanın SSL ile şifrelenerek güvenlik
                        altına alındığını göstermektedir.</p>
                     
                    <p><strong>GÜVENLİK UYARISI</strong></p>
                    <p>Kredi kartı bilgilerinin çalınması gibi durumlar büyük çoğunlukla fiziki ortamlarda
                        gerçekleşmektedir.Bu nedenle restoran vb. yerlerde kredi kartlarının kullanılması veya kart
                        numarasının başkalarına verilmesi tavsiye edilmemektedir.</p>
                    <p>Kredi kartının çalınması,kaybolması veya izinsiz kullanıldığının fark edilmesi gibi durumlarda
                        zaman kaybedilmeksizin kartın ait olduğu banka ile irtibata geçilmeli ve durum
                        bildirilmelidir.</p>
                    <p><a href="http://camlicatahsilat.com/">http://camlicatahsilat.com/</a> tarafından tespit edilen
                        kredi kartı sahtekarlığı durumlarında ilgili banka ve kart sahibi durum hakkında
                        bilgilendirilmektedir.</p>
                    <p>İnternet üzerinden yapılan alışverişlerde alışveriş yapılan sitenin adres ve telefon bilgilerinin
                        doğruluğundan emin olunmalı, gerekirse sitedeki müşteri hizmetleri numarası aranarak doğrulama
                        yapılmalıdır.</p>
                    <p>İş bu sözleşmeden doğabilecek tüm uyuşmazlıkların giderilmesinde İl merkez mahkemeleri ve icra
                        daireleri yetkilidir.</p>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>


    <!-- hata mseaji -->
    <div class="modal fade" id="error-modal" tabindex="-1" role="dialog" aria-labelledby="error-modal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>HATA!!! <br> <small>Lütfen aşağıdaki
                                uyarıları dikkate alarak tekrar deneyiniz.</small></strong></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body " id="error-modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>

    <div id="three-d-pay-modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="loading" class="text-center mt-5">
                    <div class="spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
                <iframe id="three-d-iframe" width="100%" height="100px" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</div>
</body>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/3.0.0/jquery.payment.min.js"></script>
<script src="{{asset('assets/js/payment.js?') . microtime() }}"></script>

<!-- CoreUI and necessary plugins-->
<script src="/@coreui/coreui/dist/js/coreui.bundle.min.js"></script>
<!--[if IE]><!-->
<script src="/@coreui/icons/js/svgxuse.min.js"></script>
<!--<![endif]--
</body>
</html>
