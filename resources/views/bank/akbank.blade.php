<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>3D PAY</title>
    <meta http-equiv="Content-Language" content="tr">

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-9">

    <meta http-equiv="Pragma" content="no-cache">

    <meta http-equiv="Expires" content="now">

    <link href="Site.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<?php

$clientId = "102198790";                                 // Mağaza Numarası
//$clientId = "100300000";                               // Test Mağaza Numarası
$amount = $amount;                                       // Tutar
$oid = $code;                                            // Sipariş numarası
$okUrl = url('/bank/akbank/return?code=' . $code);       // Islem basariliysa dönülecek isyeri sayfasi  (3D isleminin ve ödeme isleminin sonucu)
$failUrl = url('/bank/akbank/return?code=' . $code);     // Islem basarizsa dönülecek isyeri sayfasi  (3D isleminin ve ödeme isleminin sonucu)
$rnd = microtime();                                      // Hash karşılaştırması için kullanılacak rastgele dizedir.
$taksit = $installmentCount;                             // Taksit
$islemtipi = "Auth";                                     // Satış
//$storekey = "123456";        //test;                   // Test Üye İş Yeri Anahtarı
$storekey = "CMLD1494";                                  // Üye İş Yeri Anahtarı

# Güvenlik amaçli hashleme yapılıyor.
$hashstr = $clientId . $oid . $amount . $okUrl . $failUrl . $islemtipi . $taksit . $rnd . $storekey;
$hash = base64_encode(pack('H*', sha1($hashstr)));
?>
<center>
    <h1>Bankaya Yönlendiriliyorsunuz..</h1>
    <br>
    {{--TEST--}}
    {{--    <form method="post" action="https://entegrasyon.asseco-see.com.tr/fim/est3Dgate" id="form">--}}
    <form method="post" action="https://www.sanalakpos.com/fim/est3Dgate" id="form">

        <table>
            <tr>
                <td>
                    <input type="hidden" name="pan" value="{{$ccNumber}}" size="20"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="hidden" name="cv2" size="4" value="{{$ccCVC}}"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="hidden" name="Ecom_Payment_Card_ExpDate_Year" value="{{$ccExpYear}}"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="hidden" name="Ecom_Payment_Card_ExpDate_Month" value="{{$ccExpMonth}}"/>
                </td>
            </tr>
            <tr>
                <td align='center' colspan='2'>
                    <input type='hidden' value='Gonder' class='buttonClass'/>
                </td>
            </tr>
        </table>

        <input type="hidden" name="clientid" value="<?php echo $clientId ?>">
        <input type="hidden" name="amount" value="<?php echo $amount ?>">
        <input type="hidden" name="currency" value="949"/>
        <input type="hidden" name="oid" value="<?php echo $oid ?>">
        <input type="hidden" name="okUrl" value="<?php echo $okUrl ?>">
        <input type="hidden" name="failUrl" value="<?php echo $failUrl ?>">
        <input type="hidden" name="islemtipi" value="Auth"/>
        <input type="hidden" name="taksit" value="<?php echo $taksit?>">
        <input type="hidden" name="rnd" value="<?php echo $rnd ?>">
        <input type="hidden" name="hash" value="<?php echo $hash ?>">
        <input type="hidden" name="storetype" value="3d_pay_hosting">
        <input type="hidden" name="refreshtime" value="10">
        <input type="hidden" name="lang" value="tr">
    </form>

</center>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</body>
</html>
