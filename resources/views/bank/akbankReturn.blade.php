Lütfen Bekleyiniz...
<br>

@if($success)
    {{$bankSuccessMessage}}
    <script>
        window.parent.redirectToSuccessPage("{{$code}}");
    </script>
@else
    {{$bankErrorMessage}}
    <script>
        window.parent.redirectToFailPage("{{$code}}");
    </script>
@endif
