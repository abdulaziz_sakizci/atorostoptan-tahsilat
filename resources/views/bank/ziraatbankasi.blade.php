<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>PayFor - 3D Pay</title>
    <meta http-equiv="Content-Language" content="tr">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-9">
    <link href="Site.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<?php
$clientId = "190301494";  //Banka tarafindan verilen isyeri numarasi
$amount = $amount;         //Islem tutari
$oid = $code;      //Siparis Numarasi

$okUrl = url('/bank/ziraatbankasi/return?code=' . $code);       //Islem basariliysa dönülecek isyeri sayfasi  (3D isleminin ve ödeme isleminin sonucu)
$failUrl = url('/bank/ziraatbankasi/return?code=' . $code);     //Islem basarizsa dönülecek isyeri sayfasi  (3D isleminin ve ödeme isleminin sonucu)

$rnd = microtime();    //Tarih veya her seferinde degisen bir deger güvenlik amaçli
$taksit = $installmentCount;         //taksit sayisi
$islemtipi="Auth";     //Islem tipi
$storekey = "CMLD1494";  //isyeri anahtari

// hash hesabinda taksit ve islemtipi de kullanilir.

$hashstr = $clientId . $oid . $amount . $okUrl . $failUrl .$islemtipi. $taksit  .$rnd . $storekey;


$hash = base64_encode(pack('H*',sha1($hashstr)));

?>
<center>
    <h1>Bankaya Yönlendiriliyorsunuz..</h1>
    <br>

    <form method="post" action="https://sanalpos2.ziraatbank.com.tr/fim/est3Dgate" id="form">
        <table class="tableClass">
            <tr>
                <td style="text-align: left">
                    <input type="hidden" name="pan" maxlength="19" class="inputClass" value="{{$ccNumber}}"/>
            </tr>
            <tr>
                <td style="text-align: left">
                    <input type="hidden" name="cv2" maxlength="4" class="inputClass" value="{{$ccCVC}}"/>
            </tr>
            <tr>

                <td><input type="hidden" name="Ecom_Payment_Card_ExpDate_Year" value="{{$ccExpYear}}"/></td>
            </tr>

            <tr>

                <td><input type="hidden" name="Ecom_Payment_Card_ExpDate_Month" value="{{$ccExpMonth}}"/></td>
            </tr>
            <td>
                <input type="hidden" name="cardType" value="{{$cardType}}"/> <!-- visa, mastercard -->
            </td>
            <tr>
                <td align='center' colspan='2'>
                    <input type='hidden' value='Gonder' class='buttonClass'/>
                </td>
            </tr>
        </table>

        <input type="hidden" name="clientid" value="<?php  echo $clientId ?>">

        <input type="hidden" name="amount" value="<?php  echo $amount ?>">
        <input type="hidden" name="oid" value="<?php  echo $oid ?>">
        <input type="hidden" name="okUrl" value="<?php  echo $okUrl ?>">
        <input type="hidden" name="failUrl" value="<?php  echo $failUrl ?>">
        <input type="hidden" name="rnd" value="<?php  echo $rnd ?>" >
        <input type="hidden" name="hash" value="<?php  echo $hash ?>" >
        <input type="hidden" name="islemtipi" value="<?php echo $islemtipi ?>" >
        <input type="hidden" name="taksit" value="<?php echo $taksit ?>" >
        <input type="hidden" name="storetype" value="3d_pay_hosting" >

        <!--
        <input type="hidden" name="lang" value="tr">
        <input type="hidden" name="firmaadi" value="Benim Firmam">

        <input type="hidden" name="Fismi" value="is">
        <input type="hidden" name="faturaFirma" value="faturaFirma">
        <input type="hidden" name="Fadres" value="XXX">
        <input type="hidden" name="Fadres2" value="XXX">
        <input type="hidden" name="Fil" value="XXX">
        <input type="hidden" name="Filce" value="XXX">
        <input type="hidden" name="Fpostakodu" value="postakod93013">

        <input type="hidden" name="tel" value="XXX">
        <input type="hidden" name="fulkekod" value="tr">

        <input type="hidden" name="nakliyeFirma" value="na fi">
        <input type="hidden" name="tismi" value="XXX">
        <input type="hidden" name="tadres" value="XXX">
        <input type="hidden" name="tadres2" value="XXX">
        <input type="hidden" name="til" value="XXX">
        <input type="hidden" name="tilce" value="XXX">

        <input type="hidden" name="tpostakodu" value="ttt postakod93013">
        <input type="hidden" name="tulkekod" value="usa">

        <input type="hidden" name="itemnumber1" value="a1">
        <input type="hidden" name="productcode1" value="a2">
        <input type="hidden" name="qty1" value="3">
        <input type="hidden" name="desc1" value="a4 desc">
        <input type="hidden" name="id1" value="a5">
        <input type="hidden" name="price1" value="6.25">
        <input type="hidden" name="total1" value="7.50">
        -->

    </form>


{{--    <button onclick="window.parent.closeIframeModal();">kapat</button>--}}
{{--    <button onclick="window.parent.redirectToSuccessPage();">redirect</button>--}}

</center>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</body>
</html>
