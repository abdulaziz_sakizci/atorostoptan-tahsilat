<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>PayFor - 3D Pay</title>
    <meta http-equiv="Content-Language" content="tr">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-9">
    <link href="Site.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<?php
/// denizbank

$shopCode = "6424";  //Banka tarafindan verilen isyeri numarasi
$purchaseAmount = $amount;        //Islem tutari
$orderId = $code;      //Siparis Numarasi
$currency = "949"; // Kur Bilgisi - 949 TL

$okUrl = url('/bank/denizbank/return?code=' . $code);                                                                         //Language_OkUrl
$failUrl = url('/bank/denizbank/return?code=' . $code);

$rnd = microtime();    //Tarih veya her seferinde degisen bir deger güvenlik amaçli
$InstallmentCount = $installmentCount;         //taksit sayisi
$txnType ="Auth";     //Islem tipi
$merchantPass = "uZWxu";  //isyeri 3D anahtari
// hash hesabinda taksit ve islemtipi de kullanilir.

$hashstr = $shopCode . $orderId . $purchaseAmount . $okUrl . $failUrl .$txnType. $InstallmentCount  .$rnd . $merchantPass;
$hash = base64_encode(pack('H*',sha1($hashstr)));

?>


<h1>Bankaya Yönlendiriliyorsunuz..</h1>
<br>

<form method="post" action="https://spos.denizbank.com/mpi/Default.aspx" id="form">     <!-- Test ortam linkidir gercek ortam linki dokumanda yer almaktadir  -->
    <table>
        <tr>
            <td>Kredi Kart Numarasi:</td>
            <td><input type="hidden" name="Pan" size="20" value="{{$ccNumber}}"/>
        </tr>

        <tr>
            <td>Güvenlik Kodu:</td>
            <td><input type="hidden" name="Cvv2" size="4" value="{{$ccCVC}}"/></td>
        </tr>

        <tr>
            <td>Son Kullanma Tarihi (MMYY):</td>
            <td><input type="hidden" name="Expiry" value="{{ $ccExpMonth . $ccExpYear  }}"/></td>
        </tr>
        <tr>
            <td>Bonus:</td>
            <td><input type="hidden" name="BonusAmount" value=""/></td>
        </tr>
        <tr>
            <td>Visa/MC secimi</td>


            <td>
                <!-- TODO -->
                <input type="hidden" name="CardType" value="{{$cardType}}"/> <!-- visa -->
            </td>
        </tr>

        <tr>
            <td align="center" colspan="2">
                <input type="hidden" value="Ödemeyi Tamamla"/>
            </td>
        </tr>

    </table>
    <input type="hidden" name="ShopCode" value="<?php  echo $shopCode ?>">
    <input type="hidden" name="PurchAmount" value="<?php  echo $purchaseAmount ?>">
    <input type="hidden" name="Currency" value="<?php  echo $currency ?>">
    <input type="hidden" name="OrderId" value="<?php  echo $orderId ?>">
    <input type="hidden" name="OkUrl" value="<?php  echo $okUrl ?>">
    <input type="hidden" name="FailUrl" value="<?php  echo $failUrl ?>">
    <input type="hidden" name="Rnd" value="<?php  echo $rnd ?>" >
    <input type="hidden" name="Hash" value="<?php  echo $hash ?>" >
    <input type="hidden" name="TxnType" value="<?php  echo $txnType ?>" />
    <input type="hidden" name="InstallmentCount" value="<?php  echo $InstallmentCount ?>" />
    <input type="hidden" name="SecureType" value="3DPay" >
    <input type="hidden" name="Lang" value="tr">
</form>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</body>
</html>
