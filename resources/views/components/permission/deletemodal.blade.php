<!-- Rollerin listelendiği ekranda silme işlemi yapıldığında açılan modal dır -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-danger" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="deleteModalLabel">DİKKAT!</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Bu içeriği silmek istediğinizden emin misiniz? Bu işlem geri alınamaz!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                <button type="button" id="continueDeletePermission" class="btn btn-danger">Devam</button>
            </div>
        </div>
    </div>
</div>
