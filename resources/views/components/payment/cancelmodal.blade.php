<!-- Ödemelerin listelendiği ekranda ödeme iptal etme işlemi yapıldığında açılan modal dır -->
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-danger" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="cancelModalLabel">DİKKAT!</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Ödemeyi iptal etmek istediğinize emin misiniz?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                <button type="button" id="continueCancelPayment" class="btn btn-danger">Devam</button>
            </div>
        </div>
    </div>
</div>
