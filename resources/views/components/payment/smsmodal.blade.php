<!-- Ödemelerin listelendiği ekranda sms gönderme işlemi yapıldığında açılan modal dır -->
<div class="modal fade" id="smsModal" tabindex="-1" role="dialog" aria-labelledby="smsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-warning" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="smsModalLabel">UYARI !!!!</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                SMS göndermek istediğinize emin misiniz?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                <button type="button" onclick="sendSMS()" id="continueSms" class="btn btn-warning">Devam</button>
            </div>
        </div>
    </div>
</div>
