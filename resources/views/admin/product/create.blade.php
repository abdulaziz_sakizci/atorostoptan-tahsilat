@extends('admin.layouts')
@section('content')

@section('breadcrumbs')
    {{ Breadcrumbs::render('product_create') }}
@endsection


<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="card mx-2">
                            <div class="card-header"><h4><strong>  {{__('admin/productType.create.product add')}}</strong></h4>
                            </div>
                            <form class="form-horizontal" action="{{route('product.store')}}" id="myform" method="post"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">


                                    @if(Session::has('success'))
                                        <div class="alert alert-success" role="alert">
                                            {!! html_entity_decode(Session::get('success')) !!}
                                        </div>
                                    @endif

                                    @if(Session::has('error'))
                                        <div class="alert"> {!! html_entity_decode(Session::get('error')) !!}  </div>
                                    @endif



                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif


                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/productType.create.product type')}}</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="name" type="text" name="name"
                                                   value="{{ old('name')}}">
                                        </div>
                                    </div>
                                        <div class="form-group row">
                                            <label
                                                class="col-md-3 col-form-label">{{__('admin/productType.create.status')}}</label>
                                            <div class="col-md-9 col-form-label">
                                                <div class="form-check">
                                                    <input class="form-check-input" id="active" type="radio"
                                                           value="active"
                                                           name="status">
                                                    <label class="form-check-label" for="radio1">Aktif</label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" id="passive" type="radio"
                                                           value="passive" name="status">
                                                    <label class="form-check-label" for="radio2">Pasif</label>
                                                </div>

                                            </div>
                                        </div>


                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary"
                                            type="submit">{{__('admin/productType.create.save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3"></div>

                </div>
            </div>
        </div>
    </main>

</div>-

@endsection
@section('css')@endsection
@section('js')@endsection
