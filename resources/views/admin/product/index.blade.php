@extends('admin.layouts')
@section('content')

@section('breadcrumbs')
    {{ Breadcrumbs::render('product') }}
@endsection

@include('components.modal')



<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">

                <!-- /.row-->
                <div class="card">

                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title mb-0">
                                {{__('admin/productType.index.product operations')}}</h4>
                            <a class="card-header-action"
                               href="{{route('product.create')}}">
                                <button class="btn btn-success">{{__('admin/productType.create.btn-add')}}</button>
                            </a>

                        </div>
                    </div>

                    <div class="card-body">

                        <table class="table table-responsive-sm table-hover table-outline mb-0">
                            <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>{{__('admin/productType.index.product type')}}</th>
                                <th>{{__('admin/productType.index.status')}}</th>
                                <th></th>
                                <th></th>


                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $product)
                                <tr id="item-{{$product->id}}">
                                    <td> {{$loop->iteration}}    </td>
                                    <td> {{$product->name}}  </td>
                                    <td>
                                        @if($product->status == 'active' )
                                            <span class="badge badge-success">{{__('admin/productType.index.active')}}</span>

                                        @else($product->status =='passive')
                                            <span class="badge badge-danger"> {{__('admin/productType.index.passive')}}</span>

                                        @endif

                                    </td>
                                    <td width="5"><a class="btn btn-warning" role="button"
                                                     href="{{route('product.edit',$product->id)}}">{{__('admin/productType.index.btn-edit')}} </a>

                                    <td width="5">
                                        <button class="btn btn-danger abc" value="{{$product->id}}"
                                                type="button"  data-toggle="modal" data-target="#exampleModal">{{__('admin/user.index.btn-delete')}}</button>
                                    </td>
                                    @endforeach
                                </tr>

                            </tbody>
                        </table>
                        <br>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                {{$data->onEachSide(1)->links()}}
                            </ul>
                        </nav>
                    </div>

                </div>

            </div>
        </div>

    </main>

    <script src="{{asset('assets/js/product.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            var data_id = "";

            $(".abc").click(function () {

                data_id = $(this).val();

            });

            $("#continue").click(function () {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'DELETE',
                    url: '{{getenv('APP_URL')}}' + '/admin/product/' + data_id,

                    success: function (data) {

                        window.location.reload();
                    },
                    error: function (xhr) {

                        alert('Yetki silme işlemi sırasında bir hata meydana geldi.');
                    }
                });
            });
        });
    </script>

@endsection
@section('css')@endsection
@section('js')@endsection
