@extends('admin.layouts')
@section('content')

@section('breadcrumbs')
    {{ Breadcrumbs::render('payment_create') }}
@endsection


<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="card mx-2">
                            <div class="card-header"><h4>
                                    <strong>{{__('admin/payment.create.payment add')}}</strong></h4></div>

                            <form class="form-horizontal" action="{{route('payment.store')}}" id="myform"
                                  method="post"
                                  enctype="multipart/form-data">

                                <div class="card-body">

                                    @csrf
                                    @if(Session::has('success'))
                                        <div class="alert alert-success" role="alert">
                                            {!! html_entity_decode(Session::get('success')) !!}
                                        </div>
                                    @endif

                                    @if(Session::has('error'))
                                        <div
                                            class="alert"> {!! html_entity_decode(Session::get('error')) !!}  </div>
                                    @endif



                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/payment.create.student name')}}</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="student_name" type="text"
                                                   name="student_name"
                                                   value="{{ old('student_name')}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/payment.create.guardian name')}}</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="guardian_name" type="text"
                                                   name="guardian_name"
                                                   value="{{ old('guardian_name')}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/payment.create.amount')}}</label>
                                        <div class="col-md-9">

                                            <div class="input-prepend input-group">
                                                <div class="input-group-prepend"><span
                                                        class="input-group-text">₺</span></div>
                                                <input class="form-control" id="amount" type="number"
                                                       name="amount"
                                                       value="{{ old('amount')}}">


                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/payment.create.guardian phone')}}</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="guardian_phone" type="text"
                                                   name="guardian_phone" placeholder="(5__) ___  -  ____"
                                                   value="{{ old('guardian_phone')}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="bolge">{{__('admin/payment.create.product type name')}}</label>
                                        <div class="col-md-9">
                                            <select class="form-control" id="product_type_name"
                                                    name="product_type_name">
                                                @foreach($products as $product)
                                                    <option value="{{$product->name}}">{{$product->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/payment.create.description')}}</label>
                                        <div class="col-md-9">
                                            <textarea class="form-control" id="description" name="description"
                                                      value="{{ old('description')}} rows=" 9"
                                            placeholder="İçerik.."></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"></label>
                                        <div class="col-md-9 col-form-label">
                                            <div class="form-check checkbox">
                                                <input class="form-check-input" id="sms" type="checkbox" name="smsGonder" value="1">
                                                <label class="form-check-label" for="check1">Sms Gönder</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-footer">
                                        <button class="btn btn-primary"
                                                type="submit">{{__('admin/corporation.create.save')}}</button>
                                    </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3"></div>

                </div>
            </div>
        </div>
    </main>

</div>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js" type="text/javascript"></script>

<script>
    $('#guardian_phone').mask('(500) 000 00 00');
</script>
<script type="text/javascript">

    $(document).ready(function () {


        $('#city-id').on('change', function () {

            var cityId = $(this).val();

            $.ajax({
                type: 'GET',
                url: '{{ route('ajax.geographic-boundaries') }}',
                data: {'parent_id': cityId},
                dataType: 'json',

                success: function (data) {

                    $("#district-id").empty();
                    $("#district-id").prop('disabled', false);
                    $("#district-id").append('<option value="0">İlçe Seçiniz</option>');
                    $.each(data, function (key, value) {
                        $("#district-id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                    })
                },
                error: function (xhr) {
                    //todo core ui nin hata modal i acilacak
                    alert('HATA: Şehir aktarımı sırasında bir hata meydana geldi.');
                }
            });
        });

        $('#bolge-id').on('change', function () {

            var bolgeId = $(this).val();

            $.ajax({
                type: 'GET',
                url: '{{ route('ajax.organizations') }}',
                data: {'parent_id': bolgeId},
                dataType: 'json',

                success: function (data) {

                    $("#mintika-id").removeAttr('disabled').html('<option value = "">Lütfen seçiniz.</option>');
                    // $("#mintika-id").empty();
                    // $("#mintika-id").prop('disabled', false);
                    // $("#mintika-id").append('<option value="0">Mıntıka Seçiniz</option>');
                    $.each(data, function (key, value) {
                        $("#mintika-id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                    })
                },
                error: function (xhr) {
                    //todo core ui nin hata modal i acilacak
                    alert('HATA: Şehir aktarımı sırasında bir hata meydana geldi.');
                }
            });
        });


    });


</script>
<script>

    $("#myform").validate({
        rules: {
            code: "required",
            student_name: "required",
            guardian_name: "required",
            amount: "required",
            guardian_phone: {
                required: true,
                minlength: 15,
                maxlength: 20
            },
            product_type_name: "required",
            description: "required",
        },
        messages: {
            code: {required: "Lütfen Kod Giriniz"},

            student_name: {required: "Lütfen Öğrenci İsmi Giriniz"},
            guardian_name: {required: "Lütfen Velinin İsmini Giriniz"},
            amount: {required: "Lütfen Miktar Giriniz"},
            guardian_phone: {
                required: " Lütfen Telefon Bilginizi Giriniz",
                minlength: "Lütfen Telefon Numaranızı Başında '0' olmadan 10 Haneli Olarak Giriniz"
            },
            product_type_name: {required: " Lütfen Ürün İsmini Seçiniz"},
            description: {required: " Lütfen Açıklama Giriniz"},
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

</script>
<style>

    .form-horizontal .form-group label.error {
        content: "\f343";
        padding-left: 0;
        /* margin-left: 22%; */
        display: block;
        margin-top: 1%;
        /* position: absolute; */
        /* bottom: -10px; */
        width: 100%;
        background: none;
        color: #ff0000;
        font-size: smaller;
    }


</style>

@endsection
@section('css')@endsection
@section('js')@endsection
