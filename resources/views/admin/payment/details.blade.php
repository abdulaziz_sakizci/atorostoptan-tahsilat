@extends('admin.layouts')
@section('content')

@section('breadcrumbs')
    {{ Breadcrumbs::render('payment_show', $payment) }}
@endsection

<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <!-- /.row-->
                <div class="card">

                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title mb-0">
                                {{__('admin/payment.index.payment details')}}</h4>
                        </div>
                    </div>

                    <div class="card-body">
                        <table class="table table-responsive-sm  table-bordered table-striped table-outline mb-0">
                            <tbody>
                                <tr>
                                    <th class="w-25">Ödeme Kodu</th>
                                    <td>{{$payment->code}}</td>
                                </tr>
                                <tr>
                                    <th class="w-25">Öğrencinin Adı Soyadı</th>
                                    <td>{{$payment->student_name}}</td>
                                </tr>
                                <tr>
                                    <th class="w-25">Velinin Adı Soyadı</th>
                                    <td>{{$payment->guardian_name}}</td>
                                </tr>
                                <tr>
                                    <th class="w-25">Ödeme Tutarı</th>
                                    <td>{{$payment->amount}}</td>
                                </tr>
                                <tr>
                                    <th class="w-25">Velinin GSM Numarası</th>
                                    <td>{{$payment->guardian_phone}}</td>
                                </tr>
                                <tr>
                                    <th class="w-25">Ürün Tipi</th>
                                    <td>{{$payment->product_type_name}}</td>
                                </tr>
                                <tr>
                                    <th class="w-25">Açıklama</th>
                                    <td>{{$payment->description}}</td>
                                </tr>
                                <tr>
                                    <th class="w-25">Banka Adı</th>
                                    <td>{{$payment->bank_name}}</td>
                                </tr>
                                <tr>
                                    <th class="w-25">Taksit Sayısı</th>
                                    <td>{{$payment->installment}}</td>
                                </tr>
                                <tr>
                                    <th class="w-25">Banka Ödeme Mesajı</th>
                                    <td>{{$payment->bank_success_message}}</td>
                                </tr>
                                <tr>
                                    <th class="w-25">Banka Cevap Kodu</th>
                                    <td>{{$payment->bank_response_code}}</td>
                                </tr>
                                <tr class="thead-light">
                                    <th colspan="2">KULLANICI TARAFINDAN GİDEN ÖDEME DETAYLARI</th>
                                </tr>
                                @foreach($bank_request as $key => $value)
                                    <tr>
                                        <th class="w-25">{{$key}}</th>
                                        <td>
                                            {{($key == "installmentCount" && $value==0)  ? "TEK ÇEKİM" : $value}}</td>
                                    </tr>
                                @endforeach
                                <tr class="thead-light">
                                    <th colspan="2">BANKA TARAFINDAN GELEN ÖDEME DETAYLARI</th>
                                </tr>
                                @foreach($bankResponse as $key => $value)
                                    <tr>
                                        <th class="w-25">{{$key}}</th>
                                        <td>{{is_array($value) ? print_r($value) : $value  }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('css')@endsection
@section('js')@endsection
