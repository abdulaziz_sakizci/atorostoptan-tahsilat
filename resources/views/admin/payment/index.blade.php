@extends('admin.layouts')
@section('content')

@section('breadcrumbs')
    {{ Breadcrumbs::render('payment') }}
@endsection

@include('components.payment.smsmodal')
@include('components.payment.deletemodal')
@include('components.payment.cancelmodal')

<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">

                <!-- /.row-->
                <div class="card">

                    <div class="card-header">
                        <div class="">
                            <div class="master">
                                <div class="title">
                                    <h4 class="card-title mb-0">
                                        {{__('admin/payment.index.payment transactions')}}</h4>
                                </div>

                                <div class="filter">
                                    @if($organizationTypeId != 4)
                                        <div class="mb-2" style="margin-right: 20px;">
                                            <select class="form-control card-header-action" id="bolge-id"
                                                    name="bolge_id">
                                                <option value="null"
                                                        selected>{{__('admin/payment.index.select state')}}</option>
                                                @foreach($bolges as $bolge)
                                                    <option value="{{$bolge->id }}"
                                                        {{  $bolge_id === $bolge->id  ? "selected" : '' }}
                                                    > {{ $bolge->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-2" style="margin-right: 20px;">
                                            @if(!$bolge_id)
                                                <select class="form-control card-header-action" id="mintika-id" disabled
                                                        name="mintika">
                                                    <option
                                                        selected>{{__('admin/payment.index.select region')}}</option>
                                                </select>
                                            @else
                                                <select class="form-control card-header-action" id="mintika-id"
                                                        name="mintika_id">
                                                    <option
                                                        selected>{{__('admin/payment.index.select region')}}</option>
                                                    @foreach($get_mintika as $mintika)
                                                        <option value="{{$mintika->id }}"
                                                            {{  $mintika_id === $mintika->id  ? "selected" : '' }}
                                                        > {{ $mintika->name }}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        </div>
                                        <div class="mb-2" style="margin-right: 20px;">
                                            <select class="form-control card-header-action" id="payment-status"
                                                    name="payment_status">
                                                <option value="0"
                                                        selected>{{__('admin/payment.index.select please')}}</option>
                                                <option
                                                    {{   $status === "paid"  ? "selected" : '' }} value="paid">{{__('admin/payment.index.paid')}}</option>
                                                <option
                                                    {{  $status === "pending"  ? "selected" : '' }} value="pending">{{__('admin/payment.index.pending')}}</option>
                                                <option
                                                    {{  $status === "failed"  ? "selected" : '' }} value="failed">{{__('admin/payment.index.failed')}}</option>
                                                <option
                                                    {{  $status === "cancelled"  ? "selected" : '' }} value="cancelled">{{__('admin/payment.index.cancelled')}}</option>
                                            </select>
                                        </div>
                                        <div class="mb-2" style="margin-right: 20px;">
                                            <button class="btn btn-primary"
                                                    type="submit"
                                                    id="filter_button">{{__('admin/payment.index.search')}}</button>


                                        </div>
                                    @endif
                                    @can('create update payment')
                                        <div class="">
                                        <a
                                           href="{{route('payment.create')}}">
                                            <button
                                                class="btn btn-success">{{__('admin/user.index.btn-add')}}</button>
                                        </a>
                                       </div>
                                    @endcan
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="card-body">

                        <br>
                        <table class="table table-responsive-sm table-hover table-outline mb-0">
                            <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>{{__('admin/payment.index.student name')}}</th>
                                <th>{{__('admin/payment.index.guardian name')}}</th>
                                <th>{{__('admin/payment.index.guardian phone')}}</th>
                                <th>{{__('admin/payment.index.corporation code')}}</th>
                                <th>{{__('admin/payment.index.corporation description')}}</th>
                                <th>{{__('admin/payment.index.region')}}</th>
                                <th>{{__('admin/payment.index.amount')}}</th>
                                <th>{{__('admin/payment.index.installmentCount')}}</th>
                                <th>{{__('admin/payment.index.process date')}}</th>
                                <th>{{__('admin/payment.index.bank name')}}</th>
                                <th>{{__('admin/payment.index.status')}}</th>
                                <th></th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr id="item-{{$payment->id}}">
                                    <td> {{$loop->iteration + $skipped}} </td>
                                    <td> {{$payment->student_name}}</td>
                                    <td> {{$payment->guardian_name}}</td>
                                    <td> {{$payment->guardian_phone}} </td>
                                    <td>
                                        {{$payment->corporation->code}}
                                    </td>
                                    <td>
                                        {{$payment->corporation->description}}
                                    </td>
                                    <td>
                                        {{$payment->corporation->organization->name}}
                                    </td>
                                    <td> {{$payment->amount}} ₺</td>
                                    <td> {{($payment->installment == null ||
                                         $payment->installment == 0) ? "Tek Çekim"  : $payment->installment}} </td>

                                    <td>{{($payment->status =='paid') ? $payment->paid_at : "-"}} </td>
                                    <td>
                                        {{$payment->bank_name}}
                                    </td>
                                    <td>

                                        @if($payment->status == 'pending')
                                            <span
                                                class="badge badge-warning"> {{__('admin/payment.index.pending')}}</span>


                                        @elseif($payment->status =='paid')
                                            <span class="badge badge-success">{{__('admin/payment.index.paid')}}</span>

                                        @elseif($payment->status =='failed')
                                            <span class="badge badge-danger">{{__('admin/payment.index.failed')}}</span>

                                        @elseif($payment->status =='cancelled')
                                            <span
                                                class="badge badge-danger">{{__('admin/payment.index.cancelled')}}</span>
                                        @endif
                                    </td>
                                    <td width="1">
                                        <div class="btn-group">
                                            @if($payment->status === 'paid')
                                                <a class="btn btn-info" role="button"
                                                   href="{{route('payment.show',$payment->id)}}">{{__('admin/payment.index.details')}} </a>
                                            @elseif($payment->status === 'cancelled')
                                            @else
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                    İşlemler
                                                </button>
                                                <div class="dropdown-menu" x-placement="bottom-start"
                                                     style="will-change: transform; margin: 0px;">
                                                    <a class="dropdown-item"
                                                       href="{{route('payments.index',$payment->code)}}"
                                                       target="_blank"><strong>Link</strong></a>
                                                    <a class="dropdown-item"
                                                       onclick="sendSmsBefore('{{$payment->guardian_phone}}', '{{$payment->code}}', '{{$payment->guardian_name}}')"
                                                       type="submit" data-toggle="modal"
                                                       data-target="#smsModal"><strong>
                                                            SMS Gönder</strong></a>
                                                    <a class="dropdown-item"
                                                       href="{{route('payment.edit',$payment->id)}}"><strong> {{__('admin/corporation.index.btn-edit')}} </strong></a>
                                                    @if($payment->status === 'failed')
                                                        <a class="dropdown-item"
                                                           href="{{route('payment.show',$payment->id)}}"><strong> {{__('admin/payment.index.details')}} </strong></a>
                                                    @endif
                                                    <a class="dropdown-item paymentCancel"
                                                       data-payment-id="{{$payment->id}}" type="submit"
                                                       data-toggle="modal"
                                                       data-target="#cancelModal"><strong>{{__('admin/payment.index.cancel')}}</strong></a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item paymentDelete"
                                                       data-payment-id="{{$payment->id}}" type="submit"
                                                       data-toggle="modal"
                                                       data-target="#deleteModal"><strong>{{__('admin/user.index.btn-delete')}}</strong></a>
                                                </div>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                {{$payments->render()}}
                            </ul>
                        </nav>
                    </div>

                </div>

            </div>

        </div>
    </main>
    <script>

    </script>


@endsection
@section('css')
        <link href="{{asset('assets/css/payment.css')}}" rel="stylesheet">
@endsection
@section('js')
        <script src="{{asset('assets/js/admin_payment.js')}}"></script>

@endsection
