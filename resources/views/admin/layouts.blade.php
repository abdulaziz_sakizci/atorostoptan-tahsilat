<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v3.2.0
* @link https://coreui.io
* Copyright (c) 2020 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
<head>

    <base href="./">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Akdeniz Toros Toptan Tahsilat Sistemi">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Akdeniz Toros Toptan Tahsilat Sistemi</title>

    <link rel="icon" href="{{asset('assets/images/Akdeniz_Toros_Transparan_Normal.png')}}">
    <link rel="manifest" href="/@coreui/assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/@coreui/assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Main styles for this application-->

    <link href="/@coreui/coreui/dist/css/coreui.min.css" rel="stylesheet">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    @yield('css')

    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        // Shared ID
        gtag('config', 'UA-118965717-3');
        // Bootstrap ID
        gtag('config', 'UA-118965717-5');
    </script>

</head>
<body class="c-app">
<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-lg-down-none">

        <div class="text-center mb-3 font-weight-bold">
            <img src="{{asset('assets/images/Akdeniz_Toros_Transparan_Normal.png')}}" width="40%" style="margin: 10px 15px 5px 15px">
            <br>
            Tahsilat Sistemi
            <br>
        </div>

    </div>
    <ul class="c-sidebar-nav">

        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{route('default.index')}}">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/@coreui/sprites/free.svg#cil-speedometer"></use>
                </svg>
                {{__('admin/layouts.dashboard')}}</a></li>
        <li class="c-sidebar-nav-title"> {{__('admin/layouts.operation')}}</li>

        @can('list payments')
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{route('payment.index')}}">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="/@coreui/sprites/free.svg#cil-pencil"></use>
                    </svg>{{__('admin/layouts.payment transactions')}}</a></li>
        @endcan

        @can('user operations')
            <li class="c-sidebar-nav-dropdown"><a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="/@coreui/sprites/free.svg#cil-people"></use>
                    </svg> {{__('admin/layouts.user operations')}}</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                                                      href="{{route('user.index')}}"> {{__('admin/layouts.users')}}</a>
                    </li>
                    @can('role operations')
                        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                                                          href="{{route('role.index')}}"> {{__('admin/layouts.roles')}}</a>
                        </li>
                    @endcan
                    @can('permission operations')
                        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                                                          href="{{route('permission.index')}}"> {{__('admin/layouts.permissions')}}</a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('corporation operations')
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{route('corporation.index')}}">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="/@coreui/sprites/free.svg#cil-institution"></use>
                    </svg> {{__('admin/layouts.corporation operations')}}</a></li>

        @endcan
        @can('product operations')
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{route('product.index')}}">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="/@coreui/sprites/free.svg#cil-cart"></use>
                    </svg> {{__('admin/layouts.product operations')}}</a></li>
        @endcan
        @can('report')
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{route('report.index')}}">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/@coreui/sprites/free.svg#cil-institution"></use>
                </svg> {{__('admin/layouts.report operations')}}</a></li>
        @endcan
        <li class="c-sidebar-nav-divider"></li>
        <li class="c-sidebar-nav-title">{{__('admin/layouts.profile')}}</li>

        <ul class="c-sidebar-nav-dropdown-items">
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="notifications/alerts.html"><span
                        class="c-sidebar-nav-icon"></span> Alerts</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="notifications/badge.html"><span
                        class="c-sidebar-nav-icon"></span> Badge</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="notifications/modals.html"><span
                        class="c-sidebar-nav-icon"></span> Modals</a></li>
        </ul>
        </li>


        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{route('user.edit',Auth::user()->id)}}">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/@coreui/sprites/free.svg#cil-user"></use>
                </svg> {{__('admin/layouts.profile')}}</a></li>

    </ul>
    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
            data-class="c-sidebar-minimized"></button>
</div>
<div class="c-wrapper c-fixed-components">
    <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
        <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar"
                data-class="c-sidebar-show">
            <svg class="c-icon c-icon-lg">
                <use xlink:href="/@coreui/sprites/free.svg#cil-menu"></use>
            </svg>
        </button>
{{--        <a class="c-header-brand d-lg-none" href="#">--}}
{{--            <svg width="118" height="46" alt="CoreUI Logo">--}}
{{--                <use xlink:href="/@coreui/assets/brand/coreui.svg#full"></use>--}}
{{--            </svg>--}}
{{--        </a>--}}
        <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar"
                data-class="c-sidebar-lg-show" responsive="true">
            <svg class="c-icon c-icon-lg">
                <use xlink:href="/@coreui/sprites/free.svg#cil-menu"></use>
            </svg>
        </button>

        <ul class="c-header-nav ml-auto mr-4">
            @auth()
                {{ Auth::user()->name }}
            @endauth

            <li class="c-header-nav-item dropdown"><a class="c-header-nav-link" data-toggle="dropdown" href="#"
                                                      role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="c-avatar"><img class="c-avatar-img" src="/@coreui/avatar.png"
                                               alt="user@email.com"></div>
                </a>
                <div class="dropdown-menu dropdown-menu-right pt-0">
                    <div class="dropdown-header bg-light py-2"><strong>{{__('admin/layouts.account')}}</strong></div>
                    <a class="dropdown-item" href="{{route('user.edit',Auth::user()->id)}}">

                        <svg class="c-icon mr-2">
                            <use xlink:href="/@coreui/sprites/free.svg#cil-user"></use>
                        </svg> {{__('admin/layouts.profile')}}</a>

                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <svg class="c-icon mr-2">
                            <use xlink:href="/@coreui/sprites/free.svg#cil-account-logout"></use>
                        </svg>
                        {{__('admin/layouts.logout')}}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    </a>
                </div>
            </li>
        </ul>
        <div class="c-subheader px-3" style="height: 30px; min-height: 55px;" >
            @yield('breadcrumbs')
        </div>
    </header>

@yield('content')
<!--content start  -->


    <!--content end  -->
    <footer class="c-footer">
        <div><a href="https://www.aurorabilisim.com/">Akdeniz Toros Toptan Tahsilat Sistemi</a> &copy; 2021</div>
        <div class="ml-auto">Powered by&nbsp;<a href="https://www.aurorabilisim.com/">AURORA BİLİŞİM</a></div>
    </footer>
</div>


<!-- CoreUI and necessary plugins-->
<script src="/@coreui/coreui/dist/js/coreui.bundle.min.js"></script>
<!--[if IE]><!-->
<script src="/@coreui/icons/js/svgxuse.min.js"></script>
<!--<![endif]-->
@yield('js')
</body>
</html>
