@extends('admin.layouts')
@section('content')

@section('breadcrumbs')
    {{ Breadcrumbs::render('report') }}
@endsection

@include('components.payment.smsmodal')
@include('components.payment.deletemodal')
@include('components.payment.cancelmodal')

<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">

                <!-- /.row-->
                <div class="card">

                    <div class="card-header">
                        <div class="">
                            <div class="master">
                                <div class="title">
                                    <h4 class="card-title mb-0">
                                        {{__('admin/report.index.report transactions')}}</h4>
                                </div>

                                <div class="filter">
                                    @if($organizationTypeId != 4)
                                        <div class="mb-2" style="margin-right: 20px;">
                                            <select class="form-control card-header-action" id="bolge-id"
                                                    name="bolge_id">
                                                <option value="null"
                                                        selected>{{__('admin/payment.index.select state')}}</option>
                                                @foreach($bolges as $bolge)
                                                    <option value="{{$bolge->id }}"
                                                        {{  $bolge_id === $bolge->id  ? "selected" : '' }}
                                                    > {{ $bolge->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-2" style="margin-right: 20px;">
                                            @if(!$bolge_id)
                                                <select class="form-control card-header-action" id="mintika-id" disabled
                                                        name="mintika">
                                                    <option
                                                        selected>{{__('admin/payment.index.select region')}}</option>
                                                </select>
                                            @else
                                                <select class="form-control card-header-action" id="mintika-id"
                                                        name="mintika_id">
                                                    <option
                                                        selected>{{__('admin/payment.index.select region')}}</option>
                                                    @foreach($get_mintika as $mintika)
                                                        <option value="{{$mintika->id }}"
                                                            {{  $mintika_id === $mintika->id  ? "selected" : '' }}
                                                        > {{ $mintika->name }}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        </div>
                                        <div class="mb-2" style="margin-right: 20px;">
                                            <select class="form-control card-header-action" id="payment-status"
                                                    name="payment_status">
                                                <option value="0"
                                                        selected>{{__('admin/payment.index.select please')}}</option>
                                                <option
                                                    {{   $status === "paid"  ? "selected" : '' }} value="paid">{{__('admin/payment.index.paid')}}</option>
                                                <option
                                                    {{  $status === "pending"  ? "selected" : '' }} value="pending">{{__('admin/payment.index.pending')}}</option>
                                                <option
                                                    {{  $status === "failed"  ? "selected" : '' }} value="failed">{{__('admin/payment.index.failed')}}</option>
                                                <option
                                                    {{  $status === "cancelled"  ? "selected" : '' }} value="cancelled">{{__('admin/payment.index.cancelled')}}</option>
                                            </select>
                                        </div>
                                        <div class="mb-2" style="margin-right: 20px;">
                                            <button class="btn btn-primary"
                                                    type="submit"
                                                    id="filter_button_report">{{__('admin/report.index.search')}}</button>
                                            <button class="btn btn-success"
                                                    type="submit"
                                                    id="export_to_report">{{__('admin/report.index.export')}}</button>

                                        </div>
                                    @endif

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="card-body">

                        <br>
                        <table class="table table-responsive-sm table-hover table-outline mb-0">
                            <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>{{__('admin/payment.index.process date')}}</th>
                                <th>{{__('admin/payment.index.corporation code')}}</th>
                                <th>{{__('admin/payment.index.region')}}</th>
                                <th>{{__('admin/payment.index.amount')}}</th>
                                <th>{{__('admin/payment.index.corporation description')}}</th>
                                <th>{{__('admin/payment.index.student name')}}</th>
                                <th>{{__('admin/payment.index.bank name')}}</th>
                                <th>{{__('admin/payment.index.installmentCount')}}</th>
                                <th>{{__('admin/payment.index.status')}}</th>

                            </tr>

                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr id="item-{{$payment->id}}">
                                    <td> {{$loop->iteration + $skipped}} </td>
                                    <td>{{($payment->status =='paid') ? $payment->paid_at : "-"}} </td>
                                    <td>
                                        {{$payment->corporation->code}}
                                    </td>
                                    <td>
                                        {{$payment->corporation->organization->name}}
                                    </td>
                                    <td> {{$payment->amount}} ₺</td>
                                    <td>
                                        {{$payment->corporation->description}}
                                    </td>
                                    <td> {{$payment->student_name}}</td>
                                    <td>
                                        {{$payment->bank_name}}
                                    </td>
                                    <td> {{($payment->installment == null ||
                                         $payment->installment == 0) ? "Tek Çekim"  : $payment->installment}} </td>

                                    <td>

                                        @if($payment->status == 'pending')
                                            <span
                                                class="badge badge-warning"> {{__('admin/payment.index.pending')}}</span>


                                        @elseif($payment->status =='paid')
                                            <span class="badge badge-success">{{__('admin/payment.index.paid')}}</span>

                                        @elseif($payment->status =='failed')
                                            <span class="badge badge-danger">{{__('admin/payment.index.failed')}}</span>

                                        @elseif($payment->status =='cancelled')
                                            <span
                                                class="badge badge-danger">{{__('admin/payment.index.cancelled')}}</span>
                                        @endif
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                {{$payments->onEachSide(1)->links()}}
                            </ul>
                        </nav>
                    </div>

                </div>

            </div>

        </div>
    </main>
    <script>

    </script>


@endsection
@section('css')
        <link href="{{asset('assets/css/payment.css')}}" rel="stylesheet">
@endsection
@section('js')
        <script src="{{asset('assets/js/admin_payment.js')}}"></script>
        <script src="{{asset('assets/js/report.js')}}"></script>


@endsection
