@extends('admin.layouts')
@section('content')

@section('breadcrumbs')
    {{ Breadcrumbs::render('user_create') }}
@endsection


<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="card mx-2">
                            <div class="card-header"><h4><strong>  {{__('admin/user.create.user add')}}</strong></h4>
                            </div>
                            <form class="form-horizontal" action="{{route('user.store')}}" id="myform" method="post"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">

                                    @if(Session::has('success'))
                                        <div class="alert alert-success" role="alert">
                                            {!! html_entity_decode(Session::get('success')) !!}
                                        </div>
                                    @endif

                                    @if(Session::has('error'))
                                        <div class="alert"> {!! html_entity_decode(Session::get('error')) !!}  </div>
                                    @endif

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif


                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/user.create.name')}}</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="name" type="text" name="name"
                                                   value="{{ old('name')}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/user.create.email')}}</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="email" type="email" name="email"
                                                   value="{{ old('email')}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/user.create.password')}}</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="password" type="password" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="bolge">{{__('admin/user.create.organization type')}}</label>
                                        <div class="col-md-9">
                                            <select class="form-control" id="organization_type_id"
                                                    name="organization_type">
                                                <option value="">Lütfen Organizasyon Tipi Seçiniz</option>
                                                @foreach($organizationTypes as $organizationType)
                                                    <option value="{{$organizationType->id}}">{{$organizationType->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row divRole">
                                        <label class="col-md-3 col-form-label"
                                               for="district">{{__('admin/user.create.role')}}</label>
                                        <div class="col-md-9">
                                            <select class="form-control" id="role_id" name="role">
                                            </select>
                                        </div>

                                    </div>
                                    <div class="form-group row divRegion">
                                        <label class="col-md-3 col-form-label"
                                               for="district">{{__('admin/user.create.region')}}</label>
                                        <div class="col-md-9">
                                            <select class="form-control" id="region_id" name="region">
                                            </select>
                                        </div>

                                    </div>
                                    <div class="form-group row divZone">
                                        <label class="col-md-3 col-form-label"
                                               for="district">{{__('admin/user.create.zone')}}</label>
                                        <div class="col-md-9">
                                            <select class="form-control" id="zone_id" name="zone" disabled>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="form-group row divCorporation">
                                        <label class="col-md-3 col-form-label"
                                               for="district">{{__('admin/user.create.corporation')}}</label>
                                        <div class="col-md-9">
                                            <select class="form-control" id="corporation_id" name="corporation" disabled>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary"
                                            type="submit">{{__('admin/user.create.save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3"></div>

                </div>
            </div>
        </div>
    </main>

</div>

<script  type="text/javascript">

    var userRoleName = '{{ \Illuminate\Support\Facades\Auth::user()->roles()->first()->name  }}';
    $(document).ready(function () {

        $(".divRegion").hide();
        $(".divZone").hide();
        $(".divCorporation").hide();

        $('#organization_type_id').on('change', function () {

            var organizationTypeId = $(this).val();

            if (organizationTypeId == 1) {

                $(".divRegion").hide();
                $(".divZone").hide();
                $(".divCorporation").hide();

                $("#region_id").empty();
                $("#zone_id").empty();
                $("#corporation_id").empty();

                $("#region_id").prop('disabled', true);
                $("#zone_id").prop('disabled', true);
                $("#corporation_id").prop('disabled', true);
            }
            else{

                if (organizationTypeId == 2) {

                    $(".divRegion").show();
                    $(".divZone").hide();
                    $(".divCorporation").hide();

                    $("#zone_id").empty();
                    $("#corporation_id").empty();

                    $("#zone_id").prop('disabled', true);
                    $("#corporation_id").prop('disabled', true);
                }

                if (organizationTypeId == 3) {

                    $(".divRegion").show();
                    $(".divZone").show();
                    $(".divCorporation").hide();

                    $("#corporation_id").empty();

                    $("#corporation_id").prop('disabled', true);
                }

                if (organizationTypeId == 4) {

                    $(".divRegion").show();
                    $(".divZone").show();
                    $(".divCorporation").show();
                }
                $.ajax({
                    type: 'GET',
                    url: '{{ route('ajax.organizations') }}',
                    dataType: 'json',

                    success: function (data) {

                        $("#region_id").empty();
                        $("#region_id").prop('disabled', false);
                        $("#region_id").append('<option value="">Üst Etiket Seçiniz</option>');
                        $.each(data, function (key, value) {
                            $("#region_id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                        })
                    },
                    error: function (xhr) {
                        //todo core ui nin hata modal i acilacak
                        alert('Bilinmeyen bir hata meydana geldi.');
                    }
                });
            }

            $.ajax({
                type: 'GET',
                url: '{{ route('ajax.roles') }}',
                data: {'organization_type_id': organizationTypeId},
                dataType: 'json',

                success: function (data) {

                    $("#role_id").empty();
                    $("#role_id").prop('disabled', false);
                    $("#role_id").append('<option value="">Role Seçiniz</option>');
                    $.each(data, function (key, value) {
                        if(value['name'] == 'Admin' && userRoleName == 'Admin'){
                            $("#role_id").append('<option value="' + value['name'] + '">' + value['name'] + '</option>');
                        }
                        if(value['name'] != 'Admin') {
                            $("#role_id").append('<option value="' + value['name'] + '">' + value['name'] + '</option>');
                        }
                    })
                },
                error: function (xhr) {
                    //todo core ui nin hata modal i acilacak
                    alert('Bilinmeyen bir hata meydana geldi.');
                }
            });
        });

        $('#region_id').on('change', function () {

            var regionId = $(this).val();

            $.ajax({
                type: 'GET',
                url: '{{ route('ajax.organizations') }}',
                data: {'parent_id': regionId},
                dataType: 'json',

                success: function (data) {

                    $("#zone_id").empty();
                    $("#zone_id").prop('disabled', false);
                    $("#zone_id").append('<option value="">Alt Etiket Seçiniz</option>');
                    $.each(data, function (key, value) {
                        $("#zone_id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                    })
                },
                error: function (xhr) {
                    //todo core ui nin hata modal i acilacak
                    alert('Bilinmeyen bir hata meydana geldi.');
                }
            });
        });

        $('#zone_id').on('change', function () {

            var zoneId = $(this).val();

            $.ajax({
                type: 'GET',
                url: '{{ route('ajax.organizations') }}',
                data: {'parent_id': zoneId},
                dataType: 'json',

                success: function (data) {

                    $("#corporation_id").empty();
                    $("#corporation_id").prop('disabled', false);
                    $("#corporation_id").append('<option value="">Kurum Seçiniz</option>');
                    $.each(data, function (key, value) {
                        $("#corporation_id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                    })
                },
                error: function (xhr) {
                    //todo core ui nin hata modal i acilacak
                    alert('Bilinmeyen bir hata meydana geldi.');
                }
            });
        });
    });
</script>
@endsection
@section('css')@endsection
@section('js')@endsection
