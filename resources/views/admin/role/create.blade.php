@extends('admin.layouts')
@section('content')

@section('breadcrumbs')
    {{ Breadcrumbs::render('role_create') }}
@endsection

<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="card mx-2">
                            <div class="card-header"><h4><strong>  {{__('admin/user.create.role add')}}</strong></h4>
                            </div>
                            <form class="form-horizontal" action="{{route('role.store')}}" id="myform" method="post"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">

                                    @if(Session::has('success'))
                                        <div class="success"> {!! html_entity_decode(Session::get('success')) !!} </div>
                                    @endif

                                    @if(Session::has('error'))
                                        <div class="alert"> {!! html_entity_decode(Session::get('error')) !!}  </div>
                                    @endif

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/user.create.role')}}</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="role" type="text" name="role"
                                                   value="{{ old('role')}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="bolge">{{__('admin/user.create.organization type')}}</label>
                                        <div class="col-md-9">
                                            <select class="form-control" id="organization_type_id"
                                                    name="organization_type">
                                                <option value="">Lütfen Organizasyon Tipi Seçiniz</option>
                                                @foreach($organizationTypes as $organizationType)
                                                    <option value="{{$organizationType->id}}">{{$organizationType->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/user.create.permissions')}}</label>
                                        <div class="col-md-9">
                                            @foreach($permissions as $permission)
                                            <div class="form-check checkbox">
                                                <input class="form-check-input" id="check_{{$permission->id}}" type="checkbox" name="permissions[]" value="{{$permission->id}}">
                                                <label class="form-check-label" for="check_{{$permission->id}}">{{$permission->name}}</label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary"
                                            type="submit">{{__('admin/user.create.save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </main>

</div>-

@endsection
@section('css')@endsection
@section('js')@endsection
