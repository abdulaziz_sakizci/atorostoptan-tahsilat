@extends('admin.layouts')
@section('content')

@section('breadcrumbs')
    {{ Breadcrumbs::render('role_edit', $role)}}
@endsection

<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">

                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="card mx-2">

                            <div class="card-header"><h4><strong> {{__('admin/user.edit.role edit')}} </strong></h4>
                            </div>
                            <form class="form-horizontal" action="{{route('role.update',$role->id)}}" id="myform"
                                  method="post"
                                  enctype="multipart/form-data">
                                @csrf

                                <div class="card-body">

                                    @method('PUT')
                                    @if(Session::has('success'))
                                        <div
                                            class="alert alert-success"> {!! html_entity_decode(Session::get('success')) !!} </div>
                                    @endif

                                    @if(Session::has('error'))
                                        <div class="alert alert-dangerr"> {!! html_entity_decode(Session::get('error')) !!}  </div>
                                    @endif

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/user.edit.role')}}</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="role" type="text" name="role"
                                                   value="{{$role->name}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="bolge">{{__('admin/user.create.organization type')}}</label>
                                        <div class="col-md-9">
                                            <select class="form-control" id="organization_type_id"
                                                    name="organization_type">
                                                <option value="">Lütfen Organizasyon Tipi Seçiniz</option>
                                                @foreach($organizationTypes as $organizationType)
                                                    <option value="{{$organizationType->id}}" <?php if ($organizationTypesName == $organizationType->name){
                                                        echo "selected";
                                                    }  ?> >{{$organizationType->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/user.create.permissions')}}</label>
                                        <div class="col-md-9">
                                            @foreach($permissions as $value)
                                                <div class="form-check checkbox">
                                                    <input class="form-check-input" id="check_{{$value->id}}" type="checkbox" name="permissions[]" value="{{$value->id}}" @if(in_array($value->id, $rolePermissionIds)) checked @endif>
                                                    <label class="form-check-label" for="check_{{$value->id}}">{{$value->name}}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary"
                                            type="submit">{{__('admin/user.edit.update')}}</button>
                                </div>
                            </form>

                        </div>
                    </div>
                    <div class="col-md-3"></div>

                </div>
            </div>
        </div>
    </main>

</div>

@endsection
@section('css')@endsection
@section('js')@endsection
