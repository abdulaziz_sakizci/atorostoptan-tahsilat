@extends('admin.layouts')
@section('content')

@section('breadcrumbs')
    {{ Breadcrumbs::render('corporation_create') }}
@endsection


<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="card mx-2">
                            <div class="card-header"><h4>
                                    <strong>{{__('admin/corporation.create.corporation add')}}</strong></h4></div>

                            <form class="form-horizontal" action="{{route('corporation.store')}}" id="myform"
                                  method="post"
                                  enctype="multipart/form-data">

                                <div class="card-body">

                                    @csrf
                                    @if(Session::has('success'))
                                        <div class="alert alert-success" role="alert">
                                            {!! html_entity_decode(Session::get('success')) !!}
                                        </div>
                                    @endif

                                    @if(Session::has('error'))
                                        <div
                                            class="alert"> {!! html_entity_decode(Session::get('error')) !!}  </div>
                                    @endif



                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="corporation_type_id">{{__('admin/corporation.create.corporation type')}}</label>
                                        <div class="col-md-9">
                                            <select class="form-control" id="corporation-type-id"
                                                    name="corporation_type_id">
                                                <option value="">Lütfen Seçiniz</option>
                                                @foreach($corporation_type_ids as $corporation_type_id)
                                                    <option value="{{$corporation_type_id->id}}">{{$corporation_type_id->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/corporation.create.corporation name')}}</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="name" type="text" name="name"
                                                   value="{{ old('name')}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/corporation.create.code')}}</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="description" type="text"
                                                   name="code"
                                                   value="{{ old('code')}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/corporation.create.description')}}</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="description" type="text"
                                                   name="description"
                                                   value="{{ old('description')}}">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="bolge">{{__('admin/corporation.create.region')}}</label>
                                        <div class="col-md-9">
                                            <select class="form-control" id="bolge-id"
                                                    name="bolge">
                                                <option>Lütfen Seçiniz</option>
                                                @foreach($bolges as $bolge)
                                                    <option value="{{$bolge->id}}">{{$bolge->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>


                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="mintika">{{__('admin/corporation.create.zone')}}</label>
                                        <div class="col-md-9">
                                            <select class="form-control" id="mintika-id" disabled name="mintika">
                                            </select>
                                        </div>


                                    </div>
                                    <hr>


                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="city">{{__('admin/corporation.create.country')}}</label>
                                        <div class="col-md-9">
                                            <select class="form-control" id="city-id" name="city">
                                                <option value="0">Lütfen Şehir Seçiniz</option>
                                                @foreach($cities as $city)
                                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="district">{{__('admin/corporation.create.district')}}</label>
                                        <div class="col-md-9">
                                            <select class="form-control" id="district-id" disabled name="district">
                                            </select>
                                        </div>

                                    </div>

                                    <hr>
                                    <div class="form-group row">
                                        <label
                                            class="col-md-3 col-form-label">{{__('admin/corporation.create.status')}}</label>
                                        <div class="col-md-9 col-form-label">
                                            <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                <input class="c-switch-input"  name="status" type="checkbox" checked=""><span class="c-switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                            </label>
                                        </div>
                                </div>


                                <div class="card-footer">
                                    <button class="btn btn-primary"
                                            type="submit">{{__('admin/corporation.create.save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3"></div>

                </div>
            </div>
        </div>
    </main>

</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#city-id').on('change', function () {

            var cityId = $(this).val();

            $.ajax({
                type: 'GET',
                url: '{{ route('ajax.geographic-boundaries') }}',
                data: {'parent_id': cityId},
                dataType: 'json',

                success: function (data) {

                    $("#district-id").empty();
                    $("#district-id").prop('disabled', false);
                    $("#district-id").append('<option value="">İlçe Seçiniz</option>');
                    $.each(data, function (key, value) {
                        $("#district-id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                    })
                },
                error: function (xhr) {
                    //todo core ui nin hata modal i acilacak
                    alert('HATA: Şehir aktarımı sırasında bir hata meydana geldi.');
                }
            });
        });

        $('#bolge-id').on('change', function () {

            var bolgeId = $(this).val();

            $.ajax({
                type: 'GET',
                url: '{{ route('ajax.organizations') }}',
                data: {'parent_id': bolgeId},
                dataType: 'json',

                success: function (data) {

                    $("#mintika-id").removeAttr('disabled').html('<option value = "">Lütfen seçiniz.</option>');
                    // $("#mintika-id").empty();
                    // $("#mintika-id").prop('disabled', false);
                    // $("#mintika-id").append('<option value="0">Mıntıka Seçiniz</option>');
                    $.each(data, function (key, value) {
                        $("#mintika-id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                    })
                },
                error: function (xhr) {
                    //todo core ui nin hata modal i acilacak
                    alert('HATA: Şehir aktarımı sırasında bir hata meydana geldi.');
                }
            });
        });


    });
    $("#a").on('change', function() {
        if ($(this).is(':checked')) {
            $(this).attr('value', 'active');
        }
        else {
            $(this).attr('value', 'passive');
        }
    });
</script>


@endsection
@section('css')@endsection
@section('js')@endsection
