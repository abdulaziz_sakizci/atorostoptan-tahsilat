@extends('admin.layouts')
@section('content')

@section('breadcrumbs')
    {{ Breadcrumbs::render('corporation') }}
@endsection

@include('components.modal')


<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">

                <!-- /.row-->
                <div class="card">
                    <div class="card-body">
                        <div id="akordeon">
                            @if($organizationTypeId != 4)
                                <div class="card">
                                    <div class="card-header" id="baslik1">
                                        <div class="d-flex bd-highlight">
                                            <div class="p-2 flex-grow-1 bd-highlight"><h4 class="card-title mb-0">
                                                    {{__('admin/corporation.index.corporation operations')}}</h4></div>
                                            <div class="p-2 bd-highlight">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-block btn-outline-info"
                                                            data-toggle="collapse" data-target="#akordeon1">
                                                        Filtreleme ve Sıralama işlemleri
                                                    </button>
                                                </h5>
                                            </div>
                                            <div class="p-2 bd-highlight"><a class="card-header-action"
                                                                             href="{{route('corporation.create')}}">
                                                    <button
                                                        class="btn btn-success">{{__('admin/user.index.btn-add')}}</button>
                                                </a></div>
                                        </div>
                                    </div>

                                    <div id="akordeon1" class="collapse" data-parent="#akordeon">
                                        <div class="card-body">
                                            <form id="filterForm" method="get" action="{{route('corporation.index')}}">
                                                <div class="row">
                                                    <div class="col-md-2 mb-2">
                                                        <select class="form-control" id="bolge-id"
                                                                name="bolge_id">
                                                            <option value="null"
                                                                    selected>{{__('admin/corporation.create.region')}}</option>
                                                            @foreach($bolges as $bolge)
                                                                <option value="{{$bolge->id }}"
                                                                    {{  $bolge_id === $bolge->id  ? "selected" : '' }}
                                                                > {{ $bolge->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2 mb-2">
                                                        @if(!$bolge_id)
                                                            <select class="form-control"
                                                                    id="mintika-id" disabled
                                                                    name="mintika">
                                                                <option
                                                                    selected>{{__('admin/corporation.index.zone')}}</option>
                                                            </select>
                                                        @else
                                                            <select class="form-control"
                                                                    id="mintika-id"
                                                                    name="mintika_id">
                                                                <option
                                                                    selected>{{__('admin/corporation.index.zone')}}</option>@foreach($get_mintika as $mintika)
                                                                    <option value="{{$mintika->id }}"
                                                                        {{  $mintika_id === $mintika->id  ? "selected" : '' }}
                                                                    > {{ $mintika->name }}</option>
                                                                @endforeach</select>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-2 mb-2">
                                                        <select class="form-control" id="corporation-status"
                                                                name="corporation_status">
                                                            <option value="0"
                                                                    selected>{{__('admin/corporation.index.status')}}</option>
                                                            <option
                                                                {{   $status === "active"  ? "selected" : '' }} value="active">{{__('admin/corporation.index.active')}}</option>
                                                            <option
                                                                {{  $status === "passive"  ? "selected" : '' }} value="passive">{{__('admin/corporation.index.passive')}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2 mb-2">
                                                        <select class="form-control" id="corporation-sort"
                                                                name="sort">
                                                            <option value=""
                                                                    selected>Sırlama Kriterleri
                                                            </option>
                                                            <option
                                                                {{$sort_criterion === "code_sort" ? "selected" : ''}}  value="code_sort">
                                                                Cari Kod'a göre Sırala
                                                            </option>
                                                            <option
                                                                {{$sort_criterion === "ust_sort" ? "selecter" : ''}} value="ust_sort">
                                                                Üst Etiket'e göre Sırala
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2 mb-2">
                                                        <div class="form-check form-check-inline mr-1">
                                                            <input {{$sorting === "asc" ? "checked" : ''}}
                                                                   class="form-check-input" id="inline-asc"
                                                                   type="radio" value="asc" name="sorting" disabled>
                                                            <label class="form-check-label"
                                                                   for="inline-radio1">Artan</label>
                                                        </div>
                                                        <div class="form-check form-check-inline mr-1">
                                                            <input {{$sorting === "desc" ? "checked" : ''}}
                                                                   class="form-check-input" id="inline-desc"
                                                                   type="radio" value="desc" name="sorting" disabled>
                                                            <label class="form-check-label"
                                                                   for="inline-radio1">Azalan</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 mb-2">

                                                        <button class="btn btn-pill btn-info" type="submit">Uygula
                                                        </button>
                                                        <button onclick="window.location.href='/admin/corporation'"
                                                                class="btn btn-pill btn-dark"
                                                                type="button">{{__('admin/corporation.index.filter clean')}}</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        @endif

                        <table class="table table-responsive-sm table-hover table-outline mb-0">
                            <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>{{__('admin/corporation.index.corporation type')}}</th>
                                <th>{{__('admin/corporation.index.corporation name')}}</th>
                                <th>
                                    <form id="code_form" action="{{route('corporation.index')}}" method="get">
                                        @if($codesort =='asc')
                                            <input type="hidden" id="code-desc" name="code_sort" value="desc"/>
                                        @else
                                            <input type="hidden" id="code-asc" name="code_sort" value="asc">
                                        @endif
                                        <a style="color: #768192" class="dropdown-toggle" href="javascript:{}"
                                           onclick="document.getElementById('code_form').submit();">{{__('admin/corporation.index.corporation code')}}</a>
                                    </form>

                                </th>

                                <th>{{__('admin/corporation.index.description')}}</th>

                                <th>
                                    <form id="code_form" action="{{route('corporation.index')}}" method="get">
                                        @if($ustSort == 'asc')
                                            <input type="hidden" id="ust-desc" name="ust_sort" value="desc"/>
                                        @else
                                            <input type="hidden" id="ust-asc" name="ust_sort" value="asc">
                                        @endif
                                        <a style="color: #768192" class="dropdown-toggle" href="javascript:{}"
                                           onclick="document.getElementById('code_form').submit();">{{__('admin/corporation.index.region')}}</a>
                                    </form>

                                </th>
                                <th>{{__('admin/corporation.index.zone')}}</th>
                                <th>{{__('admin/corporation.index.country')}}</th>
                                <th>{{__('admin/corporation.index.district')}}</th>
                                <th>{{__('admin/corporation.index.status')}}</th>
                                <th></th>
                                <th></th>

                            </tr>

                            </thead>
                            <tbody>
                            @foreach($corporations as $corporation)
                                <tr id="item-{{$corporation->id}}">
                                    <td> {{$loop->iteration + $skipped}} </td>
                                    <td> {{$corporation->corporationType->name}} </td>
                                    <td> {{$corporation->name}}  </td>
                                    <td> {{$corporation->code}}  </td>
                                    <td> {{$corporation->description}} </td>
                                    <td> {{$corporation->organization->parent->parent->name}}</td>
                                    <td> {{$corporation->organization->parent->name}}</td>
                                    <td> {{$corporation->geographicBoundary->parent->name}}</td>
                                    <td> {{$corporation->geographicBoundary->name}}</td>

                                    <td>
                                        @if($corporation->status == 'active' )
                                            <span
                                                class="badge badge-success">{{__('admin/corporation.index.active')}}</span>

                                        @else($corporation->status =='passive')
                                            <span
                                                class="badge badge-danger"> {{__('admin/corporation.index.passive')}}</span>
                                        @endif
                                    </td>
                                    <td width="5"><a class="btn btn-warning" role="button"
                                                     href="{{route('corporation.edit',$corporation->id)}}">{{__('admin/corporation.index.btn-edit')}} </a>
                                    <td width="5">
                                        <button class="btn btn-danger abc" value="{{$corporation->id}}"
                                                type="button" data-toggle="modal"
                                                data-target="#exampleModal">{{__('admin/user.index.btn-delete')}}</button>
                                    </td>


                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                {{$corporations->onEachSide(1)->links()}}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script src="{{asset('assets/js/corporation.js')}}"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            var data_id = "";

            $(".abc").click(function () {
                data_id = $(this).val();
            });

            $("#continue").click(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'DELETE',
                    url: '{{getenv('APP_URL')}}' + '/admin/corporation/' + data_id,

                    success: function (data) {

                        window.location.reload();
                    },
                    error: function (xhr) {

                        alert('Yetki silme işlemi sırasında bir hata meydana geldi.');
                    }
                });
            });

        });
    </script>
    <script>
        $(document).ready(function () {
            $('#bolge-id').on('change', function () {
                var bolgeId = $(this).val();
                if (bolgeId == "null") {
                    $("#mintika-id").html('<option value = "" selected>Alt Mıntıkalar</option>');
                    document.getElementById("mintika-id").disabled = true;
                }
                $.ajax({
                    type: 'GET',
                    url: '{{ route('ajax.organizations') }}',
                    data: {'parent_id': bolgeId},
                    dataType: 'json',

                    success: function (data) {
                        if (bolgeId != "null") {
                            $("#mintika-id").removeAttr('disabled').html('<option value = "" selected>Alt Etiketler</option>');
                        }
                        // $("#mintika-id").empty();
                        // $("#mintika-id").prop('disabled', false);
                        // $("#mintika-id").append('<option value="0">Mıntıka Seçiniz</option>');
                        $.each(data, function (key, value) {
                            $("#mintika-id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                        })
                    },
                    error: function (xhr) {
                        //todo core ui nin hata modal i acilacak
                        alert('HATA: Şehir aktarımı sırasında bir hata meydana geldi.');
                    }
                });
            });

            $('#corporation-sort').on('change', function () {
                var sort = $(this).val();
                if (sort != "null") {
                    $("#inline-asc").removeAttr('disabled').html();
                    $("#inline-desc").removeAttr('disabled').html();

                }
            });

            {{--$('#filter_button').on('click', function () {--}}

            {{--    var bolgeId = document.getElementById("bolge-id").value;--}}
            {{--    var mintika_id = document.getElementById("mintika-id").value;--}}
            {{--    var status = document.getElementById("corporation-status").value;--}}
            {{--    var button_id ="filter" ;--}}


            {{--    var url = "corporation?bolge_id=" + encodeURIComponent(bolgeId) + "&mintika_id=" + encodeURIComponent(mintika_id)--}}
            {{--        + "&corporation_status=" + encodeURIComponent(status) + "&button_id=" + encodeURIComponent(button_id);--}}
            {{--    window.location.href = url;--}}

            {{--});--}}

            {{--$('#button-desc').on('click', function () {--}}
            {{--    var code_desc = document.getElementById("code-desc").value;--}}
            {{--    --}}{{--var i = <?php echo $codesort; ?>--}}
            {{--    var button_id ="code_sort" ;--}}

            {{--    var url = "corporation?code_sort=" + encodeURIComponent(code_desc) + "&button_id=" + encodeURIComponent(button_id);--}}
            {{--    window.location.href = url;--}}
            {{--});--}}
            {{--$('#button-asc').on('click', function () {--}}
            {{--    var code_asc = document.getElementById("code-asc").value;--}}
            {{--    --}}{{--var i = <?php echo $codesort; ?>--}}
            {{--    var button_id ="code_sort" ;--}}

            {{--    var url = "corporation?code_sort=" + encodeURIComponent(code_asc) + "&button_id=" + encodeURIComponent(button_id);--}}
            {{--    window.location.href = url;--}}
            {{--});--}}

            {{--$('#button-desc-ust').on('click', function () {--}}
            {{--    var ust_desc = document.getElementById("ust-desc").value;--}}
            {{--    --}}{{--var i = <?php echo $codesort; ?>--}}
            {{--    var button_id ="ust_sort" ;--}}

            {{--    var url = "corporation?ust_sort=" + encodeURIComponent(ust_desc) + "&button_id=" + encodeURIComponent(button_id);--}}
            {{--    window.location.href = url;--}}
            {{--});--}}
            {{--$('#button-asc-ust').on('click', function () {--}}
            {{--    var ust_asc = document.getElementById("ust-asc").value;--}}
            {{--    --}}{{--var i = <?php echo $codesort; ?>--}}
            {{--    var button_id ="ust_sort" ;--}}

            {{--    var url = "corporation?ust_sort=" + encodeURIComponent(ust_asc) + "&button_id=" + encodeURIComponent(button_id);--}}
            {{--    window.location.href = url;--}}
            {{--});--}}
        });
    </script>

    <style>
        .error {
            content: "\f343";
            display: block;
            margin-top: 3%;
            width: 100%;
            background: none;
            color: #ff0000;
            font-size: smaller;
        }

    </style>
@endsection
@section('css')@endsection
@section('js')@endsection
