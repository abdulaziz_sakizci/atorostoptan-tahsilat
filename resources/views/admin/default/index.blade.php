@extends('admin.layouts')
@section('content')

    @section('breadcrumbs')
    {{ Breadcrumbs::render('home') }}
    @endsection

    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid">
                <div class="fade-in">

                    <!--
                    <div class="row">
                        <div class="col-6 col-lg-4">
                            <div class="card overflow-hidden">
                                <div class="card-body p-0 d-flex align-items-center">
                                    <div class="bg-gradient-info py-4 px-5 mfe-3">
                                        <svg class="c-icon c-icon-xl">
                                            <use xlink:href="/@coreui/sprites/free.svg#cil-thumb-up"></use>
                                        </svg>
                                    </div>
                                    <div>
                                        <div class="text-value text-info">$1.999,50</div>
                                        <div class="text-muted text-uppercase font-weight-bold small">ÖDENMİŞ MİKTAR</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 col-lg-4">
                            <div class="card overflow-hidden">
                                <div class="card-body p-0 d-flex align-items-center">
                                    <div class="bg-gradient-warning py-4 px-5 mfe-3">
                                        <svg class="c-icon c-icon-xl">
                                            <use xlink:href="/@coreui/sprites/free.svg#cil-moon"></use>
                                        </svg>
                                    </div>
                                    <div>
                                        <div class="text-value text-warning">$1.999,50</div>
                                        <div class="text-muted text-uppercase font-weight-bold small">BEKLEMEDE OLAN MİKTAR</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 col-lg-4">
                            <div class="card overflow-hidden">
                                <div class="card-body p-0 d-flex align-items-center">
                                    <div class="bg-gradient-danger py-4 px-5 mfe-3">
                                        <svg class="c-icon c-icon-xl">
                                            <use xlink:href="/@coreui/sprites/free.svg#cil-thumb-down"></use>
                                        </svg>
                                    </div>
                                    <div>
                                        <div class="text-value text-danger">$1.999,50</div>
                                        <div class="text-muted text-uppercase font-weight-bold small">iPTAL EDİLEN MİKTAR</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    -->

                </div>
            </div>
        </main>


    </div>


@endsection
@section('css')@endsection
@section('js')@endsection

