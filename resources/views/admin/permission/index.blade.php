@extends('admin.layouts')
@section('content')

@section('breadcrumbs')
    {{ Breadcrumbs::render('permission') }}
@endsection

@include('components.permission.deletemodal')

<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <!-- /.row-->
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title mb-0">
                                {{__('admin/user.index.permissions')}}
                            </h4>
                            <a class="card-header-action"
                               href="{{route('permission.create')}}">
                                <button class="btn btn-success">{{__('admin/user.index.btn-add')}}</button>
                            </a>
                        </div>
                    </div>

                    <div class="card-body">
                        <table class="table table-responsive-sm table-hover table-outline mb-0">
                            <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>{{__('admin/user.index.permission')}}</th>
                                <th>{{__('admin/user.index.created_at')}}</th>
                                <th>{{__('admin/user.index.updated_at')}}</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($permissions as $permission)

                                <tr id="item-{{$permission->id}}">
                                    <td> {{$loop->iteration + $skipped}} </td>
                                    <td> {{$permission->name}}  </td>
                                    <td> {{$permission->created_at}} </td>
                                    <td> {{$permission->updated_at}} </td>
                                    <td width="5"><a class="btn btn-warning" role="button"
                                                     href="{{route('permission.edit',$permission->id)}}">{{__('admin/user.index.btn-edit')}} </a>
                                    <td width="5">
                                        <button class="btn btn-danger permissionDelete" value="{{$permission->id}}"
                                                type="button"  data-toggle="modal" data-target="#deleteModal">{{__('admin/user.index.btn-delete')}}</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </main>

    <script src="{{asset('assets/js/permission.js')}}"></script>

@endsection
@section('css')@endsection
@section('js')@endsection
