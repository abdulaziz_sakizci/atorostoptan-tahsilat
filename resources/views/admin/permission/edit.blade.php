@extends('admin.layouts')
@section('content')

@section('breadcrumbs')
    {{ Breadcrumbs::render('permission_edit', $permission)}}
@endsection

<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">

                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="card mx-2">

                            <div class="card-header"><h4><strong> {{__('admin/user.edit.permission edit')}} </strong></h4>
                            </div>
                            <form class="form-horizontal" action="{{route('permission.update',$permission->id)}}" id="myform"
                                  method="post"
                                  enctype="multipart/form-data">
                                @csrf

                                <div class="card-body">

                                    @method('PUT')
                                    @if(Session::has('success'))
                                        <div
                                            class="alert-success"> {!! html_entity_decode(Session::get('success')) !!} </div>
                                    @endif

                                    @if(Session::has('error'))
                                        <div class="alert"> {!! html_entity_decode(Session::get('error')) !!}  </div>
                                    @endif

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label"
                                               for="text-input">{{__('admin/user.edit.permission')}}</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="permission" type="text" name="permission"
                                                   value="{{$permission->name}}">
                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary"
                                            type="submit">{{__('admin/user.edit.update')}}</button>
                                </div>
                            </form>

                        </div>
                    </div>
                    <div class="col-md-3"></div>

                </div>
            </div>
        </div>
    </main>

</div>

@endsection
@section('css')@endsection
@section('js')@endsection
