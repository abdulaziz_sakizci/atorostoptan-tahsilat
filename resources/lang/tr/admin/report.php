<?php

return [

    'index' => [
        'report transactions' => 'Rapor İşlemleri',
        'payment details' => 'Ödeme Detayları',
        'code'=> 'Kod',
        'student name'=> 'Öğrencinin Adı',
        'guardian name' => 'Velinin Adı',
        'amount' => 'Ödeme Miktarı',
        'guardian phone' => 'Velinin Telefonu',
        'product type name' => 'Ürün Tipi',
        'description' => 'Açıklama',
        'user' => 'Kullanıcı',
        'status' => 'Durum',
        'save' => 'Kaydet',
        'pending' => 'Bekliyor',
        'paid' => 'Ödendi',
        'cancelled' => 'İptal Edildi',
        'cancel' => 'İptal Et',
        'failed' => 'Başarısız',
        'details' => 'Detaylar',
        'payment date' =>'Ödeme Tarihi',
        'installmentCount' =>"Taksit",
        'corparation' =>'Kurum',
        'process code' =>"İşlem No",
        'credit card number' =>"Kart Numarası",
        'process date' => 'İşlem Tarihi',
        'bank name' => "Banka Adı",
        'customer id' =>"Müşteri ID",
        'search' =>"Ara",
        'export' =>"Excele Aktar",
        'corporation code' =>"Cari Kod",
        'corporation description' =>"Tabela Ünvanı",
        'region' =>"Üst Etiket"
    ],

];




















