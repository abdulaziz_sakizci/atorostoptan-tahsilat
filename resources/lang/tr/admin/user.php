<?php

return [
    'index' => [
        'user operations' => 'Kullanıcı İşlemleri',
        'roles' => 'Roller',
        'permissions' => 'Yetkiler',
        'btn-add' => 'Ekle',
        'btn-assign-role' => 'Rol Ata',
        'btn-edit' => 'Düzenle',
        'btn-delete' => 'Sil',
        'name' => 'Adı Soyadı',
        'email' => 'E-Posta',
        'corporation id' => 'Şirket ID',
        'organization' => 'Organizasyon',
        'role' => 'Rol',
        'permission' => 'Yetki',
        'created_at' => 'Eklenme Tarihi',
        'updated_at' => 'Güncellenme Tarihi'
    ],


    'create' => [
        'user add' => 'Kullanıcı Ekle',
        'role add' => 'Rol Ekle',
        'permission add' => 'Yetki Ekle',
        'save' => 'Kaydet',
        'name' => 'Adı Soyadı',
        'email' => 'E-Posta',
        'password' => 'Şifre',
        'role' => 'Rol',
        'permission' => 'Yetki',
        'permissions' => 'Yetkiler',
        'organization type' => 'Organizasyon Tipi',
        'zone' => 'Alt Etiket',
        'corporation' => 'Kurum',
        'region' => 'Üst Etiket'

    ],

    'edit' => [
        'user edit' => 'Kullanıcı Düzenle',
        'role edit' => 'Rol Düzenle',
        'permission edit' => 'Yetki Düzenle',
        'update' => 'Güncelle',
        'name' => 'Adı Soyadı',
        'email' => 'E-Posta',
        'password' => 'Şifre',
        'role' => 'Rol',
        'permission' => 'Yetki',

    ],
];
