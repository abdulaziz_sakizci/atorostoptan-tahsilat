<?php

return [

    'login' => 'Giriş',
    'sign in to your account' => 'Hesabınıza giriş yapın',
    'email' => 'E-Posta',
    'password' => 'Şifre',
    'forgot password' => 'Şifremi unuttum',
    'camlica' => 'Akdeniz Toros Toptan Tahsilat Sistemi',

];
