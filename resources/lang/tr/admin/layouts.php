<?php

return [

    'dashboard' => 'Anasayfa',
    'payment transactions'=> 'Ödeme İşlemleri',
    'user operations'=> 'Kullanıcı İşlemleri',
    'roles'=> 'Roller',
    'permissions'=> 'Yetkiler',
    'corporation operations'=> 'Kurum İşlemleri',
    'report operations'=> 'Rapor İşlemleri',
    'product operations' => 'Ürün İşlemleri',
    'profile'=> 'Profil',
    'settings'=> 'Ayarlar',
    'logout'=> 'Çıkış',
    'account' => 'Hesap',
    'users' => 'Kullanıcılar',
    'operation' => 'İşlemler',
];
