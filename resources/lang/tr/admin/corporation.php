<?php

return [

    'index' =>[
        'corporation operations' => 'Kurum İşlemleri',
        'corporation type' => 'Kurum Tipi',
        'btn-add'=>'Ekle',
        'btn-edit'=>'Düzenle',
        'btn-delete'=>'Sil',
        'corporation name' => 'Kurum İsmi',
        'corporation code' => 'Cari Kodu',
        'description' => 'Tabela Ünvanı',
        'region' => 'Üst Etiket',
        'zone' => 'Alt Etiket',
        'country' => 'İl',
        'district' => 'İlçe',
        'status' => 'Durum',
        'active' => 'Aktif',
        'passive' => 'Pasif',
        'search' => 'Ara',
        'filter' => 'Filtrele',
        'filter clean' => 'Temizle'

    ],

    'create' => [
        'corporation add' => 'Kurum Ekle',
        'corporation type' => 'Kurum Tipi',
        'save' => 'Kaydet',
        'corporation name' => 'Kurum İsmi',
        'code' => 'Cari Kodu',
        'description' => 'Açıklama',
        'region' => 'Üst Etiket',
        'zone' => 'Alt Etiket',
        'country' => 'İl',
        'district' => 'İlçe',
        'status' => 'Durum',

    ],

    'edit' => [
        'corporation edit' => 'Kurum Düzenle',
        'corporation type' => 'Kurum Tipi',
        'update' => 'Güncelle',
        'corporation name' => 'Kurum İsmi',
        'code' => 'Cari Kodu',
        'description' => 'Açıklama',
        'region' => 'Üst Etiket',
        'zone' => 'Alt Etiket',
        'country' => 'İl',
        'district' => 'İlçe',
        'status' => 'Durum',

    ],

];
