<?php

return [

    'index' => [
        'payment transactions' => 'Ödeme İşlemleri',
        'payment details' => 'Ödeme Detayları',
        'code'=> 'Kod',
        'student name'=> 'Öğrencinin Adı',
        'guardian name' => 'Velinin Adı',
        'amount' => 'Tutar',
        'guardian phone' => 'Velinin Telefonu',
        'product type name' => 'Ürün Tipi',
        'description' => 'Açıklama',
        'user' => 'Kullanıcı',
        'status' => 'Durum',
        'save' => 'Kaydet',
        'pending' => 'Bekliyor',
        'paid' => 'Ödendi',
        'cancelled' => 'İptal Edildi',
        'cancel' => 'İptal Et',
        'failed' => 'Başarısız',
        'details' => 'Detaylar',
        'payment date' =>'Ödeme Tarihi',
        'installmentCount' =>"Taksit",
        'corparation' =>'Kurum',
        'process code' =>"İşlem No",
        'credit card number' =>"Kart Numarası",
        'process date' => 'İşlem Tarihi',
        'bank name' => "Banka Adı",
        'customer id' =>"Müşteri ID",
        'select state' =>'Tüm Üst Etiketler',
        'select region' =>'Tüm Alt Etiketler',
        'select please' =>"Ödeme Durumu Seçiniz",
        'search' =>'Ara',
        'corporation code' =>"Cari Kod",
        'corporation description' =>"Tabela Ünvanı",
        'region' =>"Üst Etiket"
    ],

    'create' => [
        'payment add'=> 'Ödeme Ekle',
        'code'=> 'Kod',
        'student name'=> 'Öğrencinin Adı',
        'guardian name' => 'Velinin Adı',
        'amount' => 'Tutar',
        'guardian phone' => 'Velinin Telefonu',
        'product type name' => 'Ürün Tipi',
        'description' => 'Açıklama',
        'user' => 'Kullanıcı',
        'status' => 'Durum',
        'save' => 'Kaydet',
        'corparation' =>'Kurum'

    ],

    'edit' => [
        'payment edit' => 'Ödeme Düzenle',
        'payment details' => 'Ödeme Detayları',
        'code' => 'Code',
        'student name' => 'Student Name',
        'guardian name' => 'Guardian Name',
        'amount' => 'Tutar',
        'guardian phone' => 'Guardian Phone',
        'product type name' => 'Product Type Name',
        'description' => 'Product Type Name',
        'user' => 'Kullanıcı',
        'status' => 'Status',
        'save' => 'Save',
        'corparation' =>'Kurum',
        'payment date' =>'Ödeme Tarihi'
    ],

];




















