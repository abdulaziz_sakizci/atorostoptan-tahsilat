<?php

return [

    'index' =>[
        'product operations' => 'Ürün İşlemleri',
        'product type' => 'Ürün Tipi',
        'status' => 'Durum',
        'active' => 'Aktif',
        'passive' => 'Pasif',
        'btn-add'=>'Ekle',
        'btn-edit'=>'Düzenle',
        'btn-delete'=>'Sil',

    ],

    'create' => [
        'product add' => 'Ürün Ekle',
        'save' => 'Kaydet',
        'product type' => 'Ürün Tipi',
        'status' => 'Durum',
        'btn-add'=>'Ekle',

    ],

    'edit' => [
        'product edit' => 'Ürün Düzenle',
        'update' => 'Güncelle',
        'product type' => 'Ürün Tipi',
        'status' => 'Durum',


    ],

];
