<?php

return [
    'index' =>[
        'product operations' => 'Product Operations',
        'product type' => 'Product Type',
        'status' => 'Status',
        'active' => 'Active',
        'passive' => 'Passive',
        'btn-add'=>'Add',
        'btn-edit'=>'Edit',
        'btn-delete'=>'Delete',


    ],

    'create' => [
        'product add' => 'Product Add',
        'save' => 'Save',
        'product type' => 'Product Type',
        'status' => 'Status',
        'btn-add'=>'Add',

    ],

    'edit' => [
        'product edit' => 'Product Edit',
        'update' => 'Update',
        'product type' => 'Product Type',
        'status' => 'Status',
    ],

];
