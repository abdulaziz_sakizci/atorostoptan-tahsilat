<?php

return [

    'index' => [
        'payment transactions' => 'Payment Transactions',
        'code' => 'Code',
        'student name' => 'Student Name',
        'guardian name' => 'Guardian Name',
        'amount' => 'Amount',
        'guardian phone' => 'Guardian Phone',
        'product type name' => 'Product Type Name',
        'description' => 'Product Type Name',
        'user' => 'User',
        'status' => 'Status',
        'save' => 'Save',
        'pending' => 'Pending',
        'paid' => 'Paid',
        'cancelled' => 'Cancelled',
        'payment date' =>'Payment Date',
        'installmentCount' =>"Installment Count",
        'corparation' => "Corparation",
        'process code' =>"Process No",
        'credit card number' =>"Credit Card Number",
        'process date' => 'Process Date',
        'bank name' => "Bank Name",
        'customer id' =>"Customer ID",
        'select state' =>'Select State',
        'select please' =>"Select Please",
        'search' =>'Search',
        'corporation code' =>"Current Code",
        'corporation description' =>"Signboard Title",
        'region' =>"Region"
    ],

    'create' => [
        'payment add' => 'Payment Add',
        'code' => 'Code',
        'student name' => 'Student Name',
        'guardian name' => 'Guardian Name',
        'amount' => 'Amount',
        'guardian phone' => 'Guardian Phone',
        'product type name' => 'Product Type Name',
        'description' => 'Product Type Name',
        'user' => 'User',
        'status' => 'Status',
        'save' => 'Save',

    ],

    'edit' => [
        'payment edit' => 'Payment Edit',
        'code' => 'Code',
        'student name' => 'Student Name',
        'guardian name' => 'Guardian Name',
        'amount' => 'Amount',
        'guardian phone' => 'Guardian Phone',
        'product type name' => 'Product Type Name',
        'description' => 'Product Type Name',
        'user' => 'User',
        'status' => 'Status',
        'save' => 'Save',
        'payment date' =>'Payment Date'
    ],

];




















