<?php

return [

   'dashboard' => 'Dashboard',
    'payment transactions'=> 'Payment Transactions',
    'user operations'=> 'User Operations',
    'corporation operations'=> 'Corporation Operations',
    'product operations' => 'Product Operations',
    'profile'=> 'Profile',
    'settings'=> 'Settings',
    'logout'=> 'Logout',
    'account' => 'Account',
    'operation' => 'Operation',


];
