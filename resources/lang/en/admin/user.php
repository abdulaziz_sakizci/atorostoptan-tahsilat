<?php

return [

     'index' =>[
         'user operations' => 'User Operations',
         'btn-add'=>'Add',
         'btn-edit'=>'Edit',
         'btn-delete'=>'Delete',
         'name' => 'Name',
         'email' => 'Email',
         'corporation id' => 'Corporation ID',

     ],

    'create' => [
        'user add' => 'User Add',
        'save' => 'Save',
        'name' => 'Name',
        'email' => 'Email',
        'password' => 'Email',

    ],

    'edit' => [
        'user edit' => 'User Edit',
        'update' => 'Update',
        'name' => 'Name',
        'email' => 'Email',
        'password' => 'Password',

    ],

];
