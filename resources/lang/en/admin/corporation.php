<?php

return [

    'index' =>[
        'corporation operations' => 'Corporation Operations',
        'corporation type' => 'Corporation Type Name',
        'btn-add'=>'Add',
        'btn-edit'=>'Edit',
        'btn-delete'=>'Delete',
        'corporation name' => 'Corporation Name',
        'description' => 'Description',
        'region' => 'Region',
        'zone' => 'Zone',
        'country' => 'country',
        'district' => 'district',
        'status' => 'status',
        'active' => 'Active',
        'passive' => 'Passive',
        'search' => 'Search',
        'filter' => 'Filter',
        'filter clean' => 'Clean Filter'
    ],

    'create' => [
        'corporation add' => 'Corporation Add',
        'corporation type' => 'Corporation Type Name',
        'save' => 'Save',
        'corporation name' => 'Corporation Name',
        'description' => 'Description',
        'region' => 'Region',
        'zone' => 'Zone',
        'country' => 'country',
        'district' => 'district',
        'status' => 'status',

    ],

    'edit' => [
        'corporation edit' => 'Corporation Edit',
        'corporation type' => 'Corporation Type Name',
        'update' => 'Update',
        'corporation name' => 'Corporation Name',
        'description' => 'Description',
        'region' => 'Region',
        'zone' => 'Zone',
        'country' => 'country',
        'district' => 'district',
        'status' => 'status',

    ],

];
