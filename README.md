# Akdeniz Tahsilat Sistemi

Projenin ayağa kalkması için warden kurulu olmalıdır.
> warden env-init komutuna gerek yok, zaten eklendi.

### Proje'nin Ayağa kaldırılması

```shell
warden env up -d
```

### /etc/hosts ayarları

/etc/hosts dosyasına aşağıdaki satır eklenir

```shell
127.0.0.1 atoros-tahsilat.test
```

### Proje'nin Çalıştırılması

```shell
warden env start
```

### Local SSL Ayarları

```shell
warden sign-certificate atoros-tahsilat.test
```

### chrome a SSL eklenmesi

https://docs.warden.dev/installing.html

### Shell Erişimi

```shell
warden shell
```

### PhpStorm DB ayarları

https://docs.warden.dev/configuration/database.html#phpstorm

### Composer

```shell
composer install
```

```bash
composer install
cp .env.local .env
php artisan key:generate
php artisan config:cache
```

# NPM ve Migrate

```shell
npm install
npm run dev
php artisan migrate
php artisan db:seed
(Yukarıdaki iki artisan komutunu yerine aşağıdaki komutu da çalıştırabiliriz)
php artisan migrate:fresh 
```

### Proje Stop

```shell
warden env stop
```

### Deployment

```shell
php artisan deploy production
php artisan deploy test
```

### Queue

```shell
php artisan queue:work --tries=3
 /RunCloud/Packages/php74rc/bin/php /home/tahsilatsistemi/webapps/tahsilatsistemi/current/artisan queue:work --sleep=3 --tries=3
```

### Docker Hatası olursa

```shell
sudo chown $USER /var/run/docker.sock
```

#### Unlock

```shell
php artisan deploy:run deploy:unlock development
```
